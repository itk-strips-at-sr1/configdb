lsof -i :8080
read -p "Do you want to kill the process? (y/n): " answer
if [ "$answer" = "y" ]; then
    lsof -i :8080 | awk 'NR>1 {print $2}' | xargs kill
fi