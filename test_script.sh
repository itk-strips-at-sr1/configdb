#!/bin/bash

host=${1:-localhost}
port=${2:-8000} # 8000 is the port in current CI jobs

echo running the first basic configdb procedure on $host:$port

#alias python=python3 # damn basic docker container on cern gitlab
# it still does not work??
PYTHON_NAME=python3
$PYTHON_NAME scripts/main_configDB.py --host $host --port $port --upload --config-dir examples/sr1-barrel-st-4staves/ || exit 1
$PYTHON_NAME scripts/main_configDB.py --host $host --port $port --read > runkey_latest.json || exit 2
mkdir temp_config_dir/
$PYTHON_NAME scripts/main_configDB.py --host $host --port $port --load runkey_latest.json --translate temp_config_dir/ || exit 3
echo run calibrations, modify the configs
$PYTHON_NAME scripts/main_configDB.py --host $host --port $port --upload --config-dir temp_config_dir/ || exit 4
