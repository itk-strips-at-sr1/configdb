alias la='ls -A'
alias lx='ls -lX'
alias l='ls -ltrh'
alias pwdf='readlink -f'
  
alias cdb='cd /home/itkstrips/tests/configdb/'
alias conts='cd /home/itkstrips/tests/configdb/ && . setupAndUpdateDocker.sh'
alias dpsr='docker ps --format '\''table {{.ID}}\t{{.Image}}\t{{.Status}}\t{{.Names}}'\'''

alias brc='vim /home/itkstrips/tests/configdb/.bashrc'
alias crc='code /home/itkstrips/tests/configdb/.bashrc'
alias vrc='vim /home/itkstrips/tests/configdb/.vimrc'
alias sbash='. /home/itkstrips/tests/configdb/.bashrc'
alias vnv='source /home/itkstrips/tests/configdb/itk-demo-configdb/example/.venv/bin/activate'
alias gfex='echo grep -r "word" ./  ,   find . -name thisfile.txt'
alias dcomp='cd /home/itkstrips/tests/configdb/itk-demo-configdb/example/ | docker compose -f services_compose.yaml up -d'
alias dcomp2='cd /home/itkstrips/tests/configdb/itk-demo-configdb/example/ | docker compose up -d'


LS_COLORS='rs=0:di=1;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30    ;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=    01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;3    1:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=0    1;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.b    mp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;3    5:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.we    bm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35    :*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;    35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac    =00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*    .oga=00;36:*.spx=00;36:*.xspf=00;36:';

#LS_COLORS="*.C=40;94:*.txt=92:*.h=91:*.pdf=95:*.d=90"
LS_COLORS="*.C=100;94:*.txt=92:*.h=91:*.pdf=95:*.d=90:di=1;32:*.py=94"
export LS_COLORS




##########################################

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac
 # uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes
 if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
        color_prompt=
    fi
fi
 if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@:\W\$ '
fi
unset color_prompt force_color_prompt
#\[\033[01;32m\]
#export PS1='\u:\W$ '
export PS1='\[\033[01;32m\]\u:\[\033[01;37m\]\W$ '
 # If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \W\a\]$PS1"
    ;;
*)
    ;;
esac
