# Configuration Database using ITk Microservices

This repository contains a configuration database powered by ITk Microservices 
software. We leverage their [itk-demo-configdb](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb)
microservice
to provide a robust and flexible solution for managing chip configurations.


## Operations

You have to run it from the ConfigDB server (swrod04) now. UPDATE: there are some instances where you 
can commit a RK from any server. We are currently unsure why some operations must be done on swrod04 and some can be done anywhere.

We are adding the option to change the server hostname.
Use `main_configDB.py` to download and upload "runkeys".
It works with `latest` runkey by default.
See `main_configDB.py -h` for more info.

To test the full basic data cycle:
```
./test_script.sh pcatlitkswrod04 5111
```


### Uploading

Upload YARR configs (all staves or one) as a new runkey ('latest' tag, also ):
```
main_configDB.py --upload -c temp_config_dir/
main_configDB.py --upload -c temp_config_dir/ PPB1A:CERNSR1_stave0_testing_setup.json
```

Our standard configs location: 
```
/home/itkstrips/itkstrips-configs-sr1/stave-configs/
```

Uploading a runkey from a JSON file (from swrod04)
```
main_configDB.py --load test_db_runkeys/testDB_one_stave_notFinalFormat.json --upload
```
### Downloading
Uses latest tag by default
```
main_configDB.py --read > runkey_latest.json
```


### Other
Convert the runkey into YARR config files (TODO: it stopped working now!):
```
main_configDB.py --load runkey_latest.json --translate temp_config_dir/
```


### Install dependencies

The only dependency is `pyconfigdb`. If it is not found, install it with `pip`:

```
pip install --user pyconfigdb --extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple
```


## Key Features - 

**OpenAPI Interface**: Interact with the database through intuitive HTTP requests.
Built on ITk Pixel's Expertise: Benefit from the ITk Microservices' software 
with easy-to-use Docker images

[TODO: Further customize README]: (mariaDB+mysql, staging/commiting, latest branch, referencing UUID's, etc)

---

# ITK Demo Setup

## Getting Started

### Prerequisites

- Install Docker on AlmaLinux 9: [Install Docker Guide](https://orcacore.com/install-use-docker-almalinux-9/)
  
- Git clone the repositories:
  - [Microservice Setup](https://demi.docs.cern.ch/tutorials/microservice_setup/)
  - [ITK Demo ConfigDB](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb)

### Installation

1. **Clone the repository**
   ```bash
   git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb
   ```

2. **Navigate to the project directory**
   ```bash
   cd itk-demo-configdb
   ```

3. **Bootstrap and configure the environment**
   ```bash
   . bootstrap
   . config
   ```

   > **Note**: Ensure the `DOCKER_DIR` and `RUNKEY_DIR` paths autofill correctly. They should look like:
   > ```
   > DOCKER_DIR=/home/itkstrips/tests/configdb/itk-demo-configdb/example/docker
   > RUNKEY_DIR=/home/itkstrips/tests/configdb/itk-demo-configdb/example/configs/tree
   > ```

4. **Activate the virtual environment**
   ```bash
   source .venv/bin/activate
   pip install -r requirements.txt
   ```

5. **Install additional packages**
   ```bash
   pip install pyconfigdb
   ```

6. **Start Docker Compose**
   ```bash
   docker compose -f services_compose.yaml up -d
   docker compose up -d
   ```

   > **Note**: If you encounter the warning:
   > ```
   > WARN[0000] The "HOST" variable is not set. Defaulting to a blank string.
   > ```
   > Run `. config` again.

## Running the Example

To get the example up and running, refer to the updated instructions in the ITK Demo ConfigDB Git repository.

## Updating Your Setup

To update your setup to the latest version:
```bash
git pull
cd example/
./config
docker compose up -d
```

## Resources

- Main Git repository: [ITK Demo ConfigDB](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb/-/tree/master/example)
- Tunneling Proxy Guide: [Tunneling Proxy](https://gitlab.cern.ch/groups/atlas-itk-pixel-systemtest/itk-demo-sw/-/wikis/tutorials/browser_proxy)

## Viewing the API

- Ensure all services (including `service_registry`) are running:
  ```bash
  docker ps
  docker compose up -d
  ```

- Access the API at:  
  `http://pcatlitkflx03.cern.ch/`

## Proxy Configuration

- **When on CERN Wi-Fi**: Set proxy to system proxy (not direct).
- **When off CERN Wi-Fi**: Set tunneling proxy to auto-switch.

## Troubleshooting

- **Cannot connect to Docker daemon:**
  ```bash
  systemctl start docker
  ```

- **Port conflicts**  
  If you encounter port conflicts, it’s safe to change the port (e.g., `5111` → `5112`).

- **Restart the whole database**  
   To restart and clean the database:
   ```bash
   docker compose down
   docker volume ls
   docker volume rm itk-demo-configdb_sqlite
   docker volume create itk-demo-configdb_sqlite
   # Remove backend content in DOCKER_DIR
   ```

## Notes on Runkeys

- **Creating runkeys**: Layer-by-layer creation is slower than all-at-once creation.
- **Runkey permanence**: Once a runkey is created, it cannot be deleted.

## Working CURL Command

```bash
curl --request POST \
  --header "content-type: application/json" \
  --header "accept: application/json" \
  --data '{"stage": false, "payload_data": true, "name": "latest"}' \
  http://pcatlitkflx03:32791/api/tag/tree | grep typ
```



# Developer Contact
### Casey Bellgraph 
Primary Strips ConfigDB backend developer. Best for Strips-related ConfigDB questions:
- Email: cbellgra@iu.edu
- Skype: casey.bellgraph

### Alex Toldaiev 
Strips ConfigDB local supervisor, best for connectivity and future-facing questions
- Email: alex.toldaiev@cern.ch


### Jonas Schmeng 
Main DEMI backend developer, best for true backend questions. Develops the proper backend used by Casey/Alex
- Email: jonas.schmeing@cern.ch
