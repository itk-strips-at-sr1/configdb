#!/usr/bin/env python
# SKELETON CONFIGURATION
from service_registry import ServiceRegistry
from json import loads
import json

from requests import post as http_post
from config import config
import os, json
import uuid
import argparse
from pathlib import Path # to get file names in a directory with Path(directory).glob('CERNSR1*testin_hcc*.json') 
import configDB_connectivity as conn
from configdb_stripsRunkeys import *
import time

try:
    import pyconfigdb
except ImportError:
    print("Error: If this is unsuccessful, make sure you are in the right .venv")

dir_path = os.path.dirname(os.path.realpath(__file__))
config_file_path = dir_path + "/../SR1_config_files/"  # path to config-files
config_file_path_hcc = dir_path + "/../yarr-star-fe-configs/"  # path to config-files
default_config_dir = os.path.dirname(os.path.realpath(__file__)) + "/../../temp_stave_configs/"

def set_rootNode(node):
    global rootNode
    rootNode = node

def set_viewNodes(stave, geometry, felix):
    global stavesNode, geometryNode, felixNode
    stavesNode = stave
    geometryNode = geometry
    felixNode = felix

# when json library converts a Python object to a json string it checks its type
# unfortunately, it checks whether an object isinstance(obj, dict)
# not some MutableMapping or UserDict, as by the modern rule in Python
# see https://stackoverflow.com/questions/70762125/why-is-class-that-extends-dict-json-serializable-but-one-that-extends-collectio
# json is a very old module, so it still works by old rules
# add default=dict to treat an object of unknown type as a dict
# we use UserDict in the shortcut classes for Strips runkey structures
def dumps(obj):
    return json.dumps(obj, default=dict)


# Beginning of Alex's formattedRunkey_comline.py functions
############################################################################################################
############################################################################################################

std_stave_connectivity_files = [
    'PPB1A:CERNSR1_stave0_testing_setup.json',
    'PPB1B:CERNSR1_stave1_testing_setup.json',
    'PPB1C:CERNSR1_stave2_testing_setup.json',
    'PPB1D:CERNSR1_stave3_testing_setup.json',
]


def load_std_configs(dir_with_configs, stave_connectivity_files=std_stave_connectivity_files):
  input_stave_configs = {}
  # a dictionary of
  # stave_name: {
  #   'chipType': <>
  #   'side_m': {}
  #   'side_s': {
  #     itsdaq_num: <config>,
  #     'rx': <>
  #     'tx': <>
  #     'enable':  <>
  #     'locked': <>
  #   }
  # }

  for stave_name, connectivity_fpath in stave_connectivity_files.items():
    input_stave_configs.setdefault(stave_name, {'side_m': {}, 'side_s': {}})
    # TODO: should we test that we do not overwrite the config for the same stave?
    stave_conf = input_stave_configs[stave_name]
    #print("Path():",dir_with_configs + '/' + connectivity_fpath)

    connectivity_file = Path(dir_with_configs + '/' + connectivity_fpath)
    with connectivity_file.open() as file:
        connectivity_json = json.load(file)

    # connectivity_json has a simple structure:
    # chipType: <>
    # chips: [{config_json, tx, rx, enable, locked}]
    # I am not sure what ebanle and locked mean -- TODO: touch base with Bruce
    # for now let's save them with the chip config
    stave_conf['chipType'] = connectivity_json['chipType'] # TODO: chipType probably has to go all the way under HCC
    # although the whole stave is loaded with the same chips,
    # we need to pick this parameter from under the fiber nodes, when we generate the run connectivity scan

    is_short_strip = len(connectivity_json['chips']) > 28
    stave_conf['staveType'] = 'SS' if is_short_strip else 'LS'
    # <-- TODO: this logic might not work for Endcap petals!

    for chip_c in connectivity_json['chips']:
        s = chip_c['config']
        # get the itsdaq  number out of the path
        # f'CERNSR1_{stave_num}_tecting_hcc<num>.json
        itsdaq_number = int(s[s.index('hcc')+3: s.index('.json')])
        # translate it to the common spec terms:
        hcc_location = conn.translate_ITSDAQ_number(itsdaq_number, is_short_strip)
        # it returns the location in the format:
        # ReferenceHCCLocation(stave_side, module_number, hybrid_name)
        # where stave_side = s or m
        # module_number is an int
        # hybrid_name is X or Y
        # save it
        side_conf = stave_conf['side_'+hcc_location.stave_side]          
        side_conf.setdefault('FEs', {})

        fe_config = {'module_number': hcc_location.module_number, 'hybrid_name': hcc_location.hybrid_name}

        # of course, read the config and save it
        # I assume it is relative to the path of the connectivity file
        # it might be an absolute path though TODO make it more robust later
        with open(dir_with_configs + '/' + chip_c['config']) as file:
            hcc_config = json.load(file)
        #side_conf['FEs'][fe_id]
        fe_config['config'] = hcc_config

        # TODO: I think enable and locked are the parameters for the scan engine
        #       (e.g. if a FE is "locked" the scan won't update the config file inplace)
        #       if so, these parameters should not be in the chip configs
        #       they are the configuartion of the scan, i.e. the procedure thatis done on the chips
        fe_config['enable'] = chip_c['enable']
        fe_config['locked'] = chip_c['locked']

        # now the connectivity
        raw_tx = chip_c['tx']
        raw_rx = chip_c['rx']

        cur_rx_connectivity = conn.translate_Felixcore_RX_elink(raw_rx)
        # namedtuple("ReferenceHCCFelixRX",  "device gbt_link raw_elink elink egroup epath")
        # device gbt_link raw_elink are global addresses, i.e. which fiber is connected to this stave side
        # only elink egroup epath are the connectivity on the stave side
        # but!
        # 1 stave side can have 1 or 2 RX fibers (and always only 1 tx fiber)
        # let's assume its current GBT links are always an odd + an even
        rx_name = 'rx_pri' # "pri" for primary lpGBT, it is the one with both tx and rx
        if is_short_strip and cur_rx_connectivity.gbt_link % 2 == 1: # TODO: this is wrong!! in the current fiber mapping not all sec lpGBT are on odd GBT links
            rx_name = 'rx_sec' # the secondary lpGBT RX fiber, this lpGBT does not have a TX fiber, and it is controlled by the primary lpGBT

        side_conf.setdefault(rx_name, {'uuid': str(uuid.uuid4()), 'elinks': {}})
        side_conf[rx_name]['elinks'][(cur_rx_connectivity.elink, itsdaq_number)] = {
            'egroup': cur_rx_connectivity.egroup, 'epath': cur_rx_connectivity.epath} #, 'itsdaq_number': itsdaq_number

        cur_tx_connectivity = conn.translate_Felixcore_TX_elink(raw_tx)
        # namedtuple("ReferenceHCCFelixTX",  "device gbt_link raw_elink elink tx_group encoder_id swapped")
        # a stave side always has only 1 tx fiber
        side_conf.setdefault('tx_pri', {'uuid': str(uuid.uuid4()), 'elinks': {}}) # to connect it under FELIX tree
        side_conf['tx_pri']['elinks'][(cur_tx_connectivity.elink, itsdaq_number)] = {
                           'tx_group': cur_tx_connectivity.tx_group,
                            'encoder_id': cur_tx_connectivity.encoder_id,
                            'swapped': cur_tx_connectivity.swapped}
                            #'itsdaq_number': itsdaq_number

        # let's save the elinks in the individual chip configs so that it is easier to use later
        fe_config['rx_elink'] = (rx_name, cur_rx_connectivity.elink) # yeah
        fe_config['tx_elink'] = cur_tx_connectivity.elink

        # let's also generate the UUID for the runkey references
        side_conf['FEs'][itsdaq_number] = str(uuid.uuid4()), fe_config

  return input_stave_configs
'''
now everything is saved in input_stave_configs as

stave_name:
  chipType: <>
  side_[m|s]:
    tx:
      elink: {tx_group, encoder_id, swapped, itsdaq_number}
    rx_[0|1]:
      elink: {egroup, epath, itsdaq_number}
    FEs:
      <itsdaq_number>:
        (uuid, {module_number, hybrid_name, config, enable, locked, rx_elink, tx_elink})

not sure if it is perfect, but should be close enough
'''
# this goes into the runkey dict as
#with open(config_file_path_hcc + 'CERNSR1_stave3_testing_hcc27.json') as file:
#    hcc27_config_file = file.read()
# the runkey structure looks like this:
#   "children": [
#     {
#       "type": "hcc", # -- no "hcc0", the number goes to "itsdaq_number"
#       "payloads": [
#         {"type": "config", "data": hcc0_config_file},
#         {"type": "txrx", "data": {"tx": 464, "rx": 384}, "meta": True},
#         {"type": "itsdaq_number", "data": <int number?>, "meta": True}
#       ]
#     },


#runkey_staves_configs = runkey_dict['children'][0]
#assert runkey_staves_configs['type'] == 'Staves'

def build_a_runkey_from_std_cfgs(input_stave_configs, database):
  print("build_a_runkey_from_std_cfgs() function executing...")
  startTime = time.time()
  
  runkey_staves_configs = {"type": "Staves", "payloads": [], "children": []}
  # -->
  # and fill the children with the configs

  for stave_name, stave_cfg in input_stave_configs.items():
    a_stave_runkey = RunkeyStave(stave_name, stave_cfg['staveType'], stave_uuid='unknown', chip_type=stave_cfg['chipType'])
    runkey_staves_configs['children'].append(a_stave_runkey)
    # -->
    stavePayloads = [
        {"type": "staveName", "data": dumps({"staveName": stave_name}), "meta": True},
        {"type": "staveType", "data": dumps({"staveType": stave_cfg['staveType']}), "meta": True},
        {"type": "chipType",  "data": dumps({"chipType": stave_cfg['chipType']}), "meta": True}
    ]

    staveNode  = database.add_node(mytype="Stave", payloads=stavePayloads, parents=[{"id":stavesNode}], children=[], stage=False)
    staveSideM = database.add_node(mytype="StaveSide", payloads=[{"type":"sideType", "data":dumps({"sideType": "side_m"}), "meta":True}], parents=[{"id":staveNode}], children=[], stage=False)
    staveSideS = database.add_node(mytype="StaveSide", payloads=[{"type":"sideType", "data":dumps({"sideType": "side_s"}), "meta":True}], parents=[{"id":staveNode}], children=[], stage=False)
    stavesList = database.search(stavesNode, object_type="StaveSide")#, config_type="StaveSide") 

    

    # fill the sides configs
    for rkey_side, side_config in [(a_stave_runkey.side_m, stave_cfg['side_m']), (a_stave_runkey.side_s, stave_cfg['side_s'])]:
        # Choose the correct side of stave
        if rkey_side['payloads'][0]['data'] == 'side_m': currentSide = staveSideM
        else: currentSide = staveSideS
        # the FEs configs:
        # itsdaq_number: (uuid, {module_number, hybrid_name, config, enable, locked, rx_elink, tx_elink})
        for itsdaq_number, (fe_uuid, fe_cfg) in side_config.get('FEs', {}).items():
            # the HCC node class is not made to shortcut anything really
            # it is the other way around: we want to be explicit about this node
            # because it is so important for operations
            # E.g. here it is clear what is converted into strings, and it is done in one place. 
            fe_runkey = RunkeyHCC(str(itsdaq_number),
                                  str(fe_cfg['module_number']),
                                  str(fe_cfg['hybrid_name']),
                                  [str(i) for i in fe_cfg['rx_elink']], # TODO: figure out if configDB payloads allow having a list as data!
                                  str(fe_cfg['tx_elink']),
                                  str(fe_cfg['config']),
                                  enable=str(fe_cfg['enable']), locked=str(fe_cfg['locked']))
            # -->
            hccPayloads = [
                {"type": "itsdaq_number", "data": dumps({"itsdaq_number": str(itsdaq_number)}), "meta": True},
                {"type": "module_number", "data": dumps({"module_number": fe_cfg['module_number']}), "meta": True},
                {"type": "hybrid_name", "data": dumps({"hybrid_name": fe_cfg['hybrid_name']}), "meta": True},
                {"type": "rx_elink", "data": dumps({"rx_elink": fe_cfg['rx_elink']}), "meta": True},
                {"type": "tx_elink", "data": dumps({"tx_elink": fe_cfg['tx_elink']}), "meta": True},
                {"type": "config", "data": dumps({"config": fe_cfg['config']}), "meta": True},
                {"type": "enable", "data": dumps({"enable": fe_cfg['enable']}), "meta": True},
                {"type": "locked", "data": dumps({"locked": fe_cfg['locked']}), "meta": True}
            ]
            hccNode = database.add_node(mytype="HCC", payloads=hccPayloads, parents=[{"id":currentSide}], children=[], stage=False)
            
            # TODO: enable and locked are part of the info for the scan in our configs
            #       I keep them here just for consistency. We need to figure out where to keep them and remove them from here.
            rkey_side['children'].append(fe_runkey)

        # under TX in side config: (elink, itsdaq_number): {tx_group, encoder_id, swapped}
        if 'tx_pri' in side_config:
            rkey_tx = RunkeyNode('GBT_TX_Fiber')
            rkey_tx.add_payload('staveName', stave_name)
            rkey_tx.add_payload('chipType',  get_node_payload(a_stave_runkey, 'chipType'))
            rkey_side['children'].append(rkey_tx)
            # -->
            TX_payloads = [{"type":"staveName", "data":dumps(stave_name), "meta":True}, {"type":"chipType", "data":dumps(stave_cfg['chipType']), "meta":True}]
            GBT_TX_Fiber_Node = database.add_node(mytype="GBT_TX_Fiber", payloads=TX_payloads, parents=[{"id":currentSide}], children=[], stage=False)

            for (elink, itsdaq_number), more_info in side_config['tx_pri']['elinks'].items():
                rkey_elink = RunkeyNode('elink', {'elink_number': str(elink)})
                # and just load the rest
                load_payloads_from_dict(rkey_elink, more_info)
                rkey_tx['children'].append(rkey_elink)
                # -->
                elinkPayloads = [
                    {"type": "elink_number", "data": dumps(elink), "meta": True},
                    {"type": "tx_group", "data": dumps(more_info['tx_group']), "meta": True},
                    {"type": "encoder_id", "data": dumps(more_info['encoder_id']), "meta": True},
                    {"type": "swapped", "data": dumps(more_info['swapped']), "meta": True}
                ]
                elinkNode = database.add_node(mytype="elink", payloads=elinkPayloads, parents=[{"id":GBT_TX_Fiber_Node}], children=[], stage=False)

                # The elink leads to the HCC. Find the corresponding HCC and duplicate it here
                hcc = get_subnode_of_type_with_payload(rkey_side, 'HCC', 'itsdaq_number', str(itsdaq_number))
                rkey_elink['children'].append(hcc.make_reference_clone())

                # --> add the reference to the HCC node TODO: (below) Fix this, make it reference the hccNode
                searchDict = {"itsdaq_number":str(itsdaq_number)}  
                hccID = database.search(currentSide, object_type="HCC", config_type="itsdaq_number", search_dict=searchDict)
                hccNode = database.add_node(mytype="HCC", payloads=[], parents=[{"id":GBT_TX_Fiber_Node}], children=[hccID[0]], stage=False)
   
        for rx_fiber_name, rx_conf in [(c, side_config[c]) for c in ('rx_pri', 'rx_sec') if c in side_config]: # TODO yeah, all of this is very brittle and probably won't work for petals
            rkey_rx = RunkeyNode('GBT_RX_Fiber', {'rx_fiber_type': rx_fiber_name})
            rkey_rx.add_payload('staveName', stave_name)
            rkey_side['children'].append(rkey_rx)
            # -->
            RX_payloads = [
                {"type":"staveName", "data":dumps(stave_name), "meta":True}, 
                {"type":"rx_fiber_type", "data":dumps({'rx_fiber_type': rx_fiber_name}), "meta":True}
            ]
            GBT_RX_Fiber_Node = database.add_node(mytype="GBT_RX_Fiber", payloads=RX_payloads, parents=[{"id":currentSide}], children=[], stage=False)

            # fill the elinks under an RX fiber of a side: (elink, itsdaq_number): {egroup, epath}
            for (elink, itsdaq_number), more_info in rx_conf['elinks'].items():
                rkey_elink = RunkeyNode('elink', {'elink_number': str(elink)})
                load_payloads_from_dict(rkey_elink, more_info)
                rkey_rx['children'].append(rkey_elink)
                # -->
                elinkPayloads = [{"type":"elink_number", "data":dumps(elink), "meta":True}, 
                                 {"type":"egroup", "data":dumps(more_info['egroup']), "meta":True}, 
                                 {"type":"epath", "data":dumps(more_info['epath']), "meta":True}]                               
                elinkNode = database.add_node(mytype="elink", payloads=elinkPayloads, parents=[{"id":GBT_RX_Fiber_Node}], children=[], stage=False)

                # the elink leads to the HCC
                # find the corresponding HCC and duplicate it here
                hcc = get_subnode_of_type_with_payload(rkey_side, 'HCC', 'itsdaq_number', str(itsdaq_number))
                rkey_elink['children'].append(hcc.make_reference_clone())
                # --> TODO: Fix this refernce like above
                searchDict = {"itsdaq_number":str(itsdaq_number)}  
                hccID = database.search(currentSide, object_type="HCC", config_type="itsdaq_number", search_dict=searchDict) #TODO: Search subtree? + reference
                hccNode = database.add_node(mytype="HCC", payloads=[], parents=[{"id":GBT_RX_Fiber_Node}], children=[hccID[0]], stage=False)


  print("build_a_runkey_from_std_cfgs() function time:", time.time() - startTime, "\n \n")
  return runkey_staves_configs

# Done with the staves configs. Now we need the references to the fibers in the FELIX tree
# https://itk-strips-system-tests-sr1.docs.cern.ch/mapping/#physical-location-of-staves
current_felix_geometry_mapping = {
        'PPB1B': StripsGeometryCoordinate('BSS', 1, 1),
        'PPB1A': StripsGeometryCoordinate('BSS', 1, 2),
        'PPB1C': StripsGeometryCoordinate('BSS', 1, 3),
        'PPB1D': StripsGeometryCoordinate('BSS', 2, 1)
}

def connect_staves(runkey_dict, fiber_mapping, current_felix_geometry_mapping, database): 
    staves_tree = get_subnode_of_type(runkey_dict, "Staves")
    felix_tree  = get_subnode_of_type(runkey_dict, "Felix")
    geom_tree   = get_subnode_of_type(runkey_dict, "Geometry")

    # -->
    stavesList = database.search(stavesNode, object_type="Stave", config_type="Stave")
    for stave in stavesList:
        staveName = database.search(stave['id'], object_type="Stave", config_type="staveName")[0]['payloads'][0]['data'][15:-2]
        stave_coord = current_felix_geometry_mapping[staveName] #TODO: there must be a better way...

        # Triple search: Find the geometry slot where the stave is located, and add the stave as a child
        # TODO: add_to_node() function, reference/reuse_id
        geomSection = database.search(geometryNode, object_type="Section", config_type="coordinate", search_dict={"coordinate":str(stave_coord.section)})
        geomLayer   = database.search(geomSection[0]['id'], object_type="Layer", config_type="coordinate", search_dict={"coordinate":str(stave_coord.layer)})
        geomSlot    = database.search(geomLayer[0]['id'], object_type="Slot", config_type="coordinate", search_dict={"coordinate":str(stave_coord.slot)})
        
        nodeTime = time.time()
        database.add_node(mytype="Stave", payloads=[], parents=[{"id":geomSlot[0]['id']}], children=[stave], stage=False) #TODO: make reference+ search subtree? add_to_node?? (may need to add_to_node(payload={'reuse_id'=}))
        print("add_node() function time:", time.time() - nodeTime, "\n \n")
        # aaa
        for staveSide in database.search(stave['id'], object_type="StaveSide"):
            # If a stave side is empty, it will be skipped
            print("side:", staveSide)
            side_x = staveSide['payloads'][0]['data'][14:-2]
            tx_fiber = database.search(staveSide['id'], object_type="GBT_TX_Fiber")
            tx_dev_num, tx_gbt_num = fiber_mapping[staveName][side_x]['tx_pri']
            #rkey_flx_device = get_subnode_with_payload(felix_tree, 'FID_CONTROLLER_ID', str(tx_dev_num))
            #rkey_tx_fiber   = get_subnode_of_type_with_payload(rkey_flx_device, 'GBT_TX', 'GBT_LINK', str(tx_gbt_num))
            #rkey_tx_fiber['children'].append(tx_fiber.make_reference_clone())

            flx_device = database.search(felixNode, object_type="FID_CONTROLLER_ID", config_type="FID_CONTROLLER_ID", search_dict={"FID_CONTROLLER_ID":str(tx_dev_num)})
            rk_tx_fiber = database.search(flx_device[0]['id'], object_type="GBT_TX", config_type="GBT_LINK", search_dict={"GBT_LINK":str(tx_gbt_num)})
            # TODO: add_to_node() function, reference/reuse_id
            nodeTime = time.time()
            database.add_node(mytype="GBT_TX", payloads=[], parents=[{"id":rk_tx_fiber[0]['id']}], children=[tx_fiber[0]], stage=False)
            print("add_node() function time:", time.time() - nodeTime, "\n \n")

            # the same for RXes. rx primary:
            rx_fiber = database.search(staveSide['id'], object_type="GBT_RX_Fiber", config_type="rx_fiber_type", search_dict={"rx_fiber_type":"rx_pri"})
            rx_dev_num, rx_gbt_num = fiber_mapping[staveName][side_x]['rx_pri']
            flx_device = database.search(felixNode, object_type="FID_CONTROLLER_ID", config_type="FID_CONTROLLER_ID", search_dict={"FID_CONTROLLER_ID":str(rx_dev_num)})
            rk_rx_fiber = database.search(flx_device[0]['id'], object_type="GBT_RX", config_type="GBT_LINK", search_dict={"GBT_LINK":str(rx_gbt_num)})
            database.add_node(mytype="GBT_RX", payloads=[], parents=[{"id":rk_rx_fiber[0]['id']}], children=[rx_fiber[0]], stage=False)

            rx_sec_fiber = database.search(staveSide['id'], object_type="GBT_RX_Fiber", config_type="rx_fiber_type", search_dict={"rx_fiber_type":"rx_sec"})
            if rx_sec_fiber:
                rx_dev_num, rx_gbt_num = fiber_mapping[staveName][side_x]['rx_sec']
                rkey_flx_device = database.search(felixNode, object_type="FID_CONTROLLER_ID", config_type="FID_CONTROLLER_ID", search_dict={"FID_CONTROLLER_ID":str(rx_dev_num)})
                rk_rx_fiber = database.search(flx_device[0]['id'], object_type="GBT_RX", config_type="GBT_LINK", search_dict={"GBT_LINK":str(rx_gbt_num)})
                database.add_node(mytype="GBT_RX", payloads=[], parents=[{"id":rk_rx_fiber[0]['id']}], children=[rx_sec_fiber[0]], stage=False) # TODO: add_to_node() function, reference/reuse_id
            else: 
                if 'rx_sec' in fiber_mapping[staveName][side_x]:
                    logging.warning(f'the runkey does not have an rx_sec but the mapping has it: stave {staveName} side {side_x}')



    # ^^^ should replace this loop below vvv
    # loop over staves, create the connectivity views according to the mappings
    for rkey_stave in staves_tree['children']:
        staveName = get_node_payload_data(rkey_stave, 'staveName')

        # Grabs geometry and stave name (above), triple search, adds/refs relevant Stave to geom slot (first do dupe, then ref)
        stave_coord = current_felix_geometry_mapping[staveName]
        rkey_geom_section = get_subnode_with_payload(geom_tree,         'coordinate', stave_coord.section)
        rkey_geom_layer   = get_subnode_with_payload(rkey_geom_section, 'coordinate', stave_coord.layer)
        rkey_geom_slot    = get_subnode_with_payload(rkey_geom_layer,   'coordinate', stave_coord.slot)
        # slot <- layer <- section <- geometry
        rkey_geom_slot['children'].append(rkey_stave.make_reference_clone())

        for rkey_side in rkey_stave['children']:
            if not rkey_side['children']:
                # it is an empty side, for whatever reason
                continue

            sideType = get_node_payload_data(rkey_side, 'sideType')
            tx_fiber = get_subnode_of_type(rkey_side, 'GBT_TX_Fiber')
            #tx_uuid = tx_fiber['id']
            # connect it to Felix tree where this stave side sits:
            tx_dev_num, tx_gbt_num = fiber_mapping[staveName][sideType]['tx_pri']

            # the fiber runkey node
            rkey_flx_device = get_subnode_with_payload(felix_tree, 'FID_CONTROLLER_ID', str(tx_dev_num))
            rkey_tx_fiber   = get_subnode_of_type_with_payload(rkey_flx_device, 'GBT_TX', 'GBT_LINK', str(tx_gbt_num))
            # connect the corresponding stave fiber: add the reference as a child
            #rkey_tx_fiber['children'].append({'id': tx_uuid}) # no, it does not work like this
            # make a reference-clone of the stave fiber node
            # hcc.make_reference_clone()
            rkey_tx_fiber['children'].append(tx_fiber.make_reference_clone())
         

            # the same for RXes
            # rx primary
            rx_pri_fiber = get_subnode_of_type_with_payload(rkey_side, 'GBT_RX_Fiber', 'rx_fiber_type', 'rx_pri')
            rx_dev_num, rx_gbt_num = fiber_mapping[staveName][sideType]['rx_pri']
            rkey_flx_device = get_subnode_with_payload(felix_tree, 'FID_CONTROLLER_ID', str(rx_dev_num))
            rkey_rx_fiber   = get_subnode_of_type_with_payload(rkey_flx_device, 'GBT_RX', 'GBT_LINK', str(rx_gbt_num))
            # connect the stave rx fiber
            #rkey_rx_fiber['children'].append({'id': rx_pri_uuid}) # no, it does not work like this
            rkey_rx_fiber['children'].append(rx_pri_fiber.make_reference_clone())

            # and test for the secondary
            rx_sec_fiber = get_subnode_of_type_with_payload(rkey_side, 'GBT_RX_Fiber', 'rx_fiber_type', 'rx_sec')
            if rx_sec_fiber is None:
                if 'rx_sec' in fiber_mapping[staveName][sideType]:
                    logging.warning(f'the runkey does not have an rx_sec but the mapping has it: stave {staveName} side {sideType}')

            else:
                rx_dev_num, rx_gbt_num = fiber_mapping[staveName][sideType]['rx_sec'] # rx_sec must be in the fiber mapping
                rkey_flx_device = get_subnode_with_payload(felix_tree, 'FID_CONTROLLER_ID', str(rx_dev_num))
                rkey_rx_fiber   = get_subnode_of_type_with_payload(rkey_flx_device, 'GBT_RX', 'GBT_LINK', str(rx_gbt_num))
                #rx_sec_uuid = rx_sec_fiber['id']
                #rkey_rx_fiber['children'].append({'id': rx_sec_uuid}) # no, it does not work like this
                rkey_rx_fiber['children'].append(rx_sec_fiber.make_reference_clone())
                

def build_from_template(config_dir, connectivity_files, current_felix_fiber_mapping, runkey_dict_template, database):
    # the runkey we work with here
    # you can deepcopy the template
    from copy import deepcopy
    runkey_dict = deepcopy(runkey_dict_template)
    
    #########################
    # --> 
    # Create a RK with Felix and Geometry, before adding Staves
    # Felix 
        # We should have multiple FELIX cards under here. 
        # FLX_DEVICE_NUM -> FID_CONTROLLER_ID
    felixDevice0 = database.add_node(mytype="FID_CONTROLLER_ID", payloads=[{"type": "FID_CONTROLLER_ID", "data": dumps({"FID_CONTROLLER_ID":"0"}), "meta": True}], parents=[{"id":felixNode}], children=[], stage=False)
    felixDevice1 = database.add_node(mytype="FID_CONTROLLER_ID", payloads=[{"type": "FID_CONTROLLER_ID", "data": dumps({"FID_CONTROLLER_ID":"1"}), "meta": True}], parents=[{"id":felixNode}], children=[], stage=False)
    for i in range(12):
        # FelixDevice0
        database.add_node(mytype='GBT_RX', payloads=[{'type': 'GBT_LINK', 'data': dumps({"GBT_LINK":str(i)}), "meta": True}], parents=[{'id': felixDevice0}], children=[], stage=False)
        database.add_node(mytype='GBT_TX', payloads=[{'type': 'GBT_LINK', 'data': dumps({"GBT_LINK":str(i)}), "meta": True}], parents=[{'id': felixDevice0}], children=[], stage=False)
        # FelixDevice1
        database.add_node(mytype='GBT_RX', payloads=[{'type': 'GBT_LINK', 'data': dumps({"GBT_LINK":str(i)}), "meta": True}], parents=[{'id': felixDevice1}], children=[], stage=False)
        database.add_node(mytype='GBT_TX', payloads=[{'type': 'GBT_LINK', 'data': dumps({"GBT_LINK":str(i)}), "meta": True}], parents=[{'id': felixDevice1}], children=[], stage=False)
    # Geometry
    geometrySection = database.add_node(mytype="Section", payloads=[{"type": "coordinate", "data": dumps({"coordinate":"BSS"}), "meta": True}], parents=[{"id":geometryNode}], children=[], stage=False)
    geometryLayer1 = database.add_node(mytype="Layer", payloads=[{"type": "coordinate", "data": dumps({"coordinate":"1"}), "meta": True}], parents=[{"id":geometrySection}], children=[], stage=False)
    geometryLayer2 = database.add_node(mytype="Layer", payloads=[{"type": "coordinate", "data": dumps({"coordinate":"2"}), "meta": True}], parents=[{"id":geometrySection}], children=[], stage=False)
    for i in range(1,6):
        if i < 4:
            database.add_node(mytype='Slot', payloads=[{'type': 'coordinate', 'data': dumps({"coordinate":str(i)}), "meta": True}], parents=[{'id': geometryLayer1}], children=[], stage=False)
        database.add_node(mytype='Slot', payloads=[{'type': 'coordinate', 'data': dumps({"coordinate":str(i)}), "meta": True}], parents=[{'id': geometryLayer2}], children=[], stage=False)
    #########################

    if not connectivity_files:
        connectivity_files = std_stave_connectivity_files

    # load_std_configs(dir_with_configs, stave_connectivity_files):
    # connectivity_files must be a list of <stave_name>:<file_path>
    connectivity_info = {}
    for c_file in connectivity_files:
        stave_name, file_path = c_file.split(':')
        connectivity_info[stave_name] = file_path

    # load the files into the temporary structure
    std_confs = load_std_configs(config_dir, connectivity_info)
    # append the staves tree under the template runkey
    runkey_dict['children'].append(build_a_runkey_from_std_cfgs(std_confs, database))
    # -->
    #build_a_runkey_from_std_cfgs(std_confs, database) # this does this^^^. Will delete above line, hopefully this doesn't happen twice

    # and connect the staves inside the runkey to the Felix fibers
    # according to the mapping
    connect_staves(runkey_dict, current_felix_fiber_mapping, current_felix_geometry_mapping, database)

    return runkey_dict



def parse_args():
    """
    -d = debug, -c = config_dir, --print = pretty print the runkey, --write = write the runkey back in files, --load = load the runkey from a JSON file
    Example:
    python created_runkeys/formattedRunkey_comline.py --print
    python created_runkeys/formattedRunkey_comline.py --print -c ../../temp_stave_configs
    python created_runkeys/formattedRunkey_comline.py --print -c ../../temp_stave_configs PPB1D:CERNSR1_stave3_testing_hcc27.json
    """
    parser = argparse.ArgumentParser(
        formatter_class = argparse.RawDescriptionHelpFormatter,
        description = "load a Runkey or pretty-print it with --print",
        epilog = """Example:
    python created_runkeys/formattedRunkey_comline.py --print
    python created_runkeys/formattedRunkey_comline.py --print -c ../../temp_stave_configs
    python created_runkeys/formattedRunkey_comline.py  --load db_configs/temp_sr1_runkeys/full-sr1_copies_blob-payloads_2024-05-31_no-id-in-payloads.json
    
    # Create RK with everything
    python casey_formattedRunkey_comline.py -c ~/.local/opt/atlas/etc/stave_configs/temp_stave_configs_2024-03/ 

    # PRINT only 1 stave (Stave0)
    python casey_formattedRunkey_comline.py --print -c ~/.local/opt/atlas/etc/stave_configs/temp_stave_configs_2024-03/ PPB1A:CERNSR1_stave0_testing_setup.json

    # Load ex with 1 Stave, a side has 2 hcc's (reuse config file payload)
    python casey_formattedRunkey_comline.py --load ../created_runkeys/rk73_reusePayloads_newFW_preRefsFix.json 
    """
    )

    parser.add_argument('-d', '--debug',  action='store_true', help="DEBUG level of logging")
    parser.add_argument('-c', '--config-dir', type=str, default=default_config_dir, help=f"path to the configs to load as a runkey, default is {default_config_dir}")
    parser.add_argument('--print', action='store_true', help=f"just pretty print the runkey")
    parser.add_argument('--write', type=str, help=f"write the runkey back in files")
    parser.add_argument('--load',  type=str, help="if given, just load the runkey from a JSON file")
    parser.add_argument('connectivity_files', type=str, nargs='*',
                    help='the filenames of YARR connectivity jsons, if non given, use the defaults for PPB1[ABCD]')
    
    return parser.parse_args()

# End of Alex's formattedRunkey_comline.py functions
############################################################################################################
############################################################################################################



# One can either create a runkey layer by layer or create a complete runkey at once
# Layer by layer:
# - Gets URL of configdb from service-registry
# - Creates new root-node in staging eara
# - Creates felix, optoboard and fe objects (and their configuration files as payloads)
# - Commits runkey (from staging area to backend)
# - Reads runkey using its tag

# It is also possible to create the entire runkey at once from a python dictionary
def write_runkey_full(url, runkey_dict):
    print("\nwrite_runkey_full() function executing...")
    # Entire runkey specified as python dictionary 
    # Every entry has a type, payloads and children
    # Runkey is created in conversions.py file

    # Send this runkey to the staging area
    dic = {"data": runkey_dict}
    response = post(dic, url, "/stage/create")
    # Get id of root node from response
    if 'id' not in response:
        raise TypeError(f'failed response: {response["status"]} -- {response["detail"][:]}')
    else:
        root_id = response["id"]

    return root_id

def commit_runkey(url, root_id, name):
    # Runkey is finished, commit it
    # Use root_id as identifier for runkey
    dic = {"identifier": root_id, "new_name": name, "author": "example_script"}
    response = post(dic, url, "/stage/commit")
    print("Commited runkey:", name)

def read_runkey(url, name):
    """ Read the whole runkey from the database, and dump it to a json file """
    print("\nread_runkey() function executing...")
    # Read runkey from database
    # Can use root_id as identifier for runkey
    dic = {"stage": False, "name": name, "payload_data": True}
    runkey = post(dic, url, "/tag/tree")

    with open(f"{os.path.dirname(__file__)}/../dumpedFiles/{name}.json", 'w') as f:
        json.dump(runkey, f, indent=4)
        print(f"    Runkey written to file {name}.json")


def return_payload_ref(url, hashID):
    ''' 
    return_payload_ref(url, hashID)

    Uses /payload/read to return the payload reference
    '''
    print("\nreturn_payload_ref() function executing...")
    dic = {"stage": False, "id": hashID}
    response = post(dic, url, "/payload/read")
    #payload_ref = response["data"]
    print("response:", response)
 
    #return payload_ref
    

def post(body, url, endpoint):
    """
    Sends HTTP post request to server using con as body
    Prints the status of the response to console

    Parameters
    ----------
    body: dictionary
        python dictionary that is used as HTTP-body (for parameters see documentation)
    url: str
        string containing database URL
    endpoint: str
        OpenAPI endpoint to post to

    Returns
    -------
    dictionary
        Dictionary containing the properties received from the HTTP-response
    """

    headers = {"content-type": "application/json", "accept": "application/json"}

    try:
        # Sends HTTP post request to server using body as http-body
        res = http_post(url + endpoint, data=dumps(body), headers=headers)

        # If the response is ok, print the status of the response to console
        con = loads(res.content)
        if res.ok:
            print(f"    {endpoint} endpoint, {con['status']}: Successful")
            return con
        else:
            print(f"    {endpoint} endpoint, {con['status']}: {con['title']}")
            return con
    except ConnectionError:
        print(f"Error, connection to itk_demo_configdb failed. Is the itk_demo_configdb-server running at {url}?")
    exit()





def search_and_return_object(url, config_type, identifier, object_type, search_dict, return_type): #leave as strings here, or extract below?
    ''' 
    Search function. Searches for dic, and returns _only_ the found object. No children. 
    NOTE: Search functions must have payloads' meta: True
    return_type = "id", "json", "print"
    To return a whole file, search_dict = {}
    '''
    print("\nsearch_and_return_object() function executing...")
    # Working dics (if meta: True)
    #dic = {"config_type": "config", "identifier": config.runkey_name, "object_type": "hcc28", "search_dict": {"rx": "64"}}# or {"HCC.regs.Delay1": "02400000"}}

    # Check if all arguments after url are strings
    if not all(isinstance(arg, str) for arg in [config_type, identifier, object_type, return_type]):
        raise ValueError("All arguments other than url and search_dict must be strings")

    dic = {
        "config_type": config_type,
        "identifier": identifier,
        "object_type": object_type,
        "search_dict": search_dict
    }
    
    response = post(dic, url, "/search")
    payload_files = response["data"]
    payloadUUID = list(payload_files)[0]
    print("    Length of payload_files (search):", len(payload_files))

    if return_type == "json":
        output_file = f"{os.path.dirname(__file__)}/../dumpedFiles/{object_type}_fromRK.json"
        if object_type == "lpGBT":
            output_file = f"{os.path.dirname(__file__)}/../dumpedFiles/{object_type}_fromRK.cnf"
            
        with open(output_file, 'w') as f:
            # Convert payload_files to JSON string and load it as a dictionary
            payload_files_dict = {payloadUUID: payload_files[payloadUUID]}
            unformatted_data = payload_files_dict[payloadUUID][0]['data']
            f.write(unformatted_data)

        print(f"    Object written to {output_file[47:]}")
    elif return_type == "print":
        print("    Object:", payload_files)
    elif return_type == "id":
        print ("    ID: ", payloadUUID)
        return payloadUUID





def search_and_return_subtree(url):
    ''' search/subtree function. Searches for dic, and returns all children of the found object
    NOTE: Search functions must have payloads' meta: True'''

    '''
    TODO: Fix the def to be like this: 
    def search_and_return_object(url, config_type, identifier, object_type, search_dict, return_type):
    '''
    print("\nsearch_and_return_subtree() function executing...")
    # Read runkey from database \"lpGBT_config_file\"
    dic = {
        "identifier": config.runkey_name, 
        "object_type": "lpGBT", 
        "search_dict": {
            "name" : "lpGBT_config_file"
        }
    }
           #"search_dict": {"name" : "value"}}

    response = post(dic, url, "/search/subtree")
    # Get files from response
    payload_files = response["data"]

    print("    Length of payload_files (subtree search):", len(payload_files))
    payload_files_str = json.dumps(payload_files)
    print(payload_files_str[:200])

    with open(f"{os.path.dirname(__file__)}/TESTsubtreesearch.json", 'w') as f:
        json.dump(payload_files, f, indent=4)
        print("    Branch only written to file TESTsubtreesearch.json")



def increment_runkey_name(): # TODO: Fix. Not working. Might work better with pypyconfig package
    ''' 
    increment_runkey_name()

    Increments the runkey name by one by changing ./config.py
    '''
    new_name = config.runkey_name
    print("Current runkey name:", new_name)
    user_input = input("Type 'y' and hit enter to increment the runkey name: ")
    if user_input.lower() == 'y':
        with open(f"{dir_path}/config.py", 'r+') as f:
            lines = f.readlines()
            for i, line in enumerate(lines):
                if line.strip().startswith("runkey_name"):
                    current_name = line.split("=")[1].strip().strip('"')
                    new_name = current_name[:-2] + str(int(current_name[-2:]) + 1)
                    lines[i] = f'    runkey_name: str = "{new_name}"\n'
                    break
            f.seek(0)
            f.writelines(lines)
            f.truncate()
        print(f"    Runkey name incremented to {new_name}")
        exit()
    else:
        print("    Runkey name not incremented.")

