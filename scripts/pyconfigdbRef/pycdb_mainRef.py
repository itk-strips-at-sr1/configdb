#!/usr/bin/env python
# SKELETON CONFIGURATION
import os, json
from json import dumps, loads

import logging
from pathlib import Path # to get file names in a directory with Path(directory).glob('CERNSR1*testin_hcc*.json') 
import configDB_connectivity as conn
import functions_configDB as fxt
from configdb_stripsRunkeys import *
import uuid
import time

start_time = time.time()

CONFIGDB_KEY = "demi/default/itk-demo-configdb/api/url"  # name of itk-demo-configdb instance
dir_path = os.path.dirname(os.path.realpath(__file__))
config_file_path = dir_path + "/../db_configs/SR1_config_files/"  # path to config-files
config_file_path_hcc = dir_path + "/../../temp_stave_configs/" #"/../db_configs/yarr-star-fe-configs/"  # path to config-files

default_config_dir = os.path.dirname(os.path.realpath(__file__)) + "/../../temp_stave_configs/"


############################################################################################################
try:
    import pyconfigdb
except ImportError:
    print("Error: If this is unsuccessful, make sure you are in the right .venv")
configDB = pyconfigdb.ConfigDB(dbKey=CONFIGDB_KEY)
configDB.name = "pyconfigdbtest21" #aaa
print("configDB.name:", configDB.name)

configDB.stage_create(data={"type": "Root"}, name=configDB.name)

rootNode     = configDB.add_node(mytype="Root", payloads=[], parents=[], children=[], stage=False)
stavesNode   = configDB.add_node(mytype="Staves", payloads=[], parents=[{"id":rootNode}], children=[], stage=False)
felixNode    = configDB.add_node(mytype="Felix", payloads=[{"type": "FID_DETECTOR_ID", "data": dumps("0")}], parents=[{"id":rootNode}], children=[], stage=False)
geometryNode = configDB.add_node(mytype="Geometry", payloads=[], parents=[{"id":rootNode}], children=[], stage=False)

fxt.set_rootNode(rootNode)
fxt.set_viewNodes(stavesNode, geometryNode, felixNode)


# Read felix config from file
with open(config_file_path + "felix_yarr_default.json") as file:
    felix_config_file = file.read()

# the HCC configs
# txrx come from the connectivity config
# and the chip configs come from individual json files
# let's just parse the files into the following 2 dictionaries:
hcc_configs = {} # a dictionary of stave_name: {hcc_itsdaq_num: payload}
hcc_connectivity = {} # a dictionary of stave_name: {itsdaq_num: {tx: <>, rx: <>}}

# we should eventually combine the connectivity and the configs into a dictionary
# that is similar to the runkey structure:
# stave_name: {"side_m": {}, "side_s": {}}
# a dictionary of stave_name: {'rx': {rx_raw, gbt_num, elink, egroup, epath}, 'tx': {tx_raw, gbt_num, group, swapped}}

# let's just load the configs as is
# I assume the connectivity and configs files are in this dir:
# config_file_path_hcc

std_stave_connectivity_files = [
    'PPB1A:CERNSR1_stave0_testing_setup.json',
    'PPB1B:CERNSR1_stave1_testing_setup.json',
    'PPB1C:CERNSR1_stave2_testing_setup.json',
    'PPB1D:CERNSR1_stave3_testing_setup.json',
]


# Entire runkey specified as python dictionary (same runkey as in other example)
# Every entry has a type, payloads and children
    # FID is the global FELIX ID for each elink=frontend in the entire ATLAS
    # it is a 64-bit number, different fields of which mean different "sub-IDs"
    # i.e. there is a DID (Detector ID)
    # we can set it to anything we want in the System Test
    # it is --did 0x00 in your felix-star processes arguments
    # https://gitlab.cern.ch/itk-strips-at-sr1/itk-strips-felix-setup-scripts/-/blob/master/supervisord_standard_2024-02-06.conf?ref_type=heads
           # CID (Controller ID) is another field in the full FID
           # it identifies not a single FELIX CARD, but the logical devices in it
           # e.g. in our felix-star arguments:
           # -d 0 ... --cid 0x0000
           # -d 1 ... --cid 0x0001
           # 1 logical FELIX "device" = 1 DMA channel on the PCIe from the card to the host computer
           # in the System Test we currently use the word "device" and not the "controller"
           # simply because we use only 1 FLX712 card, and it is not ambiguous in this case
           # but we need to start using the full FID terminology for future
runkey_dict_template = {
  "type": "Root",
  "children": [
    {
      "type": "Felix", # we should have multiple FELIX cards under here
      "payloads": [{'type': 'FID_DETECTOR_ID', 'data': "0"},
                   {"type": "config", "data": felix_config_file}], # TODO what is felix_config_file ? is it really the FELIX registers?
      "children": [
          {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': "0"}], # FLX_DEVICE_NUM -> FID_CONTROLLER_ID
           'children': [
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "0"}], 'children': []}, # the children here are references to RX and TX fibers of staves
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "1"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "2"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "3"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "4"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "5"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "6"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "7"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "8"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "9"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data": "10"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data": "11"}], 'children': []},

                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "0"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "1"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "2"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "3"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "4"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "5"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "6"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "7"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "8"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "9"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data": "10"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data": "11"}], 'children': []},
                   ]
           },
          {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': "1"}],
           'children': [
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "0"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "1"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "2"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "3"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "4"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "5"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "6"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "7"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "8"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data":  "9"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data": "10"}], 'children': []},
                   {'type': 'GBT_RX', "payloads": [{'type': 'GBT_LINK', "data": "11"}], 'children': []},

                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "0"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "1"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "2"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "3"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "4"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "5"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "6"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "7"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "8"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data":  "9"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data": "10"}], 'children': []},
                   {'type': 'GBT_TX', "payloads": [{'type': 'GBT_LINK', "data": "11"}], 'children': []},
                   ]
           },
      ]
    },
    {
      "type": "Geometry",
      "payloads": [],
      "children": [
        {'type': 'Section', "payloads": [{'type': 'coordinate', 'data': 'BSS'}],
            'children': [
            {'type': 'Layer', 'payloads': [{'type': 'coordinate', 'data': 1}],
                'children': [
                    {'type': 'Slot', 'payloads': [{'type': 'coordinate', 'data': 1}], 'children': []},
                    {'type': 'Slot', 'payloads': [{'type': 'coordinate', 'data': 2}], 'children': []},
                    {'type': 'Slot', 'payloads': [{'type': 'coordinate', 'data': 3}], 'children': []},
                ]
            },
            {'type': 'Layer', 'payloads': [{'type': 'coordinate', 'data': 2}],
                'children': [
                    {'type': 'Slot', 'payloads': [{'type': 'coordinate', 'data': 1}], 'children': []},
                    {'type': 'Slot', 'payloads': [{'type': 'coordinate', 'data': 2}], 'children': []},
                    {'type': 'Slot', 'payloads': [{'type': 'coordinate', 'data': 3}], 'children': []},
                    {'type': 'Slot', 'payloads': [{'type': 'coordinate', 'data': 4}], 'children': []},
                    {'type': 'Slot', 'payloads': [{'type': 'coordinate', 'data': 5}], 'children': []},
                ]
            }
          ]
        }
      ]
    }
  ]
}



# from the docs: https://itk-strips-system-tests-sr1.docs.cern.ch/mapping/#support-structure-fibre-mapping
current_felix_fiber_mapping = {
        'PPB1A': {
         'side_m': {'tx_pri': (0, 7), 'rx_pri': (0, 7), 'rx_sec': (0, 6)},
         'side_s': {'tx_pri': (0, 5), 'rx_pri': (0, 5), 'rx_sec': (0, 4)},
        },

        'PPB1B': {
         'side_m': {'tx_pri': (0,  9), 'rx_pri': (0,  9), 'rx_sec': (0,  8)},
         'side_s': {'tx_pri': (0, 11), 'rx_pri': (0, 11), 'rx_sec': (0, 10)},
        },

        'PPB1C': {
         'side_m': {'tx_pri': (0, 0), 'rx_pri': (0, 0), 'rx_sec': (0, 1)},
         'side_s': {'tx_pri': (0, 2), 'rx_pri': (0, 2), 'rx_sec': (0, 3)},
        },

        'PPB1D': {
         'side_m': {'tx_pri': (1, 2), 'rx_pri': (1, 2)},
         'side_s': {'tx_pri': (1, 3), 'rx_pri': (1, 3)},
        },
    }


# Ask to increase the runkey name by one
#fxt.increment_runkey_name()  

if __name__ == "__main__":
    args = fxt.parse_args()  

    # Logging levels
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    if args.load:
        with open(args.load) as f:
            runkey_dict = json.load(f)
    else: 
        runkey_dict = fxt.build_from_template(args.config_dir, args.connectivity_files, current_felix_fiber_mapping, runkey_dict_template, configDB)

    if args.print: 
        import pprint
        # just pretty print it
        #pprint.pp(json.dumps(runkey_dict))
        print("Printing done, uncomment for actual functionality")
        #configDB.name = "pyconfigdbTest"
        configDB.stage_commit(configDB.name, new_name=configDB.name)
        rk = configDB.tag_tree(configDB.name, stage=False)
        print("rk:", dumps(rk, indent=2))
        #print(json.dumps(runkey_dict, default=dict, indent=2)) # json expects dict (which is incorrect, but that's how it is) # https://stackoverflow.com/questions/70762125/why-is-class-that-extends-dict-json-serializable-but-one-that-extends-collectio
        print("Total time:", time.time() - start_time)
        exit(0)

    if args.write:
        # try to write the files back, with the new namings
        output_dir = args.write
        yarr_connectivity = runkey_fiber_to_yarr_connectivity(get_subnode_of_type(runkey_dict, 'Felix'))
        write_cfg_files(yarr_connectivity, output_dir)
        exit(0)

    # if it is not a demo
    # load the runkey to the configDB
    from service_registry import ServiceRegistry
    from requests import post as http_post
    from config import config
    import json 
    

    # Get configdb-URL from service-registry using the CONFIGDB-KEY
    sr = ServiceRegistry()
    url = sr.get(config.configdb_key)


