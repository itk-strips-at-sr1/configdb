"""
Handy functions to work with a ConfigDB instance over pyconfigdb.
"""
import logging

# and add a new node under it
# ConfigDB does not provide an API to just upload a dictionary
# with payloads and children together.
# you have to create nodes one by one.
# Hence, our helper function:
def add_subtree(configdb, parent_id, subtree: dict, new_id_map={}, to_stage=False):
    assert isinstance(subtree, dict), f'Not a dict: {subtree}'

    if 'reuse_id' in subtree:
        # skip it
        # the references are done with the special workaround: new_id_map
        return

    if 'type' not in subtree:
        raise RuntimeError(f'No "type" in {subtree}')

    # create the root node of this subtree
    subtree_id = configdb.add_node(subtree['type'],
                    payloads=subtree['payloads'],
                    parents=[{'id': parent_id}],
                    children=[],
                    stage=to_stage)

    if 'children' not in subtree:
        raise RuntimeError(f'No "children" in {subtree}')

    # create children nodes and save their IDs
    children_ids = []
    for child_node in subtree['children']:
        add_subtree(configdb, subtree_id, child_node, new_id_map)

    # update the children IDs in the root node
    configdb.add_to_node(subtree_id, children=children_ids, stage=to_stage)

    # update the ID
    current_id = subtree.get('id')
    new_id_map[current_id] = subtree_id
    subtree['id'] = subtree_id

def update_references(configdb, parent_id, subtree: dict, new_id_map={}, to_stage=False):
    if 'reuse_id' in subtree:
        # find the new id of the reference
        new_id = new_id_map[subtree['reuse_id']]
        # add it to the node's children
        configdb.add_to_node(parent_id, children=[new_id], stage=to_stage)
        # and done
        return

    uptodate_id = subtree['id']
    for child_node in subtree['children']:
        update_references(configdb, uptodate_id, child_node, new_id_map, to_stage)

def upload_stave(configDB, runkey_ref, stave_node):
    '''upload_stave(configDB, runkey_ref, stave_node)

    configDB : pyconfigdb.ConfigDB instance
    runkey_ref : str with a runkey ID or a name (latest_runkey)
    stave_node : RunkeyStave instance, a dictionary of that format
    '''

    # get the ID of the "Staves" node
    # TODO: `search` finds _payloads_ not the nodes???
    # we need nodes -- the functions `add_node` need IDs of parent nodes
    #staves_tree_id = configDB.search(runkey_ref, object_type="Staves")
    staves_subtree = configDB.search_subtree(runkey_ref, object_type="Staves")
    logging.debug(f'Found Staves subtree: {staves_subtree}')
    staves_tree_id = staves_subtree[0]['id']
    logging.info(f'Got Staves subtree ID: {staves_tree_id}')
    assert len(staves_tree_id) > 0

    id_mapping = {}
    add_subtree(configDB, staves_tree_id, stave_node, id_mapping)
    update_references(configDB, staves_tree_id, stave_node, id_mapping)
