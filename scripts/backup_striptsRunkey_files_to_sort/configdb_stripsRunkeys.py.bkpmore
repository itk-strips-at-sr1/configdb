'''
A module with typical operations on Runkey nodes in general, and on Strips-specific nodes.

For example:

>>> node = RunkeyNode('FELIX_DEVICE', payloads={'FID_CONTROLLER_ID': 1})
>>> node
{'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': []}
'''

import logging
#from collections import UserDict, namedtuple
from collections import namedtuple
import uuid  # str(uuid.uuid4())
import json

DEBUG_DUPLICATE_PAYLOADS = True # duplicate everything for debugging when ConfigDB handles references


def get_one(a_list: list, assert_one=True):
    if assert_one and len(a_list) != 1:
        raise AssertionError(f'Got a list of len != 1: {a_list}')

    if a_list:
        return a_list[0]
    else:
        return None


def get_subnodes_of_type(runkey_node: dict, type_val: str) -> list:
    return [c for c in runkey_node['children'] if c['type'] == type_val]


def get_subnode_of_type(runkey_node: dict, type_val: str, assert_one=True) -> list:
    res = get_subnodes_of_type(runkey_node, type_val)
    return get_one(res, assert_one)


def get_node_payloads(runkey_node: dict, payload_type: str):
    return [p for p in runkey_node['payloads'] if p['type'] == payload_type]


def get_node_payload(runkey_node: dict, payload_type: str, assert_one=True):
    return get_one(get_node_payloads(runkey_node, payload_type), assert_one)


def get_node_payloads_data(runkey_node: dict, payload_type: str):
    return [p['data'] for p in runkey_node['payloads'] if p['type'] == payload_type]


def get_node_payload_data(runkey_node: dict, payload_type: str, assert_one=True):
    return get_one(get_node_payloads_data(runkey_node, payload_type), assert_one)


def get_nodes_with_payload(runkey_children: list, payload_type: str, payload_val: str):
    return [c for c in runkey_children if get_node_payload_data(c, payload_type) == payload_val]


def get_node_with_payload(runkey_children: list, payload_type: str, payload_val: str, default=None, assert_one=True):
    nodes = get_nodes_with_payload(runkey_children, payload_type, payload_val)
    if not nodes:
        return default
    return get_one(nodes, assert_one)


def get_subnodes_with_payload(runkey_node: dict, payload_type: str, payload_val: str):
    # note, by default, get_node_payload_data asserts that it finds only 1 payload
    return [c for c in runkey_node['children'] if get_node_payload_data(c, payload_type) == payload_val]


def get_subnode_with_payload(runkey_node: dict, payload_type: str, payload_val: str, default=None, assert_one=True):
    subnodes = get_subnodes_with_payload(runkey_node, payload_type, payload_val)
    if not subnodes:
        return default
    return get_one(subnodes, assert_one)


def get_subnodes_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str):
    return [c for c in runkey_node['children'] if c['type'] == node_type and get_node_payload_data(c, payload_type) == payload_val]


def get_subnode_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str, default=None, assert_one=True):
    subnodes = get_subnodes_of_type_with_payload(runkey_node, node_type, payload_type, payload_val)
    if not subnodes:
        return default
    return get_one(subnodes, assert_one)


class Payload(dict):
    def __init__(self, payload_type=None, payload_data=None, payload_id=None, meta=False):
        # a payload can be given a UUID directly by a user
        # and it can be a reference, i.e. it contains only the ID
        # of the payload it refers to

        #self.type = payload_type # this is potentially bad:
        ## if the user will try to update self.type = "newString"
        ## then the underlying dictionary won't be updated
        ## but such use-cases don't happen on practice
        #self.data = payload_data
        #self.id   = payload_id

        # initialise the dict (it was needed UserDict - check if needed now)
        super().__init__()

        # now skip Nones
        if payload_type is not None:
            # what's always true is that the type and data must both be None or not None
            if payload_data is None:
                raise ValueError(f'inconsistent payload type-data pair (one is None): {payload_type} {payload_data}')

            self['type'] = payload_type

            # TODO: but UserDict uses self.data, right? it will overwrite it
            if not meta:
                # "meta" payloads can save any data type
                # blob payloads can only save bytes
                #payload_data = bytes(str(payload_data), encoding='utf8') if not isinstance(payload_data, bytes) else payload_data
                payload_data = payload_data if isinstance(payload_data, str) else str(payload_data)
            self['data'] = payload_data

        if payload_id is not None:
            self['id'] = payload_id

        if meta: # TODO test again how this works. E.g. do we need this "meta" in the reference payloads?
            self['meta'] = 'True'

    def make_reference(self):
        if 'id' not in self:
            self['id'] = str(uuid.uuid4())

        if DEBUG_DUPLICATE_PAYLOADS:
            return Payload(self['type'], self['data'], payload_id=self['id'])

        else:
            return Payload(payload_id=self['id']) # TODO check is "meta" should be added here?


def is_a_payload_dict(x):
    res  = isinstance(x, dict) # TODO: maybe use abstract class instead of dict?
    res &= len(x) == 2         # only 2 keys
    res &= 'type' in x and 'data' in x
    return res


def payloads_from_dict(dictionary):
    return [Payload(key, str(val)) for key, val in dictionary.items()]


def load_payloads_from_dict(rkey_payloads, dictionary):
    for p in payloads_from_dict(dictionary):
        rkey_payloads.setdefault('payloads', []).append(p)


class RunkeyNode(dict):

    '''
    A class for a general Runkey node. Nothing Strips-specific.

    >>> node = RunkeyNode('FELIX_DEVICE', payloads={'FID_CONTROLLER_ID': 1})
    >>> node
    {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': []}
    >>> import pprint
    >>> pprint.pp(node)
    {'type': 'FELIX_DEVICE',
     'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}],
     'children': []}
    >>> [t for t, d in [p.values() for p in node.payloads]] # i.e. I rely on the fact that items are ordered
    ['FID_CONTROLLER_ID']
    >>> 
    >>> flx_node = RunkeyNode('FELIX', {'FID_DETECTOR_ID': 0}, [node])
    >>> flx_node
    {'type': 'FELIX', 'payloads': [{'type': 'FID_DETECTOR_ID', 'data': '0'}], 'children': [{'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': []}]}
    >>> dict(flx_node)
    {'type': 'FELIX', 'payloads': [{'type': 'FID_DETECTOR_ID', 'data': '0'}], 'children': [{'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': []}]}
    >>> stave_node = RunkeyNode('Stave', {'UUID': 'unknown', 'staveName': 'PPB1A', 'staveType': 'SS', 'chipType':  'Star_h1a1'})
    >>> import json
    >>> json.dumps(stave_node, default=dict)
    '{"type": "Stave", "payloads": [{"type": "UUID", "data": "unknown"}, {"type": "staveName", "data": "PPB1A"}, {"type": "staveType", "data": "SS"}, {"type": "chipType", "data": "Star_h1a1"}], "children": []}'
    '''

    def __init__(self, node_type, payloads={}, children=[]):
        self.payloads = []
        self.children = []
        self.type     = node_type
        self.update({'type': self.type, 'payloads': self.payloads, 'children': self.children})

        if isinstance(payloads, list): # then it is the runkey format
            for p_dict in payloads:
                self['payloads'].append(Payload(p_dict.get('type'), p_dict.get('data'), p_dict.get('id')))

        elif isinstance(payloads, dict): # then it is the succinct Python format
            for p_type, p_data in payloads.items():
                self['payloads'].append(Payload(p_type, p_data))

        for c in children:
            if isinstance(c, RunkeyNode):
                self['children'].append(c)
            else:
                self['children'].append(RunkeyNode(c['type'], c.get('payloads', {}), c.get('children', [])))

    #def __repr__(self):
    #    # repr actually should represent how the object was built..
    #    # but pprint uses it, so I return the internals
    #    return super().__repr__()

    def add_payload(self, p_type, p_value):
        # TODO: test if the same type payload exists?
        self['payloads'].append(Payload(p_type, p_value))

    def make_reference_clone(self):
        '''
        Copy the node, with reference IDs in the payloads, and recurse children.

        >>> node = RunkeyNode('FELIX_DEVICE', payloads={'FID_CONTROLLER_ID': 1})
        >>> node
        {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': []}
        >>> node_ref = node.make_reference_clone()
        >>> node_ref
        {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1', 'id': ...}], 'children': []}
        >>> node
        {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1', 'id': ...}], 'children': []}

        TODO: the `node_ref` still copies everything -- we are still not sure
        hot the referencing works etc. Currently, we just duplicate everything.
        '''

        #
        clone_type = self.type
        # add IDs to the payloads, if needed
        clone_payloads = [p.make_reference() for p in self['payloads']]
        #
        clone_children = [c.make_reference_clone() for c in self['children']]

        return RunkeyNode(clone_type, clone_payloads, clone_children)


class RunkeyStave(RunkeyNode):
    def __init__(self, name, stave_type, stave_uuid, chip_type, **more_payloads):
        all_payloads = {'staveName': name, 'UUID': stave_uuid, 'staveType': stave_type, 'chipType': chip_type}
        self.side_m = None # TODO: these shortcuts can be handy if they are never updated - figure out on practice if it is true
        self.side_s = None

        if 'children' in more_payloads:
            # then it must be a list of child nodes
            children = more_payloads['more_payloads']
            more_payloads.pop('more_payloads')
            if not isinstance(children, list):
                raise ValueError(f'got a "children" argument that is not a list: {children}')
            # a stave can have only 2 children: the stave sides
            # TODO should check the names of the sides, or make a RunkeySide class and test for it etc
            if not (len(children) == 2 and all(c['type'] == 'StaveSide' for c in children)):
                raise ValueError(f'got a "children" that is not a pair of stave sides: {children}')
            m = get_node_with_payload(children, 'sideType', 'side_m')
            s = get_node_with_payload(children, 'sideType', 'side_s')
            self.side_m = m if isinstance(m, RunkeyNode) else RunkeyNode(m['type'], m['payloads'], m['children'])
            self.side_s = s if isinstance(s, RunkeyNode) else RunkeyNode(s['type'], s['payloads'], s['children'])

        else:
            self.side_m = RunkeyNode('StaveSide', {'sideType': 'side_m'})
            self.side_s = RunkeyNode('StaveSide', {'sideType': 'side_s'})

        children = []
        children.append(self.side_m)
        children.append(self.side_s)

        all_payloads.update(more_payloads)
        super().__init__(node_type='Stave', payloads=all_payloads, children=children)

    # again, I cannot make this handy iterator because of the json dump
    # json iterates over the nodes, it expects to iterate over keys of a dictionary-like node
    #def __iter__(self):
    #    return (c for c in self['children']) # TODO make sure the order is right?

    # TODO let's think if we need to re-implement this method to preserve the obejct class in the reference
    # def make_reference_clone(self)


class RunkeyHCC(RunkeyNode):
    def __init__(self, itsdaq_number, module_number, hybrid_name, rx_elink, tx_elink, config="", **more_payloads):
        all_payloads = {'itsdaq_number': itsdaq_number,
                        'module_number': module_number,
                        'hybrid_name': hybrid_name,
                        'rx_elink': rx_elink,
                        'tx_elink': tx_elink,
                        'config': config,
                        }

        if 'children' in more_payloads:
            logging.warning(f'Unexpected children nodes for an HCC: {more_payloads["children"]}')
            more_payloads.pop('children')

        all_payloads.update(more_payloads)
        super().__init__(node_type='HCC', payloads=all_payloads)

        # and assign a UUID to the config payload
        conf = get_node_payload(self, 'config')
        conf['id'] = str(uuid.uuid4())

    def copy(self):
        '''
        return a deep copy of everything, but refer the config payload

        >>> hcc = RunkeyHCC(str(1), str(0), "Y", ["0", "14"], "5", enable="True", locked="False")
        >>> hcc
        {'type': 'HCC', 'payloads': [{'type': 'itsdaq_number', 'data': '1'}, {'type': 'module_number', 'data': '0'}, {'type': 'hybrid_name', 'data': 'Y'}, {'type': 'rx_elink', 'data': "['0', '14']"}, {'type': 'tx_elink', 'data': '5'}, {'type': 'config', 'data': '', 'id': ...}, {'type': 'enable', 'data': 'True'}, {'type': 'locked', 'data': 'False'}], 'children': []}
        >>> hcc.copy()
        {'type': 'HCC', 'payloads': [{'type': 'itsdaq_number', 'data': '1'}, {'type': 'module_number', 'data': '0'}, {'type': 'hybrid_name', 'data': 'Y'}, {'type': 'rx_elink', 'data': "['0', '14']"}, {'type': 'tx_elink', 'data': '5'}, {'type': 'enable', 'data': 'True'}, {'type': 'locked', 'data': 'False'}, {'id': ...}], 'children': []}
        '''

        pls = self['payloads']
        # copy all payloads except config
        new_node = RunkeyNode('HCC',
                              {p['type']: p['data'] for p in pls if p['type'] != 'config'})

        # and reference the config payload
        cfg_payload = get_node_payload(self, 'config')
        new_node['payloads'].append({'id': cfg_payload['id']})

        return new_node


def runkey_fiber_to_yarr_connectivity(rkey_felix):
    '''
    Input: a json tree Felix > FELIX_DEVICE > GBT_LINKs > stave fibers > elinks > chips.
    Output: a list of chip connectivity, and connectivity configs.

    1 FELIX device = 1 DMA connection & 1 RDMA connection.
    Hence everything is grouped by the card id and device id pairs.
    But we typically also group it per stave...
    Let's add stave names in the payloads of their fibers then.
    So, we'll have the mappings:
    (card, device = controller ID), staveName => hcc itsdaq_number => a pair of TX & RX gbt_number, elink_number
    or just a list of whatever is hcc full name to elinks
    and
    staveName and hcc itsdaq_number => the full elink mapping and hcc config
    i.e. we need the same thing in both directions?

    And I need to see the connectivity by:
    gbt link, egroup, epath -- hcc module, hybrid
    '''

    #
    mapping_configs = {}

    detector_id = get_node_payload(rkey_felix, 'FID_DETECTOR_ID')

    for rkey_flx_device in get_subnodes_of_type(rkey_felix, 'FELIX_DEVICE'):
        controller_id = get_node_payload(rkey_flx_device, 'FID_CONTROLLER_ID') # TODO: confirm that "controller_id" gets you a felix _device_, not a whole card

        for rkey_fiber in rkey_flx_device['children']:
            its_rx_fiber = rkey_fiber['type'] == 'GBT_RX'
            gbt_link = get_node_payload(rkey_fiber, 'GBT_LINK')

            # this link node must have only 1 child -- the stave fiber that is connected to it
            if len(rkey_fiber['children']) == 0:
                continue

            assert len(rkey_fiber['children']) == 1
            #
            rkey_stave_fiber = rkey_fiber['children'][0]
            staveName = get_node_payload(rkey_stave_fiber, 'staveName') # TODO: so, currently it does not work, because the payload UUIDs are not infolded into payloads
            # TODO maybe emulate how it should work by keeping a dict with UUID: payload stuff?
            #      we need something like that for testing
            chipType  = get_node_payload(rkey_stave_fiber, 'chipType')

            for rkey_elink in get_subnodes_of_type(rkey_stave_fiber, 'elink'):
                #
                elink_number = get_node_payload(rkey_elink, 'elink_number')
                egroup = get_node_payload(rkey_elink, 'egroup')
                epath  = get_node_payload(rkey_elink, 'epath')

                # again, the elink must have only 1 child, the hcc
                assert len(rkey_elink['children']) == 1
                rkey_hcc = rkey_elink['children'][0]

                itsdaq_number = get_node_payload(rkey_hcc, 'itsdaq_number')
                module_number = get_node_payload(rkey_hcc, 'module_number')
                hybrid_name   = get_node_payload(rkey_hcc, 'hybrid_name')
                hcc_config    = get_node_payload(rkey_hcc, 'config')

                # TODO: again, these two must be scan configs, not chip configs
                enable = get_node_payload(rkey_hcc, 'enable') 
                locked = get_node_payload(rkey_hcc, 'locked')

                # pack all of this into the mappings
                # hcc configs
                hcc_info = mapping_configs.setdefault((staveName, chipType, module_number, hybrid_name, itsdaq_number), {})
                hcc_info['rx' if its_rx_fiber else 'tx'] = detector_id, controller_id, gbt_link, egroup, epath, elink_number # TODO: use the full reference tuples?
                if 'config' not in hcc_info:
                    hcc_info['config'] = hcc_config
                    hcc_info['enable'] = enable
                    hcc_info['locked'] = locked

                ## and the stave mapping
                #connect_info = mapping_connectivity.setdefault((detector_id, controller_id, staveName), {})
                #connect_info.setdefault(itsdaq_number, {})
                # no wait, this one can be simpler

    #
    mapping_connectivity = {'hccs': {}}
    for full_hcc_id, hcc_info in mapping_configs.items():
        # full_hcc_id the ID stave-wise
        # the mapping are tx and rx
        # let's merge all of them into one full identifier
        hcc_name = 'hcc' + '-'.join(full_hcc_id)
        rx_name  = 'rx' + '-'.join(hcc_info['rx'])
        tx_name  = 'tx' + '-'.join(hcc_info['tx'])

        _, chipType, _, _, _ = full_hcc_id # TODO: also not perfect

        # elink numbering that we currently use
        # TODO: transition to using full FID
        _, _, rx_gbt_link, _, _, rx_elink_num = hcc_info['rx']
        _, _, tx_gbt_link, _, _, tx_elink_num = hcc_info['tx']
        rx_elink = rx_gbt_link*0x40 + rx_elink_num
        tx_elink = tx_gbt_link*0x40 + tx_elink_num

        mapping_connectivity['hccs']['_'.join((hcc_name, rx_name, tx_name))] = hcc_info['config'], rx_elink, tx_elink, hcc_info['enable'], hcc_info['locked']
        # TODO: or, make the enable and locked options here
        mapping_connectivity['chipType'] = chipType # i.e. I assume all chips are the same, and overwrite the type here

    # and we need only this bit:
    return mapping_connectivity


def write_cfg_files(mapping_connectivity, directory='./', cfg_prefix='CERNSR1_testing_setup'):
    connectivity_list = [] # TODO: damn, where is chipType

    for full_hcc_name, full_hcc_cfg in mapping_connectivity['hccs'].items():
        cfg_filename = directory + cfg_prefix + '_' + full_hcc_name + '.json'
        cfg, rx, tx, enable, locked = full_hcc_cfg
        connectivity_list.append({'config': cfg_filename,
            'tx': tx, 'rx': rx, 'enable': enable, 'locked': locked})

        with open(cfg_filename, w) as f:
            f.write(cfg)

    #
    connectivity_dict = {'chipType': mapping_connectivity['chipType'],
            'chips': connectivity_list}

    #
    with open(directory + cfg_prefix + '.json', 'w') as f:
        json.dump(connectivity_dict, f)


StripsGeometryCoordinate = namedtuple('StripsGeometryCoordinate', 'section layer slot')
# barrel-only? no, it should work for endcaps too
# but they might call things in weird ways, like with sectors etc
# TODO: check TDR for the official jargon https://cds.cern.ch/record/2257755?ln=en

if __name__ == "__main__":
    '''
    Run doctest -- it is a module in the standard Python library
    that pulls the test cases directly from the docstrings and runs them.
    Run it simply as

    $ python scripts/configDB_StripsRunkeys.py

    Or verbose:

    $ python scripts/configDB_StripsRunkeys.py -v
    '''

    import doctest
    doctest.testmod(optionflags=doctest.ELLIPSIS)
