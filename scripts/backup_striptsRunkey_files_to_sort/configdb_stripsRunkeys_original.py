'''
A module with typical operations on Runkey nodes in general, and on Strips-specific nodes.

For example:

>>> node = RunkeyNode('FELIX_DEVICE', payloads={'FID_CONTROLLER_ID': 1})
>>> node.data
{'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': []}
'''

import logging
from collections import namedtuple
import uuid # str(uuid.uuid4())
import json
# duplicate everything for debugging when ConfigDB handles references
DEBUG_DUPLICATE_PAYLOADS = True   


def get_one(a_list: list, assert_one=True):
    if assert_one and len(a_list) != 1:
        raise AssertionError(f'Got a list of len != 1: {a_list}')

    if a_list:
        return a_list[0]
    else:
        return None

#def get_subnodes_of_type(runkey_node: dict, node_type: str) -> list:
#    return [c for c in runkey_node['children'] if c['type'] == node_type]
def get_subnodes_of_type(runkey_node: dict, node_type: str) -> list:
    return [c for c in runkey_node.get('children', []) if c['type'] == node_type]


#def get_subnode_of_type(runkey_node: dict, type_val: str, assert_one=True) -> list:
#    res = get_subnodes_of_type(runkey_node, type_val)
#    return get_one(res, assert_one)
def get_subnode_of_type(runkey_node: dict, type_val: str, assert_one=True) -> dict:
    subnodes = get_subnodes_of_type(runkey_node, type_val)
    return get_one(subnodes, assert_one)

#def get_node_payloads(runkey_node: dict, payload_type: str):
#    return [p for p in runkey_node['payloads'] if p['type'] == payload_type]
def get_node_payloads(runkey_node: dict, payload_type: str):
    return [p for p in runkey_node.get('payloads', []) if p['type'] == payload_type]

def get_node_payload(runkey_node: dict, payload_type: str, assert_one=True):
    return get_one(get_node_payloads(runkey_node, payload_type), assert_one)

def get_node_payloads_data(runkey_node: dict, payload_type: str):
    return [p['data'] for p in runkey_node['payloads'] if p['type'] == payload_type]
#####
def get_node_payload_data(node: dict, payload_type: str) -> dict:
    if 'payloads' not in node:
        print(f"Node has no 'payloads' key. Node type: {node.get('type')}")
        return None
    
    payloads = node['payloads']
    for payload in payloads:
        if payload['type'] == payload_type:
            data = payload.get('data')
            if payload_type == 'hcc_data' and isinstance(data, str):
                return json.loads(data)
            return data
        elif payload['type'] == 'hcc_data':
            hcc_data = json.loads(payload.get('data')) if isinstance(payload.get('data'), str) else payload.get('data')
            if isinstance(hcc_data, dict) and payload_type in hcc_data:
                return hcc_data[payload_type]
    
    print(f"No payload of type '{payload_type}' found. Available types: {[p['type'] for p in payloads]}")
    return None

def get_subnodes_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str) -> list:
    subnodes = []
    for c in runkey_node.get('children', []):
        if c['type'] == node_type:
            node_data = get_node_payload_data(c, payload_type)
            if node_data == payload_val:
                subnodes.append(c)
            elif node_type == 'HCC':
                hcc_data = get_node_payload_data(c, 'hcc_data')
                if isinstance(hcc_data, dict) and hcc_data.get(payload_type) == str(payload_val):
                    subnodes.append(c)
    return subnodes

def get_subnode_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str, default=None, assert_one=True):
    subnodes = get_subnodes_of_type_with_payload(runkey_node, node_type, payload_type, payload_val)
    '''
    if node_type != 'HCC':
        print(f"\nnode_type: {node_type}")
        print(f"payload_type: {payload_type}")
        print(f"payload_val: {payload_val}")
        print(f"subnodes >=1: {len(subnodes)>=1}")
    '''
    if not subnodes:
        return default
    return get_one(subnodes, assert_one)
#####

def get_nodes_with_payload(runkey_children: list, payload_type: str, payload_val: str):
    return [c for c in runkey_children if get_node_payload_data(c, payload_type) == payload_val]
    return [p['data'] for p in runkey_node['payloads'] if p['type'] == payload_type]
#####
def get_node_payload_data(node: dict, payload_type: str) -> dict:
    if 'payloads' not in node:
        print(f"Node has no 'payloads' key. Node type: {node.get('type')}")
        return None
    
    payloads = node['payloads']
    for payload in payloads:
        if payload['type'] == payload_type:
            data = payload.get('data')
            if payload_type == 'hcc_data' and isinstance(data, str):
                return json.loads(data)
            return data
        elif payload['type'] == 'hcc_data':
            hcc_data = json.loads(payload.get('data')) if isinstance(payload.get('data'), str) else payload.get('data')
            if isinstance(hcc_data, dict) and payload_type in hcc_data:
                return hcc_data[payload_type]
    
    print(f"No payload of type '{payload_type}' found. Available types: {[p['type'] for p in payloads]}")
    return None

def get_subnodes_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str) -> list:
    subnodes = []
    for c in runkey_node.get('children', []):
        if c['type'] == node_type:
            node_data = get_node_payload_data(c, payload_type)
            if node_data == payload_val:
                subnodes.append(c)
            elif node_type == 'HCC':
                hcc_data = get_node_payload_data(c, 'hcc_data')
                if isinstance(hcc_data, dict) and hcc_data.get(payload_type) == str(payload_val):
                    subnodes.append(c)
    return subnodes

def get_subnode_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str, default=None, assert_one=True):
    subnodes = get_subnodes_of_type_with_payload(runkey_node, node_type, payload_type, payload_val)
    '''
    if node_type != 'HCC':
        print(f"\nnode_type: {node_type}")
        print(f"payload_type: {payload_type}")
        print(f"payload_val: {payload_val}")
        print(f"subnodes >=1: {len(subnodes)>=1}")
    '''
    if not subnodes:
        return default
    return get_one(subnodes, assert_one)
#####

def get_nodes_with_payload(runkey_children: list, payload_type: str, payload_val: str):
    return [c for c in runkey_children if get_node_payload_data(c, payload_type) == payload_val]

def get_node_with_payload(runkey_children: list, payload_type: str, payload_val: str, default=None, assert_one=True):
    nodes = get_nodes_with_payload(runkey_children, payload_type, payload_val)
    if not nodes:
        return default
    return get_one(nodes, assert_one)

#def get_subnodes_with_payload(runkey_node: dict, payload_type: str, payload_val: str):
#    # note, by default, get_node_payload_data asserts that it finds only 1 payload
#    if runkey_node is None or 'children' not in runkey_node:
#        print("runkey_node:", runkey_node)
#        print("get_subnodes_with_payload: runkey_node is None or has no children: \n")
#
#    return [c for c in runkey_node['children'] if get_node_payload_data(c, payload_type) == {payload_val}]
def get_subnodes_with_payload(runkey_node: dict, payload_type: str, payload_val: str):
    if payload_type == 'coordinate':
        #print("runkey_node.get('children'):", runkey_node.get('children'))
        for c in runkey_node.get('children', []):
            '''
            print("c:", c)
            print("payload_type:", payload_type)
            print("payload_val:", payload_val)
            print("get_node_payload_data(c, payload_type):", get_node_payload_data(c, payload_type)['value'])
            '''
    return [c for c in runkey_node.get('children', []) if get_node_payload_data(c, payload_type)['value'] == payload_val]

def get_subnode_with_payload(runkey_node: dict, payload_type: str, payload_val: str, default=None, assert_one=True):
    subnodes = get_subnodes_with_payload(runkey_node, payload_type, payload_val)
    '''
    print("\npayload_type:", payload_type)
    print("payload_val:", payload_val)
    #print("subnodes:", subnodes)
    print(f"subnodes >=1: {len(subnodes)>=1}")
    '''
    if not subnodes:
        return default
    return get_one(subnodes, assert_one)

'''
def get_subnodes_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str) -> list:
    return [c for c in runkey_node.get('children', []) 
            if c['type'] == node_type and get_node_payload_data(c, payload_type) == payload_val]
#debug:
def get_subnodes_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str) -> list:
    subnodes = []
    for c in runkey_node.get('children', []):
        if c['type'] == node_type:
            if get_node_payload_data(c, payload_type) == payload_val:
                subnodes.append(c)
            elif node_type == 'HCC':
                hcc_data = [p for p in c['payloads'] if p['type'] == 'hcc_data'][0]['data'][payload_type]
                if hcc_data == str(payload_val):
                    #print("hcc_data['payload_type']:", hcc_data)
                    subnodes.append(c)
    return subnodes
''' 

'''
def get_subnode_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str, default=None, assert_one=True):
    subnodes = get_subnodes_of_type_with_payload(runkey_node, node_type, payload_type, payload_val)
    print("\nnode_type:", node_type)
    print("payload_type:", payload_type)
    print("payload_val:", payload_val)
    print("subnodes:", subnodes)
    if not subnodes:
        return default
    return get_one(subnodes, assert_one)
'''
  

class Payload(dict):
    def __init__(self, payload_type=None, payload_data=None, payload_id=None, meta=True):
        # a payload can be given a UUID directly by a user
        # and it can be a reference, i.e. it contains only the ID
        # of the payload it refers to

        #self.type = payload_type # this is potentially bad:
        ## if the user will try to update self.type = "newString"
        ## then the underlying dictionary won't be updated
        ## but such use-cases don't happen on practice
        #self.data = payload_data
        #self.id   = payload_id

        # initialise the UserDict
        super().__init__()

        # now skip Nones
        if payload_type is not None:
            # what's always true is that the type and data must both be None or not None
            if payload_data is None:
                raise ValueError(f'inconsistent payload type-data pair (one is None): {payload_type} {payload_data}')

            # TODO: but UserDict uses self.data, right? it will overwrite it
            if not meta:
                # "meta" payloads can save any data type. blob payloads can only save bytes
                #payload_data = bytes(str(payload_data), encoding='utf8') if not isinstance(payload_data, bytes) else payload_data
                payload_data = payload_data if isinstance(payload_data, str) else str(payload_data)
            
            self['type'] = payload_type
            self['data'] = payload_data
            # TODO: but UserDict uses self.data, right? it will overwrite it
            if not meta:
                # "meta" payloads can save any data type. blob payloads can only save bytes
                #payload_data = bytes(str(payload_data), encoding='utf8') if not isinstance(payload_data, bytes) else payload_data
                payload_data = payload_data if isinstance(payload_data, str) else str(payload_data)
            
            self['type'] = payload_type
            self['data'] = payload_data
            
        if payload_id is not None:
            self['id'] = payload_id 

        if meta: # TODO test again how this works. E.g. do we need this "meta" in the reference payloads?
            self['meta'] = True

    def make_reference(self):
        # The idea was to pass "id" everywhere, but it does not work for some reason
        # so, let's just manually duplicate everything, without references and "id" altogether
        if 'id' not in self and not DEBUG_DUPLICATE_PAYLOADS:
            self['id'] = str(uuid.uuid4())

        if DEBUG_DUPLICATE_PAYLOADS:
            #return Payload(self['type'], self['data'], payload_id=self['id'])
            return Payload(self['type'], self['data'])

        else:
            return Payload(payload_id=self['id']) # TODO check is "meta" should be added here?        

def is_a_payload_dict(x):
    res  = isinstance(x, dict) # TODO: maybe use abstract class instead of dict?
    res &= len(x) == 2         # only 2 keys
    res &= 'type' in x and 'data' in x
    return res

def payloads_from_dict(dictionary):
    return [Payload(key, str(val)) for key, val in dictionary.items()]

def load_payloads_from_dict(rkey_payloads, dictionary):
    for p in payloads_from_dict(dictionary):
        rkey_payloads.setdefault('payloads', []).append(p)

class RunkeyNode(dict):
    def __init__(self, node_type, *payload_dicts, children=None):
        self.payloads = []
        self.children = children or []
        self.type = node_type
        self.update({'type': self.type, 'payloads': self.payloads, 'children': self.children})
        self['id'] = str(uuid.uuid4())

        for payload_dict in payload_dicts:
            if isinstance(payload_dict, dict):
                for p_type, p_data in payload_dict.items():
                    self['payloads'].append(Payload(p_type, p_data))
            elif isinstance(payload_dict, list):
                for p_dict in payload_dict:
                    self['payloads'].append(Payload(p_dict.get('type'), p_dict.get('data'), p_dict.get('id')))
            else:
                raise ValueError(f"Unsupported payload type: {type(payload_dict)}")

        for i, c in enumerate(self.children):
            if not isinstance(c, RunkeyNode):
                self.children[i] = RunkeyNode(c['type'], c.get('payloads', {}), children=c.get('children', []))

    def add_payload(self, p_type, p_value):
        self['payloads'].append(Payload(p_type, p_value))

    def make_reference_clone(self):
        clone_type = self.type
        clone_payloads = [p.make_reference() for p in self['payloads']]
        clone_children = [c.make_reference_clone() for c in self['children']]
        return RunkeyNode(clone_type, *clone_payloads, children=clone_children)

class RunkeyStave(RunkeyNode):
    def __init__(self, name, stave_type, stave_uuid=None, chip_type=None, *more_payload_dicts, children=None):
        self.side_m = None
        self.side_s = None

        base_payload = {'staveName': name, 'staveType': stave_type}
        if stave_uuid is not None:
            base_payload['UUID'] = stave_uuid
        if chip_type is not None:
            base_payload['chipType'] = chip_type

        all_payloads = [base_payload] + list(more_payload_dicts)

        if children is None:
            self.side_m = RunkeyNode('StaveSide', {'sideType': 'side_m'})
            self.side_s = RunkeyNode('StaveSide', {'sideType': 'side_s'})
            children = [self.side_m, self.side_s]
        elif isinstance(children, list) and len(children) == 2 and all(c['type'] == 'StaveSide' for c in children):
            m = get_node_with_payload(children, 'sideType', 'side_m')
            s = get_node_with_payload(children, 'sideType', 'side_s')
            self.side_m = m if isinstance(m, RunkeyNode) else RunkeyNode(m['type'], m['payloads'], children=m.get('children', []))
            self.side_s = s if isinstance(s, RunkeyNode) else RunkeyNode(s['type'], s['payloads'], children=s.get('children', []))
            children = [self.side_m, self.side_s]
        else:
            raise ValueError(f'Invalid children: {children}. Expected two StaveSide nodes.')

        super().__init__('Stave', *all_payloads, children=children)


class RunkeyHCC(RunkeyNode):
    def __init__(self, hcc_data, config=""): #TODO: add extra fields
        # Ensure all required fields are present
        required_fields = ['itsdaq_number', 'module_number', 'hybrid_name', 'rx_elink', 'tx_elink']
        for field in required_fields:
            if field not in hcc_data:
                raise ValueError(f"Missing required field: {field}")

        # Remove config from hcc_data if present
        hcc_data = hcc_data.copy()
        hcc_data.pop('config', None)

        # Create two payload dictionaries
        hcc_data_payload = {'hcc_data': hcc_data}#json.dumps(hcc_data)}
        #hcc_data_payload = {'hcc_data': json.dumps(hcc_data)}
        config_payload = {'config': config}

        super().__init__('HCC', hcc_data_payload, config_payload)

    def copy(self):
        hcc_data = json.loads(self.get_hcc_data())
        config = self.get_config()
        return RunkeyHCC(hcc_data, config)

    def get_hcc_data(self):
        return get_node_payload_data(self, 'hcc_data')

    def get_config(self):
        return get_node_payload_data(self, 'config')

    def get_field(self, field_name):
        if field_name == 'config':
            return self.get_config()
        hcc_data = json.loads(self.get_hcc_data())
        return hcc_data.get(field_name)

    def set_config(self, new_config):
        config_payload = get_node_payload(self, 'config')
        config_payload['data'] = new_config


        self.payloads = []
        self.children = children or []
        self.type = node_type
        self.update({'type': self.type, 'payloads': self.payloads, 'children': self.children})
        self['id'] = str(uuid.uuid4())

        for payload_dict in payload_dicts:
            if isinstance(payload_dict, dict):
                for p_type, p_data in payload_dict.items():
                    self['payloads'].append(Payload(p_type, p_data))
            elif isinstance(payload_dict, list):
                for p_dict in payload_dict:
                    self['payloads'].append(Payload(p_dict.get('type'), p_dict.get('data'), p_dict.get('id')))
            else:
                raise ValueError(f"Unsupported payload type: {type(payload_dict)}")

        for i, c in enumerate(self.children):
            if not isinstance(c, RunkeyNode):
                self.children[i] = RunkeyNode(c['type'], c.get('payloads', {}), children=c.get('children', []))

    def add_payload(self, p_type, p_value):
        self['payloads'].append(Payload(p_type, p_value))

    def make_reference_clone(self):
        clone_type = self.type
        clone_payloads = [p.make_reference() for p in self['payloads']]
        clone_children = [c.make_reference_clone() for c in self['children']]
        return RunkeyNode(clone_type, *clone_payloads, children=clone_children)

class RunkeyStave(RunkeyNode):
    def __init__(self, name, stave_type, stave_uuid=None, chip_type=None, *more_payload_dicts, children=None):
        self.side_m = None
        self.side_s = None

        base_payload = {'staveName': name, 'staveType': stave_type}
        if stave_uuid is not None:
            base_payload['UUID'] = stave_uuid
        if chip_type is not None:
            base_payload['chipType'] = chip_type

        all_payloads = [base_payload] + list(more_payload_dicts)

        if children is None:
            self.side_m = RunkeyNode('StaveSide', {'sideType': 'side_m'})
            self.side_s = RunkeyNode('StaveSide', {'sideType': 'side_s'})
            children = [self.side_m, self.side_s]
        elif isinstance(children, list) and len(children) == 2 and all(c['type'] == 'StaveSide' for c in children):
            m = get_node_with_payload(children, 'sideType', 'side_m')
            s = get_node_with_payload(children, 'sideType', 'side_s')
            self.side_m = m if isinstance(m, RunkeyNode) else RunkeyNode(m['type'], m['payloads'], children=m.get('children', []))
            self.side_s = s if isinstance(s, RunkeyNode) else RunkeyNode(s['type'], s['payloads'], children=s.get('children', []))
            children = [self.side_m, self.side_s]
        else:
            raise ValueError(f'Invalid children: {children}. Expected two StaveSide nodes.')

        super().__init__('Stave', *all_payloads, children=children)


class RunkeyHCC(RunkeyNode):
    def __init__(self, hcc_data, config=""): #TODO: add extra fields
        # Ensure all required fields are present
        required_fields = ['itsdaq_number', 'module_number', 'hybrid_name', 'rx_elink', 'tx_elink']
        for field in required_fields:
            if field not in hcc_data:
                raise ValueError(f"Missing required field: {field}")

        # Remove config from hcc_data if present
        hcc_data = hcc_data.copy()
        hcc_data.pop('config', None)

        # Create two payload dictionaries
        hcc_data_payload = {'hcc_data': hcc_data}#json.dumps(hcc_data)}
        #hcc_data_payload = {'hcc_data': json.dumps(hcc_data)}
        config_payload = {'config': config}

        super().__init__('HCC', hcc_data_payload, config_payload)

    def copy(self):
        hcc_data = json.loads(self.get_hcc_data())
        config = self.get_config()
        return RunkeyHCC(hcc_data, config)

    def get_hcc_data(self):
        return get_node_payload_data(self, 'hcc_data')

    def get_config(self):
        return get_node_payload_data(self, 'config')

    def get_field(self, field_name):
        if field_name == 'config':
            return self.get_config()
        hcc_data = json.loads(self.get_hcc_data())
        return hcc_data.get(field_name)

    def set_config(self, new_config):
        config_payload = get_node_payload(self, 'config')
        config_payload['data'] = new_config



def runkey_fiber_to_yarr_connectivity(rkey_felix):
    '''
    Input: a json tree Felix > FELIX_DEVICE > GBT_LINKs > stave fibers > elinks > chips.
    Output: a list of chip connectivity, and connectivity configs.

    1 FELIX device = 1 DMA connection & 1 RDMA connection.
    Hence everything is grouped by the card id and device id pairs.
    But we typically also group it per stave...
    Let's add stave names in the payloads of their fibers then.
    So, we'll have the mappings:
    (card, device = controller ID), staveName => hcc itsdaq_number => a pair of TX & RX gbt_number, elink_number
    or just a list of whatever is hcc full name to elinks
    and
    staveName and hcc itsdaq_number => the full elink mapping and hcc config
    i.e. we need the same thing in both directions?

    And I need to see the connectivity by:
    gbt link, egroup, epath -- hcc module, hybrid
    '''

    #
    mapping_configs = {}

    detector_id = get_node_payload(rkey_felix, 'FID_DETECTOR_ID')

    for rkey_flx_device in get_subnodes_of_type(rkey_felix, 'FELIX_DEVICE'):
        controller_id = get_node_payload(rkey_flx_device, 'FID_CONTROLLER_ID') # TODO: confirm that "controller_id" gets you a felix _device_, not a whole card

        for rkey_fiber in rkey_flx_device['children']:
            its_rx_fiber = rkey_fiber['type'] == 'GBT_RX'
            gbt_link = get_node_payload(rkey_fiber, 'GBT_LINK')

            # this link node must have only 1 child -- the stave fiber that is connected to it
            if len(rkey_fiber['children']) == 0:
                continue

            assert len(rkey_fiber['children']) == 1
            #
            rkey_stave_fiber = rkey_fiber['children'][0]
            staveName = get_node_payload(rkey_stave_fiber, 'staveName') # TODO: so, currently it does not work, because the payload UUIDs are not infolded into payloads
            # TODO maybe emulate how it should work by keeping a dict with UUID: payload stuff?
            #      we need something like that for testing
            chipType  = get_node_payload(rkey_stave_fiber, 'chipType')

            for rkey_elink in get_subnodes_of_type(rkey_stave_fiber, 'elink'):
                #
                elink_number = get_node_payload(rkey_elink, 'elink_number')
                egroup = get_node_payload(rkey_elink, 'egroup')
                epath  = get_node_payload(rkey_elink, 'epath')

                # again, the elink must have only 1 child, the hcc
                assert len(rkey_elink['children']) == 1
                rkey_hcc = rkey_elink['children'][0]

                #itsdaq_number = get_node_payload(rkey_hcc, 'itsdaq_number')
                #module_number = get_node_payload(rkey_hcc, 'module_number')
                #hybrid_name   = get_node_payload(rkey_hcc, 'hybrid_name')
                #hcc_config    = get_node_payload(rkey_hcc, 'config')
                itsdaq_number = rkey_hcc.get_field('itsdaq_number')
                module_number = rkey_hcc.get_field('module_number')
                hybrid_name = rkey_hcc.get_field('hybrid_name')
                hcc_config = rkey_hcc.get_config()
                enable = rkey_hcc.get_field('enable')
                locked = rkey_hcc.get_field('locked')

                # TODO: again, these two must be scan configs, not chip configs
                #enable = get_node_payload(rkey_hcc, 'enable') 
                #locked = get_node_payload(rkey_hcc, 'locked')

                # pack all of this into the mappings
                # hcc configs
                hcc_info = mapping_configs.setdefault((staveName, chipType, module_number, hybrid_name, itsdaq_number), {})
                hcc_info['rx' if its_rx_fiber else 'tx'] = detector_id, controller_id, gbt_link, egroup, epath, elink_number # TODO: use the full reference tuples?
                if 'config' not in hcc_info:
                    hcc_info['config'] = hcc_config
                    hcc_info['enable'] = enable
                    hcc_info['locked'] = locked

                ## and the stave mapping
                #connect_info = mapping_connectivity.setdefault((detector_id, controller_id, staveName), {})
                #connect_info.setdefault(itsdaq_number, {})
                # no wait, this one can be simpler

    #
    mapping_connectivity = {'hccs': {}}
    for full_hcc_id, hcc_info in mapping_configs.items():
        # full_hcc_id the ID stave-wise
        # the mapping are tx and rx
        # let's merge all of them into one full identifier
        hcc_name = 'hcc' + '-'.join(full_hcc_id)
        rx_name  = 'rx' + '-'.join(hcc_info['rx'])
        tx_name  = 'tx' + '-'.join(hcc_info['tx'])

        _, chipType, _, _, _ = full_hcc_id # TODO: also not perfect

        # elink numbering that we currently use
        # TODO: transition to using full FID
        _, _, rx_gbt_link, _, _, rx_elink_num = hcc_info['rx']
        _, _, tx_gbt_link, _, _, tx_elink_num = hcc_info['tx']
        rx_elink = rx_gbt_link*0x40 + rx_elink_num
        tx_elink = tx_gbt_link*0x40 + tx_elink_num

        mapping_connectivity['hccs']['_'.join((hcc_name, rx_name, tx_name))] = hcc_info['config'], rx_elink, tx_elink, hcc_info['enable'], hcc_info['locked']
        #mapping_connectivity['hccs']['_'.join((hcc_name, rx_name, tx_name))] = hcc_info['config'], rx_elink, tx_elink, hcc_info['enable'], hcc_info['locked']
        #mapping_connectivity['hccs']['_'.join((hcc_name, rx_name, tx_name))] = hcc_info['config'], rx_elink, tx_elink, hcc_info['enable'], hcc_info['locked']
        # TODO: or, make the enable and locked options here
        mapping_connectivity['chipType'] = chipType # i.e. I assume all chips are the same, and overwrite the type here

    # and we need only this bit:
    return mapping_connectivity

def write_cfg_files(mapping_connectivity, directory='./', cfg_prefix='CERNSR1_testing_setup'):
    connectivity_list = [] # TODO: damn, where is chipType

    for full_hcc_name, full_hcc_cfg in mapping_connectivity['hccs'].items():
        cfg_filename = directory + cfg_prefix + '_' + full_hcc_name + '.json'
        cfg, rx, tx, enable, locked = full_hcc_cfg
        connectivity_list.append({'config': cfg_filename,
            'tx': tx, 'rx': rx, 'enable': enable, 'locked': locked})

        with open(cfg_filename, w) as f:
            f.write(cfg)

    #
    connectivity_dict = {'chipType': mapping_connectivity['chipType'],
            'chips': connectivity_list}

    #
    with open(directory + cfg_prefix + '.json', 'w') as f:
        json.dump(connectivity_dict, f)

StripsGeometryCoordinate = namedtuple('StripsGeometryCoordinate', 'section layer slot')
# barrel-only? no, it should work for endcaps too
# but they might call things in weird ways, like with sectors etc
# TODO: check TDR for the official jargon https://cds.cern.ch/record/2257755?ln=en

if __name__ == "__main__":
    '''
    Run doctest -- it is a module in the standard Python library
    that pulls the test cases directly from the docstrings and runs them.
    Run it simply as

    $ python scripts/configDB_StripsRunkeys.py

    Or verbose:

    $ python scripts/configDB_StripsRunkeys.py -v
    '''

    import doctest
    doctest.testmod(optionflags=doctest.ELLIPSIS)

