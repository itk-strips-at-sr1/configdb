'''
A module with typical operations on Runkey nodes in general, and on Strips-specific nodes.

For example:

>>> node = RunkeyNode('FELIX_DEVICE', payloads={'FID_CONTROLLER_ID': 1})
>>> node.data
{'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': []}
'''

import logging
from collections import namedtuple
import uuid # str(uuid.uuid4())
import json
# duplicate everything for debugging when ConfigDB handles references
DEBUG_DUPLICATE_PAYLOADS = True   


class Payload(dict):
    def __init__(self, payload_type=None, payload_data=None, payload_id=None, payload_name=None, meta=True, JSON=True):
        super().__init__()
        
        if payload_type is not None:
            if payload_data is None:
                raise ValueError(f'Inconsistent payload type-data pair (one is None): {payload_type} {payload_data}')
            
            # Store the payload_data as is, without converting it to a JSON string
            self['type'] = payload_type
            if JSON:
                self['data'] = payload_data if isinstance(payload_data, (dict, list)) else {payload_type: str(payload_data)}
            else:
                self['data'] = str(payload_data)
                meta = False
            #self['data'] = json.dumps(payload_data) if isinstance(payload_data, (dict, list)) else str(payload_data)
        if payload_name is not None:
            self['name'] = payload_name

        if payload_id is not None:
            self['id'] = payload_id
        
        if meta:
            self['meta'] = True


class RunkeyNode(dict):
    def __init__(self, node_type=None, node_data=None, config=None, config_name=None, children=None):
        super().__init__()
        self['type'] = node_type
        self['id'] = str(uuid.uuid4())
        self['payloads'] = []
        self['children'] = children or []

        if node_type is not None and node_data is not None:
            node_data_key = f'{node_type.lower()}_data'
            self.add_payload(node_data_key, node_data)
        
        if config is not None:
            node_config_key = f'{node_type.lower()}_config'
            self.add_payload(node_config_key, config, config_name)

    def get_node_data(self):
        node_data_key = f'{self["type"].lower()}_data'
        # This payload is totally not needed.
        # If you have a payload called "data" -- then just call it "data"
        # no need to also add the type of the node,
        # and moreover convert it to lower case when the node may not be lower case!
        # No need to create more steps to do the same.
        # We now have nodes that look like this:
        # {"type": "GBT_RX", 
        #  "payloads": [{"type": "gbt_rx_data", "data": {"link": str(j)}}], "children": []}
        # how this method helps me to get that link number?
        # it returns a dictionary, from which I need to get "link"
        # why do these extra steps?
        node_data_payload = self.get_payload(node_data_key)
        #return json.loads(node_data_payload['data']) if node_data_payload else {}
        return node_data_payload['data'] if node_data_payload else {}

    def get_config(self):
        node_config_key = f'{self["type"].lower()}_config'
        config_payload = self.get_payload(node_config_key)
        #return json.loads(config_payload['data']) if config_payload else None
        return config_payload['data'] if config_payload else None

    def get_field(self, field_name):
        if field_name.endswith('config'):
            return self.get_config()
        node_data = self.get_node_data()
        return node_data.get(field_name)

    def set_config(self, new_config, config_name=None):
        self.add_payload(f'{self["type"].lower()}_config', new_config, config_name)

    def set_field(self, field_name, value):
        node_data = self.get_node_data()
        node_data[field_name] = value
        self.add_payload(f'{self["type"].lower()}_data', node_data)

    def add_child(self, child):
        if isinstance(child, dict) and 'reuse_id' in child:
            self['children'].append(child)
        elif isinstance(child, RunkeyNode):
            self['children'].append(child)
        else:
            raise ValueError("Child must be a RunkeyNode or a dictionary with 'reuse_id'")

    def add_payload(self, payload_type, payload_data, payload_name=None, JSON=True):
        if JSON:
            new_payload = Payload(payload_type, payload_data, payload_name=payload_name)
        else:
            new_payload = Payload(payload_type, payload_data, payload_name=payload_name, JSON=False)
        # this list removes existing payloads that don't have the same type as the new one!
        self['payloads'] = [p for p in self['payloads'] if p['type'] != payload_type] + [new_payload]

    def get_payload(self, payload_type):
        return next((p for p in self['payloads'] if p['type'] == payload_type), None)

    def to_dict(self):
        return dict(self)


# Helper functions
def get_subnodes_of_type(runkey_node: dict, node_type: str) -> list:
    return [c for c in runkey_node.get('children', []) if c.get('type') == node_type]

def get_subnode_of_type(runkey_node: dict, type_val: str, assert_one=True) -> dict:
    subnodes = get_subnodes_of_type(runkey_node, type_val)
    return get_one(subnodes, assert_one)

def get_node_payload(runkey_node: dict, payload_type: str, assert_one=True):
    payloads = [p for p in runkey_node.get('payloads', []) if p['type'] == payload_type]
    return get_one(payloads, assert_one)

def get_node_payload_data(runkey_node: dict, node_type: str, payload_type: str) -> dict:
    node_data_key = f'{node_type.lower()}_data'
    payload = get_node_payload(runkey_node, node_data_key)
    if payload:
        #node_data = json.loads(payload['data'])
        node_data = payload['data']
        return node_data.get(payload_type)
    return None

def get_subnode_of_type_with_payload(runkey_node, node_type, payload_type, payload_val):
    subnodes = get_subnodes_of_type(runkey_node, node_type)
    for subnode in subnodes:
        if get_node_payload_data(subnode, node_type, payload_type) == payload_val:
            return subnode
    return None

def get_one(a_list: list, assert_one=True):
    if assert_one and len(a_list) != 1:
        raise AssertionError(f'Got a list of len != 1: {a_list}. Length: {len(a_list)}')
    return a_list[0] if a_list else None

def load_payloads_from_dict(rkey_node, dictionary):
    for key, val in dictionary.items():
        rkey_node.add_payload(key, val)


def runkey_fiber_to_yarr_connectivity(rkey_felix):
    '''
    Input: a json tree Felix > FELIX_DEVICE > GBT_LINKs > stave fibers > elinks > chips.
    Output: a list of chip connectivity, and connectivity configs.

    1 FELIX device = 1 DMA connection & 1 RDMA connection.
    Hence everything is grouped by the card id and device id pairs.
    But we typically also group it per stave...
    Let's add stave names in the payloads of their fibers then.
    So, we'll have the mappings:
    (card, device = controller ID), staveName => hcc itsdaq_number => a pair of TX & RX gbt_number, elink_number
    or just a list of whatever is hcc full name to elinks
    and
    staveName and hcc itsdaq_number => the full elink mapping and hcc config
    i.e. we need the same thing in both directions?

    And I need to see the connectivity by:
    gbt link, egroup, epath -- hcc module, hybrid
    '''

    #
    mapping_configs = {}

    detector_id = get_node_payload(rkey_felix, 'FID_DETECTOR_ID')

    for rkey_flx_device in get_subnodes_of_type(rkey_felix, 'FELIX_DEVICE'):
        #controller_id0 = get_node_payload(rkey_flx_device, 'FID_CONTROLLER_ID') # TODO: confirm that "controller_id" gets you a felix _device_, not a whole card
        # staveName = get_node_payload_data(rkey_stave, 'Stave', 'staveName')
        #print("rkey_fe_device", rkey_flx_device)
        #print("rkey_felix", rkey_felix)
        ##print(get_node_payload_data(rkey_side, 'Side', 'sideType'))
        #print(get_node_payload_data(rkey_flx_device, 'felix_device', 'FID_CONTROLLER_ID'))
        #print("get_node_payload", get_node_payload(rkey_felix, 'FELIX_DEVICE', 'felix_device_data'))
        controller_id = str(0)#get_node_payload(rkey_flx_device, 'felix_device_data') # TODO: confirm that "controller_id" gets you a felix _device_, not a whole card
        #print("controller_id0", controller_id0)
        print("controller_id", controller_id)

        for rkey_fiber in rkey_flx_device['children']:
            its_rx_fiber = rkey_fiber['type'] == 'GBT_RX'
            if its_rx_fiber:
                gbt_link = get_node_payload(rkey_fiber, 'GBT_LINK_RX')
            else:
                gbt_link = get_node_payload(rkey_fiber, 'GBT_LINK_TX')

            # this link node must have only 1 child -- the stave fiber that is connected to it
            if len(rkey_fiber['children']) == 0:
                continue

            assert len(rkey_fiber['children']) == 1
            #
            rkey_stave_fiber = rkey_fiber['children'][0]
            staveName = get_node_payload(rkey_stave_fiber, 'staveName') # TODO: so, currently it does not work, because the payload UUIDs are not infolded into payloads
            # TODO maybe emulate how it should work by keeping a dict with UUID: payload stuff?
            #      we need something like that for testing
            chipType  = get_node_payload(rkey_stave_fiber, 'chipType')

            for rkey_elink in get_subnodes_of_type(rkey_stave_fiber, 'elink'):
                #
                elink_number = get_node_payload(rkey_elink, 'elink_number')
                egroup = get_node_payload(rkey_elink, 'egroup')
                epath  = get_node_payload(rkey_elink, 'epath')

                # again, the elink must have only 1 child, the hcc
                assert len(rkey_elink['children']) == 1
                rkey_hcc = rkey_elink['children'][0]

                itsdaq_number = rkey_hcc.get_field('itsdaq_number')
                module_number = rkey_hcc.get_field('module_number')
                hybrid_name = rkey_hcc.get_field('hybrid_name')
                hcc_config = rkey_hcc.get_config()
                enable = rkey_hcc.get_field('enable')
                locked = rkey_hcc.get_field('locked')

                # TODO: again, these two must be scan configs, not chip configs
                #enable = get_node_payload(rkey_hcc, 'enable') 
                #locked = get_node_payload(rkey_hcc, 'locked')

                # pack all of this into the mappings
                # hcc configs
                hcc_info = mapping_configs.setdefault((staveName, chipType, module_number, hybrid_name, itsdaq_number), {})
                hcc_info['rx' if its_rx_fiber else 'tx'] = detector_id, controller_id, gbt_link, egroup, epath, elink_number # TODO: use the full reference tuples?
                if 'config' not in hcc_info:
                    hcc_info['config'] = hcc_config
                    hcc_info['enable'] = enable
                    hcc_info['locked'] = locked

                # and the stave mapping
                # connect_info = mapping_connectivity.setdefault((detector_id, controller_id, staveName), {})
                # connect_info.setdefault(itsdaq_number, {})
                # no wait, this one can be simpler

    mapping_connectivity = {'hccs': {}}
    for full_hcc_id, hcc_info in mapping_configs.items():
        # full_hcc_id the ID stave-wise
        # the mapping are tx and rx
        # let's merge all of them into one full identifier
        hcc_name = 'hcc' + '-'.join(full_hcc_id)
        rx_name  = 'rx' + '-'.join(hcc_info['rx'])
        tx_name  = 'tx' + '-'.join(hcc_info['tx'])

        _, chipType, _, _, _ = full_hcc_id # TODO: also not perfect

        # elink numbering that we currently use
        # TODO: transition to using full FID
        _, _, rx_gbt_link, _, _, rx_elink_num = hcc_info['rx']
        _, _, tx_gbt_link, _, _, tx_elink_num = hcc_info['tx']
        rx_elink = rx_gbt_link*0x40 + rx_elink_num
        tx_elink = tx_gbt_link*0x40 + tx_elink_num

        mapping_connectivity['hccs']['_'.join((hcc_name, rx_name, tx_name))] = hcc_info['config'], rx_elink, tx_elink, hcc_info['enable'], hcc_info['locked']
        # TODO: or, make the enable and locked options here
        mapping_connectivity['chipType'] = chipType # i.e. I assume all chips are the same, and overwrite the type here

    # and we need only this bit:
    return mapping_connectivity

def write_cfg_files(mapping_connectivity, directory='./', cfg_prefix='CERNSR1_testing_setup'):
    connectivity_list = [] # TODO: damn, where is chipType

    for full_hcc_name, full_hcc_cfg in mapping_connectivity['hccs'].items():
        cfg_filename = directory + cfg_prefix + '_' + full_hcc_name + '.json'
        cfg, rx, tx, enable, locked = full_hcc_cfg
        connectivity_list.append({'config': cfg_filename,
            'tx': tx, 'rx': rx, 'enable': enable, 'locked': locked})

        with open(cfg_filename, w) as f:
            f.write(cfg)

    #
    connectivity_dict = {'chipType': mapping_connectivity['chipType'],
            'chips': connectivity_list}

    #
    with open(directory + cfg_prefix + '.json', 'w') as f:
        json.dump(connectivity_dict, f)

StripsGeometryCoordinate = namedtuple('StripsGeometryCoordinate', 'section layer slot')
# barrel-only? no, it should work for endcaps too
# but they might call things in weird ways, like with sectors etc
# TODO: check TDR for the official jargon https://cds.cern.ch/record/2257755?ln=en

if __name__ == "__main__":
    '''
    Run doctest -- it is a module in the standard Python library
    that pulls the test cases directly from the docstrings and runs them.
    Run it simply as

    $ python scripts/configDB_StripsRunkeys.py

    Or verbose:

    $ python scripts/configDB_StripsRunkeys.py -v
    '''

    import doctest
    doctest.testmod(optionflags=doctest.ELLIPSIS)

