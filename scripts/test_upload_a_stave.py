import json
from sys import argv
import pyconfigdb
import functions_configDB as fxt
import logging
import datetime
from configdb_stripsRunkeys import get_subnode_of_type, get_node_with_payload, get_node_payload, node_repr, RunkeyNode
from configdb_operations import upload_stave


logging.basicConfig(level=logging.INFO)

# our standard runkey reference:
runkey_ref = 'latest'

# inputs:
# the full config of a stave
# and the stave name
#full_stave_cfg_fname = "examples/sr1-barrel-st-4staves/CERNSR1_stave2_testing_setup_full.json"
full_stave_cfg_fname = "examples/sr1-barrel-st-4staves/CERNSR1_stave2_testing_setup_full_small.json"
stave_name = 'AnotherNewStave'  # or use the stave serial number 

# Here you create a stave dictionary:
# YARR connectivity json + all chip "config" fields contain HCC configs
# instead of the file paths to the configs.
# And create a Stave node out of this dictionary.
with open(full_stave_cfg_fname, 'r') as cfg_file:
    stave_cfg = json.load(cfg_file)

stave_node = fxt.build_stave_node_from_dict(stave_name, stave_cfg)

# ConfigDB connection:
_, hostname, port = argv
configdb_url = f'http://{hostname}:{port}/api'
# TODO: the URL for configDB does not really work now
# they require to go via Service Registry
# in CI we use a hack: patch the pyconfigdb ConfigDB class
# to use an environment variable CONFIGDB_API_URI
# to pass the URL inside the class, without going to Service Registry
# so, that's how ConfigDB will connect here
# it won't use this key either:
CONFIGDB_KEY = "demi/default/itk-demo-configdb/api/url"

configDB = pyconfigdb.ConfigDB(dbKey=CONFIGDB_KEY, srUrl=configdb_url)
logging.info(f'ConfigDB is UP: {configDB.health()}')

# How to upload a stave:
upload_stave(configDB, runkey_ref, stave_node)

# test that it worked
# download the runkey and confirm the stave is there
tagTree = configDB.tag_tree(runkey_ref, stage=False, payload_data=True)
logging.info(f'Got full tag tree: {json.dumps(tagTree, indent=2)}')
assert tagTree['objects'][0]['type'] == 'Root'
assert any(ch['type'] == "Staves" for ch in tagTree['objects'][0]['children'])

runkey_root = tagTree['objects'][0]
staves_subtree = get_subnode_of_type(runkey_root, "Staves")

downloaded_stave = get_node_with_payload(staves_subtree['children'], 'staveName', stave_name)
logging.info(f'Found our stave {stave_name} in the downloaded runkey: {downloaded_stave}')
assert downloaded_stave is not None

downloaded_stave = RunkeyNode(downloaded_stave['type'], downloaded_stave['payloads'], downloaded_stave['children'])

# confirm that the downloaded stave is the same as the original node
def eq_types_payloads(node_a, node_b, dont_log=False):
    if dont_log:
        log_info = logging.debug
    else:
        log_info = logging.info

    if 'reuse_id' in node_a:
        # TODO: the IDs change on upload to the database
        # so, you cannot compare them
        # no idea how to handle this nicely.
        # Maybe RunkeyNode should contain a dictionary of node IDs?
        #return node_a['reuse_id'] in (node_b.get('reuse_id'), node_b.get('id'))
        # let's just skip it and return true
        return True

    if 'type' not in node_a:
        logging.warning(f'got a node without type: {node_a}')
        return False

    if 'type' not in node_b:
        logging.warning(f'got a node without type: {node_b}')
        return False

    #if node_a['type'] == node_b['type'] == 'HCC':
    if node_a['type'] == 'HCC':
        log_info == logging.info

    # compare types
    if node_a['type'] != node_b['type']:
        log_info(f'node types are different: {node_a["type"]} vs {node_b["type"]}')
        return False

    # compare payloads
    for pload in node_a['payloads']:
        if pload['type'] == 'id':
            continue

        node_b_pload = get_node_payload(node_b, pload['type'])
        atyp, btyp = pload['type'], node_b_pload['type']
        adata, bdata = pload['data'], node_b_pload['data']

        eq_payload = (atyp==btyp) and (adata==bdata)
        if isinstance(adata, dict) and isinstance(bdata, dict):
            log_info(f'adata and bdata sizes: {len(adata.items())} {len(bdata.items())}')

            for key, val in adata.items():
                if key not in bdata:
                    log_info(f'key {key} not found in bdata')

                elif val != bdata[key]:
                    log_info(f'key {key} values are different:\n{val}\nvs\n{bdata[key]}')

        if not eq_payload:
            log_info(f'node payloads are different: {atyp == btyp} and {adata == bdata}, {type(adata)} and {type(bdata)}')
            log_info(f'node payloads are different:\n{atyp}:\n{json.dumps(adata, sort_keys=True, indent=2)}')
            log_info(f'vs:\n{btyp}:\n{json.dumps(bdata, sort_keys=True, indent=2)}')
            return False

    return True

def eq_nodes(node_a, node_b):
    eq_node_data = eq_types_payloads(node_a, node_b, dont_log=False)
    if not eq_node_data:
        logging.info(f'nodes are different on data')
        return False

    if 'reuse_id' in node_a:
        # again, match all
        return True

    # compare children?
    if len(node_a['children']) != len(node_b['children']):
        logging.info(f'nodes are different in number of children: {len(node_a['children'])} != {len(node_b['children'])}')
        return False

    #logging.info(f'node type {node_a["type"]} has {[ch['type'] for ch in node_a["children"]]} child nodes')
    #logging.info(f'comparing it to type {node_b["type"]} has {[ch['type'] for ch in node_b["children"]]} child nodes')
    eq_children = []
    for ch in node_a['children']:
        # matched child node in b:
        equal_ch_b = None

        # try to match by name
        ch_match = [n for n in node_b['children'] if 'name' in ch and n.get('name') == ch['name']]
        if ch_match:
            assert len(ch_match) == 1
            logging.info(f'found a match by name: {ch["name"]}')
            if eq_types_payloads(ch, ch_match[0], dont_log=False):
                equal_ch_b = ch_match[0]

        else:
            logging.warning(f'did not match by name: {ch.get("name")}')
            for ch_b in node_b['children']:
                matched = False

                if ch.get('type') == 'HCC':
                    logging.warning('node_a is HCC type')
                    matched = eq_types_payloads(ch, ch_b, dont_log=False)

                else:
                    logging.warning(f'node_a is not HCC type: {ch.get("type")} reuse_id: {"reuse_id" in ch}')
                    matched = eq_types_payloads(ch, ch_b, dont_log=True)

                if matched:
                    equal_ch_b = ch_b
                    break

        eq_children.append((equal_ch_b is not None, (ch, equal_ch_b)))

    not_matched = [(ch_a, ch_b) for match, (ch_a, ch_b) in eq_children if not match]
    if not all(match for match, _ in eq_children):
        logging.info(f'nodes:\n{node_repr(node_a)}\nvs\n{node_repr(node_b)}')
        logging.info(f'did not match by types and payloads')
        logging.info(f'nodes number of children: {len(node_a["children"])} vs {len(node_b["children"])}')
        logging.info(f'nodes are different in children: {[match for match, _ in eq_children]}')

        for ch_a, ch_b in not_matched:
            logging.info(f'did not match child:\n{ch_a}\nvs\n{ch_b}')
        return False

    return all(eq_nodes(ch_a, ch_b) for _, (ch_a, ch_b) in eq_children)

assert eq_nodes(stave_node, downloaded_stave)
