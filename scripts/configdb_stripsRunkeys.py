'''
A module with typical operations on Runkey nodes in general, and on Strips-specific nodes.

For example:

>>> node = RunkeyNode('FELIX_DEVICE', payloads={'FID_CONTROLLER_ID': 1})
>>> node
{'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': [], 'id': ...}

get_nested_field and set_nested_field comprise the protocol to work with
conventions nested inside ConfigDB meta payloads. ConfigDB requires meta
payloads to be dictionaries. You can build conventional protocols within
these payloads. For example, 'data' or 'config' payloads that look like this:

    {'type': 'data',   'data':   {<field>: <field_value>}}
    {'type': 'config', 'config': {<field>: <field_value>}}

Example:

    >>> f = RunkeyNode('foo')
    >>> get_nested_field('data', f, 'bar')
    >>> set_nested_field('data', f, bar=55, baz=7)
    >>> get_nested_field('data', f, 'bar')
    55
    >>> f
    {'type': 'foo', 'payloads': [{'type': 'data', 'meta': True, 'data': {'bar': 55, 'baz': 7}}], 'children': [], 'id': ...}
'''

import logging
from collections import namedtuple
import uuid  # str(uuid.uuid4())
import json
import ast  # to use ast.literal_eval - TODO: remove it when we are switched to all meta payloads
from pprint import pprint

# make copies instead of references for debugging
DEBUG_DUPLICATE_PAYLOADS = False


def node_repr(node, depth=1, indent=0):
    if 'reuse_id' in node:
        return f'{" "*indent}reuse_id={node["reuse_id"]}'

    string = f'{" "*indent}type={node["type"]}\n'
    string += f'{" "*indent}id={node.get("id")}\n'
    string += f'{" "*indent}name={node.get("name")}\n'
    string += f'{" "*indent}payloads={json.dumps(node["payloads"], indent=2, sort_keys=True)}\n'

    if depth>0:
        string += f'{" "*indent}children:\n'
        for ch in node['children']:
            string += node_repr(ch, depth-1, indent+2)

    return string

def get_one(a_list: list, assert_one=True):
    if assert_one and len(a_list) != 1:
        raise AssertionError(f'Got a list of len != 1: {a_list}')

    if a_list:
        return a_list[0]
    else:
        return None


def get_subnodes_of_type(runkey_node: dict, type_val: str) -> list:
    return [c for c in runkey_node['children'] if c['type'] == type_val]


def get_subnode_of_type(runkey_node: dict, type_val: str, assert_one=True) -> list:
    res = get_subnodes_of_type(runkey_node, type_val)
    return get_one(res, assert_one)


def get_node_payloads(runkey_node: dict, payload_type: str):
    return [p for p in runkey_node['payloads'] if p['type'] == payload_type]


def get_node_payload(runkey_node: dict, payload_type: str, assert_one=True):
    return get_one(get_node_payloads(runkey_node, payload_type), assert_one)


def get_node_payloads_data(runkey_node: dict, payload_type: str):
    return [p['data'] for p in runkey_node['payloads'] if p['type'] == payload_type]


def get_node_payload_data(runkey_node: dict, payload_type: str, assert_one=True):
    return get_one(get_node_payloads_data(runkey_node, payload_type), assert_one)
    # TODO: maybe always assert one and return the data itself? now it returns 1-item list [data]


def get_nodes_with_payload(runkey_children: list, payload_type: str, payload_val: str):
    return [c for c in runkey_children if get_node_payload_data(c, payload_type) == payload_val]

def get_nodes_with_meta_payload(runkey_children: list, payload_name: str, payload_val: str, meta_payload_name='config'):
    return [c for c in runkey_children if get_nested_field(meta_payload_name, c, payload_name) == payload_val]


def get_node_with_payload(runkey_children: list, payload_type: str, payload_val: str, default=None, assert_one=True):
    nodes = get_nodes_with_payload(runkey_children, payload_type, payload_val)
    if not nodes:
        return default
    return get_one(nodes, assert_one)


def get_subnodes_with_payload(runkey_node: dict, payload_type: str, payload_val: str):
    # note, by default, get_node_payload_data asserts that it finds only 1 payload
    return [c for c in runkey_node['children'] if get_node_payload_data(c, payload_type) == payload_val]


def get_subnode_with_payload(runkey_node: dict, payload_type: str, payload_val: str, default=None, assert_one=True):
    subnodes = get_subnodes_with_payload(runkey_node, payload_type, payload_val)
    if not subnodes:
        return default
    return get_one(subnodes, assert_one)


def get_subnodes_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str):
    return [c for c in runkey_node['children'] if c['type'] == node_type and get_node_payload_data(c, payload_type) == payload_val]


####################

def get_subnode_of_type_with_payload_field(runkey_node, node_type, payload_type, field_name, field_value, assert_one=True):
    subnodes = get_subnodes_of_type(runkey_node, node_type)
    matching_nodes = []
    for subnode in subnodes: # Get the payload with the specified payload_type
        payload_data = get_node_payload_data(subnode, payload_type, assert_one=False)
        if payload_data:
            # If the payload data is a JSON string, parse it
            if isinstance(payload_data, str):
                payload_data = json.loads(payload_data)
            # Check if the field matches the desired value
            if payload_data.get(field_name) == field_value:
                matching_nodes.append(subnode)

    return get_one(matching_nodes, assert_one) if assert_one else matching_nodes

def find_subnode_with_field(runkey_node, node_type, payload_type, field_name, field_value, assert_one=True):
    subnodes = get_subnodes_of_type(runkey_node, node_type)
    # Filter subnodes based on the field value in the specified payload type
    matching_nodes = [] 
    for subnode in subnodes:
        if get_nested_field(payload_type, subnode, field_name, default=None) == field_value:
            matching_nodes.append(subnode)

    return get_one(matching_nodes, assert_one) if assert_one else matching_nodes

####################



def get_subnode_of_type_with_payload(runkey_node: dict, node_type: str, payload_type: str, payload_val: str, default=None, assert_one=True):
    subnodes = get_subnodes_of_type_with_payload(runkey_node, node_type, payload_type, payload_val)
    if not subnodes:
        return default
    return get_one(subnodes, assert_one)

def get_subnodes_of_type_with_nested_field(runkey_node: dict, node_type: str, payload_name: str, payload_val, default=None):
    #meta_payload_name = 'config'
    subnodes = get_subnodes_of_type(runkey_node, node_type)
    # get_nodes_with_meta_payload(runkey_children: list, payload_name: str, meta_payload_name='config'):
    return get_nodes_with_meta_payload(subnodes, payload_name, payload_val)


# to make it more customizable, it needs a closure around these functions?
#TYPE_W_FIELDS = 'data'
# no, just put it in parameters

def get_nested_field(TYPE_W_FIELDS, node, name, default=None):
    '''get_nested_field(TYPE_W_FIELDS, node, name, default=None)

    Returns a field for a nested meta payload of structure like this:
    {'type': 'data', 'data': {<name>: <return_value>}}

    If the payload or the field not found, returns default.
    '''

    #if not any(typ == TYPE_W_FIELDS for typ in node['payloads']['type']):
    if len(get_node_payloads(node, TYPE_W_FIELDS)) == 0:
        return default

    payload = get_node_payload(node, TYPE_W_FIELDS)
    assert payload.get('meta') is True, f"Something is wrong: '{TYPE_W_FIELDS}' payload must be meta"
    payload_data = get_node_payload_data(node, TYPE_W_FIELDS)
    assert isinstance(payload_data, dict), f"Something is wrong: '{TYPE_W_FIELDS}' payload must be meta i.e. dict"

    return payload_data.get(name)

def set_nested_field(TYPE_W_FIELDS, node, **data_fields):
    # dict() .get('key'[, defaultvalue or None])
    if len(get_node_payloads(node, TYPE_W_FIELDS)) == 0:
        node.add_payload(TYPE_W_FIELDS, {}, meta=True)

    payload_data = get_node_payload_data(node, TYPE_W_FIELDS)
    assert isinstance(payload_data, dict), f"Something is wrong: '{TYPE_W_FIELDS}' payload must be meta i.e. dict"

    #payload[name] = value
    payload_data.update(data_fields)


class Payload(dict):
    '''
    A payload is a simple dictionary with 'type' and 'data' fields.
    It can have additional fields 'meta' and 'id' for special purposes.

    A payload can be given a UUID in 'id' field directly by a user
    and it can be a reference, i.e. it contains only the ID
    of the payload it refers to.

    TODO: we still need to figure out how the UUID works.
    When we refer to a UUID, do we need to have the 'type' and 'data' fields?
    It looks like to refer a UUID of a Node, you create a node with only:
    {'reuse_id': <UUID>} and that's it.

    >>> pl = Payload('config', '{"Foo": 5}')
    >>> pl
    {'type': 'config', 'data': '{"Foo": 5}'}

    >>> pl_autostr = Payload('config', {"Foo": 5})
    >>> pl_autostr
    {'type': 'config', 'data': "{'Foo': 5}"}

    >>> pl = Payload('config', {"Foo": 5}, name='hey')
    >>> pl
    {'type': 'config', 'data': "{'Foo': 5}", 'name': 'hey'}

    >>> args_basic = 'config', {"Foo": 5}
    >>> args_more = {'name': 'hey'}
    >>> pl = Payload(*args_basic, **args_more)
    >>> pl
    {'type': 'config', 'data': "{'Foo': 5}", 'name': 'hey'}

    Correct Meta payload:
    >>> pl_meta = Payload('config', {'Foo': 5}, meta=True)
    >>> pl_meta
    {'type': 'config', 'meta': True, 'data': {'Foo': 5}}

    Incorrect Meta payload with correct json inside:
    >>> pl_meta = Payload('config', '"Foo_Config"', meta=True)
    Traceback (most recent call last):
      ...
    AssertionError: Meta payloads can only be dictionaries, got: <class 'str'> Foo_Config

    Incorrect Meta payload with string that contains incorrect json inside:
    >>> pl_meta = Payload('config', 'Foo_Config', meta=True)
    Traceback (most recent call last):
      ...
    AssertionError: Meta payloads can only be json dictionaries/objects or strings with dictionaries
    '''

    def __init__(self, *args, **kwargs):
        '''__init__(self, *args, **kwargs):

        It expects the following arguments:
        payload_type, payload_data, payload_id=None, name=None, meta=False

        But payload_type, payload_data and payload_id can be passed as named
        arguments with names: type, data, id. I.e. the Python built in names.
        '''

        # check the input arguments are correct:
        allowed_payload_args = ('type', 'data', 'id', 'name', 'meta')
        assert all(key in allowed_payload_args for key in kwargs), f"Wrong payload arg: {kwargs.keys()}"
        assert len(args) < 3, f"Wrong number of position arguments: {len(args)} < 3"
        if len(args) == 1:
            assert 'type' not in kwargs, "Both position and keyword args set 'type' for Payload"
            kwargs['type'] = args[0]
        elif len(args) == 2:
            arg_type, arg_data = args
            assert 'type' not in kwargs and 'data' not in kwargs, "'type' and/or 'data' are given in both position and keyword args for Payload"
            kwargs['type'] = arg_type
            kwargs['data'] = arg_data

        # a Payload must have 'type' and 'data'
        # 'data' is either a "blob" i.e. str or bytes
        # or a dictionary, when meta=True
        self['type'] = str(kwargs['type'])
        payload_data = kwargs['data']

        if kwargs.get('meta'):  # TODO test again how this works. E.g. do we need this "meta" in the reference payloads?
            # TODO: check with configDB is this is intentional
            #       they return meta payload 'data' as string!
            # now we allow a string that contains a json dictionary/object
            if isinstance(payload_data, str):
                # try to parse json out of it
                try:
                    payload_data = json.loads(payload_data)

                except json.decoder.JSONDecodeError as e:
                    #
                    raise AssertionError(f"Meta payloads can only be json dictionaries/objects or strings with dictionaries")

            assert isinstance(payload_data, dict), f"Meta payloads can only be dictionaries, got: {type(payload_data)} {payload_data}"
            # it must be a dictionary
            # but!
            # since we also get it from the jsons from configdb
            # it must be compatible to json
            # i.e. no tuples, only lists
            # let's just convert it back and forth
            inp_data = payload_data
            payload_data = json.loads(json.dumps(inp_data))

            # here the payload must be a dictionary/json object
            self['meta'] = True

        else:
            payload_data = payload_data if isinstance(payload_data, str) else str(payload_data)

        self['data'] = payload_data

        if kwargs.get('id'):
            self['id'] = kwargs['id']

        if kwargs.get('name'):
            self['name'] = kwargs['name']

    def clone(self):
        '''clone(self)

        Returns a shallow copy of self, with a new ID.

        >>> pl = Payload('pltype', {'pldata': 55}, meta=True)
        >>> cl = pl.clone()
        >>> cl
        {'type': 'pltype', 'meta': True, 'data': {'pldata': 55}, 'id': ...}
        >>> isinstance(cl, Payload)
        True
        '''

        ret = self.copy()
        ret['id'] = str(uuid.uuid4())
        return Payload(**ret)

    def make_reference(self, duplicate=DEBUG_DUPLICATE_PAYLOADS):
        '''make_reference(self, duplicate=DEBUG_DUPLICATE_PAYLOADS)

        Returns a special reference payload: {'reuse_id': <uuid>}.
        The duplicate option changes it back to cloning, for debugging.
        DEBUG_DUPLICATE_PAYLOADS sets the default for the whole module.

        >>> pl = Payload('pltype', {'pldata': 55}, meta=True)
        >>> cl = pl.make_reference()
        >>> cl 
        {'reuse_id': ...}
        >>> cl = pl.make_reference(duplicate=True)
        >>> cl
        {'type': 'pltype', 'meta': True, 'data': {'pldata': 55}, 'id': ...}
        >>> isinstance(cl, Payload)
        True
        '''

        if 'id' not in self:
            self['id'] = str(uuid.uuid4())

        # just for debugging - an option to clone instead of reference
        if duplicate:
            #new_payload = Payload(**self)
            #new_payload['id'] = str(uuid.uuid4())
            #return {'id': new_payload['id']}  # new_payload
            return self.clone()

        else:
            # Check how references work in payloads:
            # we should have 'reuse_id' and a clone method?
            #return Payload(id=self['id'])  # TODO check should it be 'reuse_id'?
            return {'reuse_id': self['id']}


def is_a_payload_dict(x):
    res = isinstance(x, dict)  # TODO: maybe use abstract class instead of dict?
    res &= len(x) == 2         # only 2 keys
    res &= 'type' in x and 'data' in x
    return res


def payloads_from_dict(dictionary):
    return [Payload(key, str(val)) for key, val in dictionary.items()]


def load_payloads_from_dict(rkey_payloads, dictionary):
    for p in payloads_from_dict(dictionary):
        rkey_payloads.setdefault('payloads', []).append(p)


class RunkeyNode(dict):
    '''
    A class for a general Runkey node. Nothing Strips-specific.

    >>> node = RunkeyNode('FELIX_DEVICE', payloads={'FID_CONTROLLER_ID': 1})
    >>> node
    {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': [], 'id': ...}
    >>> import pprint
    >>> pprint.pp(node)
    {'type': 'FELIX_DEVICE',
     'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}],
     'children': [],
     'id': ...}
    >>> [t for t, d in [p.values() for p in node.payloads]] # i.e. I rely on the fact that items are ordered
    ['FID_CONTROLLER_ID']

    >>> flx_node = RunkeyNode('FELIX', {'FID_DETECTOR_ID': 0}, [node])
    >>> flx_node
    {'type': 'FELIX', 'payloads': [{'type': 'FID_DETECTOR_ID', 'data': '0'}], 'children': [{'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': [], 'id': ...}], 'id': ...}
    >>> dict(flx_node)
    {'type': 'FELIX', 'payloads': [{'type': 'FID_DETECTOR_ID', 'data': '0'}], 'children': [{'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': [], 'id': ...}], 'id': ...}

    >>> flx_node.add_payload("some_type", "some_data", name='and_name')
    >>> pprint.pp(flx_node)
    {'type': 'FELIX',
     'payloads': [{'type': 'FID_DETECTOR_ID', 'data': '0'},
                  {'type': 'some_type', 'data': 'some_data', 'name': 'and_name'}],
     'children': [{'type': 'FELIX_DEVICE',
                   'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}],
                   'children': [],
                   'id': ...}],
     'id': ...}

    >>> import json
    >>> stave_node = RunkeyNode('Stave', {'UUID': 'unknown', 'staveName': 'PPB1A', 'staveType': 'SS', 'chipType':  'Star_h1a1'})
    >>> json.dumps(stave_node, default=dict)
    '{"type": "Stave", "payloads": [{"type": "UUID", "data": "unknown"}, {"type": "staveName", "data": "PPB1A"}, {"type": "staveType", "data": "SS"}, {"type": "chipType", "data": "Star_h1a1"}], "children": [], "id": ...}'

    >>> node_fields = RunkeyNode('FELIX_DEVICE')
    >>> node_fields
    {'type': 'FELIX_DEVICE', 'payloads': [], 'children': [], 'id': ...}
    >>> node_fields.set_field(FID_DETECTOR_ID=0)
    >>> node_fields.get_field('FID_DETECTOR_ID')
    0
    >>> node_fields
    {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'data', 'meta': True, 'data': {'FID_DETECTOR_ID': 0}}], 'children': [], 'id': ...}
    '''

    def __init__(self, node_type, payloads={}, children=[]):
        self.payloads = []
        self.children = []
        self.type     = node_type
        self.update({'type': self.type, 'payloads': self.payloads, 'children': self.children})
        # TODO: check whether a node can have more than just 'type' attribute?
        #       it looks like it has "id". Check if it has a 'name'.
        self['id'] = str(uuid.uuid4())

        if isinstance(payloads, list):  # then it is the runkey format
            for p_dict in payloads:
                assert isinstance(p_dict, dict)
                # self['payloads'].append(Payload(p_dict.get('type'), p_dict.get('data'), p_dict.get('id')))
                self['payloads'].append(Payload(**p_dict))

        elif isinstance(payloads, dict):  # then it is the succinct Python format
            for p_type, p_data in payloads.items():
                self['payloads'].append(Payload(p_type, p_data))

        for c in children:
            if isinstance(c, RunkeyNode):
                self['children'].append(c)
            else:
                self['children'].append(RunkeyNode(c['type'], c.get('payloads', {}), c.get('children', [])))

    def add_payload(self, *args, **kwargs):
        # TODO: test if the same type payload exists?
        self['payloads'].append(Payload(*args, **kwargs))

    def add_child(self, rkey_node):
        assert isinstance(rkey_node, RunkeyNode)
        self['children'].append(rkey_node)

    def make_reference(self):
        '''make_reference(self)

        Returns a special reference node:
        {'reuse_id': <self['id']>}

        >>> node = RunkeyNode('foo')
        >>> ref = node.make_reference()
        >>> ref
        {'reuse_id': ...}
        >>> ref['reuse_id'] == node['id']
        True
        '''

        self_id = self.get('id')
        if self_id is None:
            self_id = str(uuid.uuid4())
            self['id'] = self_id

        return {'reuse_id': self_id}

    def clone(self):
        '''clone(self):

        Copy the node, including its payloads and children.

        >>> node = RunkeyNode('FELIX_DEVICE', payloads={'FID_CONTROLLER_ID': 1})
        >>> node
        {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': [], 'id': ...}
        >>> node_copy = node.clone()
        >>> node_copy
        {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1', 'id': ...}], 'children': [], 'id': ...}
        >>> node
        {'type': 'FELIX_DEVICE', 'payloads': [{'type': 'FID_CONTROLLER_ID', 'data': '1'}], 'children': [], 'id': ...}
        '''

        clone_type = self.type
        clone_payloads = [p.clone() for p in self['payloads']]
        clone_children = [c.clone() for c in self['children']]

        return RunkeyNode(clone_type, clone_payloads, clone_children)

    def get_field(self, name, default=None):
        '''get_field(self, name, default=None)

        >>> node = RunkeyNode('foo')
        >>> node.get_field('bar')
        >>> node.set_field(bar=55, baz=7)
        >>> node.get_field('bar')
        55
        '''

        return get_nested_field('data', self, name, default)

    def set_field(self, **data_fields):
        '''set_field(self, **data_fields)

        Uses key-values from data_fields to set fields in a "data" payload:
        {'type': 'data', 'data': {<key>: <value>}}

        >>> node = RunkeyNode('foo')
        >>> node.set_field(bar=55, baz=7)
        >>> node
        {'type': 'foo', 'payloads': [{'type': 'data', 'meta': True, 'data': {'bar': 55, 'baz': 7}}], 'children': [], 'id': ...}
        '''
        return set_nested_field('data', self, **data_fields)

    def get_config(self, name, default=None):
        return get_nested_field('config', self, name, default)

    def set_config(self, **data_fields):
        return set_nested_field('config', self, **data_fields)

class RunkeyStave(RunkeyNode):
    def __init__(self, name, stave_type, stave_uuid, chip_type, **more_payloads):
        all_payloads = {'staveName': name, 'UUID': stave_uuid, 'staveType': stave_type, 'chipType': chip_type}
        self.side_m = None  # TODO: these shortcuts can be handy if they are never updated - figure out on practice if it is true
        self.side_s = None

        if 'children' in more_payloads:
            # then it must be a list of child nodes
            children = more_payloads['more_payloads']
            more_payloads.pop('more_payloads')
            if not isinstance(children, list):
                raise ValueError(f'got a "children" argument that is not a list: {children}')
            # a stave can have only 2 children: the stave sides
            # TODO should check the names of the sides, or make a RunkeySide class and test for it etc
            if not (len(children) == 2 and all(c['type'] == 'StaveSide' for c in children)):
                raise ValueError(f'got a "children" that is not a pair of stave sides: {children}')
            m = get_node_with_payload(children, 'sideType', 'side_m')
            s = get_node_with_payload(children, 'sideType', 'side_s')
            self.side_m = m if isinstance(m, RunkeyNode) else RunkeyNode(m['type'], m['payloads'], m['children'])
            self.side_s = s if isinstance(s, RunkeyNode) else RunkeyNode(s['type'], s['payloads'], s['children'])

        else:
            self.side_m = RunkeyNode('StaveSide', {'sideType': 'side_m'})
            self.side_s = RunkeyNode('StaveSide', {'sideType': 'side_s'})

        children = []
        children.append(self.side_m)
        children.append(self.side_s)

        all_payloads.update(more_payloads)
        super().__init__(node_type='Stave', payloads=all_payloads, children=children)

    # again, I cannot make this handy iterator because of the json dump
    # json iterates over the nodes, it expects to iterate over keys of a dictionary-like node
    #def __iter__(self):
    #    return (c for c in self['children']) # TODO make sure the order is right?

    # TODO let's think if we need to re-implement this method to preserve the obejct class in the reference
    # def clone(self)

    def name(self) -> str:
        '''name(self) -> str

        Returns the stave name.

        The current implementation is to store it as a separate payload of type 'staveName'.
        We may move it under the common meta payload 'config'. Or the node itself may get
        a new item called 'name'. This interface should remain consistent through such changes.
        '''

        return get_node_payload_data(self, 'staveName')[0]


class RunkeyHCC(RunkeyNode):
    '''
    RunkeyHCC just defines the protocol of our HCC nodes.
    I.e. which payloads they carry, and which payloads are meta.
    '''

    def __init__(self, itsdaq_number, module_number, hybrid_name, rx_elink, tx_elink, config="", **more_payloads):
        # Payload arguments:
        # allowed_payload_args = ('type', 'data', 'id', 'name', 'meta')
        payload_dict = {
            'itsdaq_number': itsdaq_number,
            'module_number': module_number,
            'hybrid_name': hybrid_name,
            'rx_elink': rx_elink,
            'tx_elink': tx_elink,
            'config': config,
            **more_payloads
            }
        self['name'] = "HCC"+str(itsdaq_number)
        all_payloads = [Payload("config", payload_dict, name=self['name'], meta=True)]
        
        # Transitioning to using only 1 meta payload for all the data ^^^
        ''' 
        all_payloads = [Payload('itsdaq_number', itsdaq_number),
                        Payload('module_number', module_number),
                        Payload('hybrid_name', hybrid_name),
                        Payload('rx_elink', rx_elink),
                        Payload('tx_elink', tx_elink),
                        Payload('config', config),
                        ]
        '''
        # TODO: we'll need some of these payloads to be meta, or save all of them as one large meta?

        if 'children' in more_payloads:
            logging.warning(f'Unexpected children nodes for an HCC: {more_payloads["children"]}')
            more_payloads.pop('children')

        all_payloads += [Payload(k, v) for k, v in more_payloads.items()]
        super().__init__(node_type='HCC', payloads=all_payloads)

        # and assign a UUID to the config payload
        # conf = get_node_payload(self, 'config')
        # conf['id'] = str(uuid.uuid4())
        # Now we don't use the IDs really

    # TODO: check if it is still used anywhere and remove it
    #       RunkeyNode implements a proper clone method now.
    # TODO: the RunkeyNode clone won't preserve the type of the object
    #       it will return the RunkeyNode -- how to make it "virtual" and preserve the type?
    def copy(self):
        '''
        return a deep copy of everything, but refer the config payload

        >>> hcc = RunkeyHCC(str(1), str(0), "Y", ["0", "14"], "5", enable="True", locked="False")
        >>> hcc
        {'name': 'HCC1', 'type': 'HCC', 'payloads': [{'type': 'config', 'meta': True, 'data': {'itsdaq_number': '1', 'module_number': '0', 'hybrid_name': 'Y', 'rx_elink': ['0', '14'], 'tx_elink': '5', 'config': '', 'enable': 'True', 'locked': 'False'}, 'name': 'HCC1'}, {'type': 'enable', 'data': 'True'}, {'type': 'locked', 'data': 'False'}], 'children': [], 'id': '...'}
        >>> hcc.copy()
        {'type': 'HCC', 'payloads': [{'type': 'enable', 'data': 'True'}, {'type': 'locked', 'data': 'False'}, {'reuse_id': '...'}], 'children': [], 'id': '...', 'name': 'HCC1'}
        '''

        pls = self['payloads']
        # copy all payloads except config
        new_node = RunkeyNode('HCC',
                              {p['type']: p['data'] for p in pls if p['type'] != 'config'})

        # and reference the config payload
        cfg_payload = get_node_payload(self, 'config')
        #new_node['payloads'].append({'id': cfg_payload['id']})
        new_node['payloads'].append(cfg_payload.make_reference())

        if 'name' in self:
            new_node['name'] = self['name']

        return new_node


def runkey_fiber_to_yarr_connectivity(rkey_felix):
    '''
    Input: a json tree Felix > FELIX_DEVICE > GBT_LINKs > stave fibers > elinks > chips.
    Output: a list of chip connectivity, and connectivity configs.

    1 FELIX device = 1 DMA connection & 1 RDMA connection.
    Hence everything is grouped by the card id and device id pairs.
    But we typically also group it per stave...
    Let's add stave names in the payloads of their fibers then.
    So, we'll have the mappings:
    (card, device = controller ID), staveName => hcc itsdaq_number => a pair of TX & RX gbt_number, elink_number
    or just a list of whatever is hcc full name to elinks
    and
    staveName and hcc itsdaq_number => the full elink mapping and hcc config
    i.e. we need the same thing in both directions?

    And I need to see the connectivity by:
    gbt link, egroup, epath -- hcc module, hybrid
    '''

    #
    mapping_configs = {}

    detector_id = get_node_payload_data(rkey_felix, 'FID_DETECTOR_ID')
    print(f"Detector ID: {detector_id}")
    # import pdb; pdb.set_trace()

    for rkey_flx_device in get_subnodes_of_type(rkey_felix, 'FELIX_DEVICE'):
        controller_id = get_node_payload_data(rkey_flx_device, 'FID_CONTROLLER_ID')  # TODO: confirm that "controller_id" gets you a felix _device_, not a whole card

        for rkey_fiber in rkey_flx_device['children']:
            its_rx_fiber = rkey_fiber['type'] == 'GBT_RX'
            gbt_link = get_node_payload_data(rkey_fiber, 'link')

            # this link node must have only 1 child -- the stave fiber that is connected to it
            if len(rkey_fiber['children']) == 0:
                continue

            assert len(rkey_fiber['children']) == 1
            #
            rkey_stave_fiber = rkey_fiber['children'][0]
            staveName = get_node_payload_data(rkey_stave_fiber, 'staveName')  # TODO: so, currently it does not work, because the payload UUIDs are not infolded into payloads
            # TODO maybe emulate how it should work by keeping a dict with UUID: payload stuff?
            #      we need something like that for testing
            chipType  = get_node_payload_data(rkey_stave_fiber, 'chipType')

            for rkey_elink in get_subnodes_of_type(rkey_stave_fiber, 'elink'):
                #
                elink_number = get_node_payload_data(rkey_elink, 'elink_number')

                # again, the elink must have only 1 child, the hcc
                assert len(rkey_elink['children']) == 1, f"Not 1 child under elink: {len(rkey_elink['children'])}"
                node_dict = rkey_elink['children'][0]
                # TODO we should be able to turn the dict to a RunkeyNode here
                #      that's where json schemas should come in
                #      also, we should be able to make RunkeyHCC
                rkey_hcc = RunkeyNode(node_dict['type'], node_dict['payloads'], node_dict['children'])

                # get_nested_field(meta_payload_name, c, payload_name)
                itsdaq_number = rkey_hcc.get_config('itsdaq_number')
                module_number = rkey_hcc.get_config('module_number')
                hybrid_name   = rkey_hcc.get_config('hybrid_name')
                hcc_config    = rkey_hcc.get_config('config')

                # TODO: again, these two must be scan configs, not chip configs
                enable = rkey_hcc.get_config('enable') 
                locked = rkey_hcc.get_config('locked')

                # pack all of this into the mappings
                # hcc configs
                hcc_info = mapping_configs.setdefault((staveName, chipType, module_number, hybrid_name, itsdaq_number), {})
                if its_rx_fiber:
                    egroup = get_node_payload_data(rkey_elink, 'egroup')
                    epath  = get_node_payload_data(rkey_elink, 'epath')
                    hcc_info['rx'] = detector_id, controller_id, gbt_link, egroup, epath, elink_number

                else:
                    tx_group = get_node_payload_data(rkey_elink, 'tx_group')
                    encoder_id = get_node_payload_data(rkey_elink, 'encoder_id')
                    hcc_info['tx'] = detector_id, controller_id, gbt_link, tx_group, encoder_id, elink_number

                # TODO: use the full reference tuples?
                if 'config' not in hcc_info:
                    hcc_info['config'] = hcc_config
                    hcc_info['enable'] = enable
                    hcc_info['locked'] = locked

                ## and the stave mapping
                #connect_info = mapping_connectivity.setdefault((detector_id, controller_id, staveName), {})
                #connect_info.setdefault(itsdaq_number, {})
                # no wait, this one can be simpler

    #
    mapping_connectivity = {'hccs': {}}
    for full_hcc_id, hcc_info in mapping_configs.items():
        # full_hcc_id the ID stave-wise
        # the mapping are tx and rx
        # let's merge all of them into one full identifier
        # hcc_name = 'hcc' + '-'.join(full_hcc_id)
        rx_name  = 'rx' + '-'.join(hcc_info['rx'])
        tx_name  = 'tx' + '-'.join(hcc_info['tx'])

        _, chipType, _, _, _ = full_hcc_id  # TODO: also not perfect

        # elink numbering that we currently use
        # TODO: transition to using full FID
        _, _, rx_gbt_link, _, _, rx_elink_num = hcc_info['rx']
        _, _, tx_gbt_link, _, _, tx_elink_num = hcc_info['tx']
        rx_elink = int(rx_gbt_link)*0x40 + int(rx_elink_num)
        tx_elink = int(tx_gbt_link)*0x40 + int(tx_elink_num)

        mapping_connectivity['hccs'][(full_hcc_id, rx_name, tx_name)] = hcc_info['config'], rx_elink, tx_elink, hcc_info['enable'], hcc_info['locked']
        # TODO: or, make the enable and locked options here
        mapping_connectivity['chipType'] = chipType # i.e. I assume all chips are the same, and overwrite the type here

    # and we need only this bit:
    return mapping_connectivity


def write_cfg_files(mapping_connectivity, directory='./', cfg_prefix='CERNSR1_testing_setup'):
    connectivity_dict = {}  # TODO: damn, where is chipType

    for (full_hcc_id, rx_name, tx_name), full_hcc_cfg in mapping_connectivity['hccs'].items():
        staveName, _, _, _, itsdaq_number = full_hcc_id
        full_hcc_name = '-'.join(str(i) for i in full_hcc_id) + '_' + rx_name + '_' + tx_name + f'_hcc{itsdaq_number}'
        # chip config filename is relative to the connectivity filename
        cfg_filename = './' + cfg_prefix + '_' + staveName + '_' + full_hcc_name + '.json'
        cfg, rx, tx, enable, locked = full_hcc_cfg

        connectivity_dict.setdefault(staveName, [])
        connectivity_dict[staveName].append({'config': cfg_filename,
                                             'tx': tx,
                                             'rx': rx,
                                             'enable': enable,
                                             'locked': locked})

        with open(directory + '/' + cfg_filename, 'w') as f:
            config = cfg  # ast.literal_eval(cfg)
            assert isinstance(config, dict)
            json.dump(config, f, indent=2)

    for staveName, chip_configs in connectivity_dict.items():
        stave_connectivity = {'chipType': mapping_connectivity['chipType'],
                              'chips': chip_configs}

        with open(directory + '/' + cfg_prefix + f'-{staveName}' + '.json', 'w') as f:
            json.dump(stave_connectivity, f, indent=2)


StripsGeometryCoordinate = namedtuple('StripsGeometryCoordinate', 'section layer slot')
# barrel-only? no, it should work for endcaps too
# but they might call things in weird ways, like with sectors etc
# TODO: check TDR for the official jargon https://cds.cern.ch/record/2257755?ln=en

if __name__ == "__main__":
    '''
    Run doctest -- it is a module in the standard Python library
    that pulls the test cases directly from the docstrings and runs them.
    Run it simply as

    $ python scripts/configdb_stripsRunkeys.py

    Or verbose:

    $ python scripts/configdb_stripsRunkeys.py -v
    '''

    import doctest
    exit(doctest.testmod(optionflags=doctest.ELLIPSIS)[0])
