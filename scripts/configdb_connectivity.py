# Detailed description at the bottom of the file
# TLDR: Functions for translating ITSDAQ numbers to stave side, module number and hybrid name
# TODO: Move to fxts file?

from collections import namedtuple

ReferenceHCCLocation = namedtuple("ReferenceHCCLocation", "stave_side module_number hybrid_name")
ReferenceHCCFelixRX  = namedtuple("ReferenceHCCFelixRX",  "device gbt_link raw_elink elink egroup epath")
ReferenceHCCFelixTX  = namedtuple("ReferenceHCCFelixTX",  "device gbt_link raw_elink elink tx_group encoder_id swapped")

def translate_ITSDAQ_number(number: int, short_strip=False):
    '''
    translate_ITSDAQ_number(number: int, short_strip=False)

    returns ReferenceHCCLocation(stave_side, module_number, hybrid_name)
    '''

    # Check if hybrid is main or secondary side
    max_hybrids_on_one_side = 28 if short_strip else 14
    assert number < 2*max_hybrids_on_one_side
    stave_side = 'm' if number < max_hybrids_on_one_side else 's'

    # remove the stave side offset
    if stave_side == 's':
        number -= max_hybrids_on_one_side
    
    module_number = number // 2 if short_strip else number

    hybrid_name = ['Y', 'X'][number % 2] if short_strip else 'X'
    # TODO: check the order of Y and X in ITSDAQ numbers
    # but I think it is indeed like that: a smaller number = Y, a larger = X

    return ReferenceHCCLocation(stave_side, module_number, hybrid_name)

device_offset = 2048
link_offset = 0x40 

def translate_Felixcore_RX_elink(elink_num: int):
    '''
    translate_Felixcore_RX_elink(elink_num: int)

    returns ReferenceHCCFelixRX(device, link, elink, egroup, epath)
    All are integers.
    '''

    device_number = elink_num // device_offset
    elink_num -= device_number * device_offset # same as elink_num % device_offset
    link_number = elink_num // link_offset # 0x40 = 64

    # final elink number, within the optical link
    elink_number = elink_num - link_number * link_offset

    # egroup contains 4 epaths
    egroup_number = elink_number // 4
    epath_number  = (elink_number - egroup_number * 4)
    assert epath_number in (0, 2) # in our FW we have only these two (at least now)

    return ReferenceHCCFelixRX(device_number, link_number, link_number* link_offset + elink_number, elink_number, egroup_number, epath_number)

def translate_Felixcore_TX_elink(elink_num: int):
    '''
    translate_Felixcore_TX_elink(elink_num: int)

    returns (device, link, tx_group, encoder_id).
    All are integers. Typically, `encoder_id` is always 1.
    '''

    device_number = elink_num // device_offset
    elink_num -= device_number * device_offset

    link_number = elink_num // link_offset # 0x40 = 64
    elink_num -= link_number * link_offset

    # tx_group currently contains 5 elinks, for 2 encoders and their features
    # in future we may completely get rid of one of the encoders and some of features
    number_of_elinks_per_tx_group = 5
    tx_group = elink_num // number_of_elinks_per_tx_group

    encoder_id = elink_num - tx_group * number_of_elinks_per_tx_group
    # typically we deal only with LCB command elink, which is number 1
    # but there is a gotcha: our chips have 2 command inputs, LCB commands and R3L1
    # R3L1 is one special feature that was abandoned for the foreseen future by FELIX
    # nevertheless, the chips on the staves do have 2 electrical lines that lead to them
    # and the gotcha is that on some stave sides these lines are swapped at the lpGBT
    # so, currently, we swap them in software: some HCCs are treated as if we talk through the R3L1 encoder
    # in future, we should not do this swapping in the software! we should do it in FELIX FW - we 
    # already have the necessary feature there but for now let's allow both enocders:
    encoder_id_lcb  = 1
    encoder_id_r3l1 = 4
    assert encoder_id in (encoder_id_lcb, encoder_id_r3l1)
    # but! let's save if the command lines are swapped
    swapped = encoder_id != encoder_id_lcb

    return ReferenceHCCFelixTX(device_number, link_number, link_number*link_offset + elink_num, elink_num, tx_group, encoder_id, swapped)

def connectivity_to_payload(hccNumber: int, rx: int, tx: int, short_strip=False):
    '''
    connectivity_to_dict(hccNumber: int, rx: int, tx: int, short_strip=False)

    returns a payload dictionary with the connectivity information
    '''
    itsdaq_info = translate_ITSDAQ_number(hccNumber, short_strip)
    rx_info = translate_Felixcore_RX_elink(rx)
    tx_info = translate_Felixcore_TX_elink(tx)

    conn_dict = {
        "ReferenceHCCLocation": {
            "stave_side": itsdaq_info.stave_side,
            "module_number": itsdaq_info.module_number,
            "hybrid_name": itsdaq_info.hybrid_name
        },
        "ReferenceHCCFelixRX": {
            "device": rx_info.device,
            "gbt_link": rx_info.gbt_link,
            "raw_elink": rx_info.raw_elink,
            "elink": rx_info.elink,
            "egroup": rx_info.egroup,
            "epath": rx_info.epath
        },
        "ReferenceHCCFelixTX": {
            "device": tx_info.device,
            "gbt_link": tx_info.gbt_link,
            "raw_elink": tx_info.raw_elink,
            "elink": tx_info.elink,
            "tx_group": tx_info.tx_group,
            "encoder_id": tx_info.encoder_id,
            "swapped": tx_info.swapped
        }  
    }

    return conn_dict


'''
From `run_YARR` you have 2 types of config files: actual configs of the FE chips,
they end in `*_hcc<ITSDAQ number for the HCC>.json`, and the connectivity config,
which specifies the FELIX TX and RX elinks that are connected to the HCC.

Short Strips staves contain 56 HCCs: 
    2 sides, 
    each side contains 14 modules, 
    each module contains 2 hybrids, called X and Y. 
    Hybrid is the board that hosts 1 HCC and its ABCs. 
Long Strips stave contains 28 HCCs: 
    because each module hosts only
    1 hybrid, i.e. 1 HCC. 


Currently we have 4 staves in the system test: A, B and C are Short Strips, 
D is the only Long Strip. Also, `run_YARR` produces their configs
with numbers. In our case it is 0 for A, 1 for B, 2 for C, and 3 for D. You can
check the number of FE configs:
    $ ls CERNSR1_stave0_testing_hcc*json | wc -l
        56
    $ ls CERNSR1_stave1_testing_hcc*json | wc -l
        56
    $ ls CERNSR1_stave2_testing_hcc*json | wc -l
        56
    $ ls CERNSR1_stave3_testing_hcc*json | wc -l
        28

First, the ITSDAQ numbering scheme for the HCCs is pretty simple:
    HCC number = 28 * [1 or 2 depending on the stave side, main or secondary]
      module_number * [1 or 2 for LS or SS stave] + [0 or 1 for hybrid Y or X]

So, I propose to convert the ITSDAQ numbers into a tuple with stave side, module
number and X or Y for each hybrid/HCC. An example is `translate_ITSDAQ_number`.
------------------------------------------------------------------------------------------------
Then, the connectivity file contains raw elink numbers that were used for Felixcore.
They should be converted into optical link numbers and FELIX FW egroups. It is also simple.
The connectivity configs look like this:

    $ head CERNSR1_stave0_testing_setup.json
    {
      "chipType": "Star_vH1A1",
      "chips": [
        {
          "config": "./CERNSR1_stave0_testing_hcc0.json",
          "tx": 464,
          "rx": 384,
          "enable": 1,
          "locked": 1
        },

Here, the raw elink numbers `tx=464` and `rx=384` include the optical link number,
which is also called GBT link number:

    elink number = optical_link * 0x40 + elink_number_within

Also, the PPB1D stave sits on FELIX FW "device 1", and the felixcore configs offset
device 1 by 2048:

    $ head CERNSR1_stave3_testing_setup.json
    {
      "chipType": "Star_vH1A1",
      "chips": [
        {
          "config": "./CERNSR1_stave3_testing_hcc0.json",
          "tx": 2192,
          "rx": 2176,
          "enable": 1,
          "locked": 1
        },

A FELIX "device" corresponds to 12 optical links. Coincidentally, optical cables
also come in groups 12 fibers within them.

Let's first work with the RX elinks, and then cover the TX. They are a little different.
But what was said about the offsets above is the same in both cases.

Let's convert the old elink numbers into something like this:

    device_number, link_number, elink_number

In the FELIX connectivity tree, you'd have a node structure like this:

    FELIX
      device 0
      device 1
        TX
          to be done later
        RX
          link 0
          link 1
          link 2
        ...
          link 11
              elink 0
              elink 2
            ...
              elink N
                 & reference to HCC N

Here, `elink_number` is the elink within 1 optical link. But, FELIX FW organises
this bandwidth into `egroups` and `epaths`. I.e. many FELIX FW registers correspond
to "egroups", not "elinks". The user has to match the elinks in software with the
egroups in firmware. Let's cover that in the config DB.

    elink_number = egroup_number*4 + epath_number

One egroup contains 4 epaths, from 0 to 3. You can _combine_ multiple epaths into
one elink, to push more data through it. I.e. if your chip sends more data, you
can make an elink out of all 4 epaths. Or you can make an elink out of 2 epaths,
or out of only 1 epath.  Our firmware makes elinks only out 1 epath (the 320 speed)
and 2 epaths (speed 640). And the elinks _always_ sit on the epath 0 and 2.
It is easier to explain in the `elinkconfig` program.

Anyhow, the translation example is `translate_Felixcore_RX_elink`.

The TX is simpler. The felixcore elink numbers include the same offsets for
the device and the optical link. Then, within the optical links we currently have
groups of 5 simple elinks (here we do not have multiple "epaths" per elink).
Each of these elinks corresponds to some encoder or encoder feature. They all are
connected or related to 1 TX group of HCCs on the stave. We can call them "TX group".
The _LCB command_ elink is the elink number 1 out of 5, counting from 0 to 4.
That's the one in the connectivity jsons.

So, let's return:

    device_number, link_number, tx_group, encoder_id

The translation example is `translate_Felixcore_TX_elink`.

And let's also save `raw_elink` which equals to the elink number plus the GBT offset.
Because that's what we use _right now_. It is a temporary hook, to make the transition smooth.
'''
