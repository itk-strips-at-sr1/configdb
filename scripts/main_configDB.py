#!/usr/bin/env python
import os, json
from json import dumps, loads

import time, datetime, logging

# from pathlib import Path # to get file names in a directory with Path(directory).glob('CERNSR1*testin_hcc*.json') 
# from requests import post as http_post

# Local imports
import functions_configDB as fxt
from configdb_stripsRunkeys import *

logging.basicConfig(level=logging.INFO)

# TODO: Eliminate need for virtual environment
# Check if the script is running in a virtual environment
if not os.getenv('VIRTUAL_ENV'):
    logging.debug(" You are not in a virtual environment. This script should be run in a virtual environment using this alias: \nvnv")


CONFIGDB_KEY = "demi/default/itk-demo-configdb/api/url"  # name of itk-demo-configdb instance
#dir_path = os.path.dirname(os.path.realpath(__file__))
#config_file_path = dir_path + "/../db_configs/SR1_config_files/"  # path to config-files
#config_file_path_hcc = dir_path + "/../../temp_stave_configs/" #"/../db_configs/yarr-star-fe-configs/"  # path to config-files

default_config_dir = '/home/itkstrips/itkstrips-configs-sr1/stave-configs/'



# Read felix config from file
#with open(config_file_path + "felix_yarr_default.json") as file:
#    felix_config_file = file.read()

# the HCC configs
# txrx come from the connectivity config
# and the chip configs come from individual json files
# let's just parse the files into the following 2 dictionaries:
hcc_configs = {} # a dictionary of stave_name: {hcc_itsdaq_num: payload}
hcc_connectivity = {} # a dictionary of stave_name: {itsdaq_num: {tx: <>, rx: <>}}

# we should eventually combine the connectivity and the configs into a dictionary
# that is similar to the runkey structure:
# stave_name: {"side_m": {}, "side_s": {}}
# a dictionary of stave_name: {'rx': {rx_raw, gbt_num, elink, egroup, epath}, 'tx': {tx_raw, gbt_num, group, swapped}}


# let's just load the configs as is
# I assume the connectivity and configs files are in this dir:
# config_file_path_hcc

# /home/itkstrips/integration_felix/flx13/Yarr_config
std_stave_connectivity_files = [
    'PPB1A:CERNSR1_stave0_testing_setup.json',
    'PPB1B:CERNSR1_stave1_testing_setup.json',
    'PPB1C:CERNSR1_stave2_testing_setup.json',
    'PPB1D:CERNSR1_stave3_testing_setup.json',
    'Stave_PPB_RAL38:CERNSR1_stave_testing_setup.json'
    #'PPB1E:CERNSR1_stave4_testing_setup.json',
]


# Entire runkey specified as python dictionary (same runkey as in other example)
# Every entry has a type, payloads and children
# FID is the global FELIX ID for each elink=frontend in the entire ATLAS
# it is a 64-bit number, different fields of which mean different "sub-IDs"
# i.e. there is a DID (Detector ID)
# we can set it to anything we want in the System Test
# it is --did 0x00 in your felix-star processes arguments
# https://gitlab.cern.ch/itk-strips-at-sr1/itk-strips-felix-setup-scripts/-/blob/master/supervisord_standard_2024-02-06.conf?ref_type=heads
# CID (Controller ID) is another field in the full FID
# it identifies not a single FELIX CARD, but the logical devices in it
# e.g. in our felix-star arguments:
# -d 0 ... --cid 0x0000
# -d 1 ... --cid 0x0001
# 1 logical FELIX "device" = 1 DMA channel on the PCIe from the card to the host computer
# in the System Test we currently use the word "device" and not the "controller"
# simply because we use only 1 FLX712 card, and it is not ambiguous in this case
# but we need to start using the full FID terminology for future

runkey_dict_template = {
    "type": "Root",
    "children": [
        {
            "type": "Felix",
            "payloads": [
                {"type": "FID_DETECTOR_ID", "data": "0"}
            ],
            "children": [
                {
                    "type": "FELIX_DEVICE",
                    "payloads": [{"type": "FID_CONTROLLER_ID", "data": str(i)}],
                    "children": [
                        {"type": "GBT_RX", 
                         "payloads": [{"type": "link", "data": str(j)}], "children": []}
                        for j in range(12)  # Loop to create GBT_RX nodes with GBT_LINK payloads
                    ] + [
                        {"type": "GBT_TX",
                         "payloads": [{"type": "link", "data": str(j)}], "children": []}
                        for j in range(12)  # Loop to create GBT_TX nodes with GBT_LINK payloads
                    ]
                }
                for i in range(2)  # Loop to create FELIX_DEVICE nodes with FID_CONTROLLER_ID payloads and child nodes
            ]
        },

        {
            "type": "Geometry",
            "payloads": [],
            "children": [
                {
                    "type": "Section",
                    "payloads": [{"type": "coordinate", "data": "BSS"}],
                    "children": [
                        {
                            "type": "Layer",
                            "payloads": [{"type": "coordinate", "data":  str(i)}],
                            "children": [
                                {"type": "Slot", "payloads": [{"type": "coordinate", "data": str(j)}], "children": []}
                                for j in range(1, 4)  # Loop to create Slot nodes with coordinate payloads
                            ]
                        }
                        for i in range(1, 3)  # Loop to create Layer nodes with coordinate payloads and child nodes
                    ]
                }
            ]
        }
    ]
}

'''
runkey_dict_template = {
    "type": "Root",
    "children": [
        {
            "type": "Felix",
            "payloads": [
                {"type": "FID_DETECTOR_ID", "data": {"value": "0"}},
                {"type": "config", "data": {"value": felix_config_file}}
            ],
            "children": [
                {
                    "type": "FELIX_DEVICE",
                    "payloads": [{"type": "FID_CONTROLLER_ID", "data": {"value": str(i)}}],
                    "children": [
                        {"type": "GBT_RX", "payloads": [{"type": "GBT_LINK", "data": {"value": str(j)}}], "children": []}
                        for j in range(12)  # Loop to create GBT_RX nodes with GBT_LINK payloads
                    ] + [
                        {"type": "GBT_TX", "payloads": [{"type": "GBT_LINK", "data": {"value": str(j)}}], "children": []}
                        for j in range(12)  # Loop to create GBT_TX nodes with GBT_LINK payloads
                    ]
                }
                for i in range(2)  # Loop to create FELIX_DEVICE nodes with FID_CONTROLLER_ID payloads and child nodes
            ]
        },
        {
            "type": "Geometry",
            "payloads": [],
            "children": [
                {
                    "type": "Section",
                    "payloads": [{"type": "coordinate", "data": {"value": "BSS"}}],
                    "children": [
                        {
                            "type": "Layer",
                            "payloads": [{"type": "coordinate", "data": {"value": str(i)}}],
                            "children": [
                                {"type": "Slot", "payloads": [{"type": "coordinate", "data": {"value": str(j)}}], "children": []}
                                for j in range(1, 4)  # Loop to create Slot nodes with coordinate payloads
                            ]
                        }
                        for i in range(1, 3)  # Loop to create Layer nodes with coordinate payloads and child nodes
                    ]
                }
            ]
        }
    ]
}

'''




# from the docs: https://itk-strips-system-tests-sr1.docs.cern.ch/mapping/#support-structure-fibre-mapping
current_felix_fiber_mapping = { 
        'PPB1A': {
         'side_m': {'tx_pri': (0, 7), 'rx_pri': (0, 7), 'rx_sec': (0, 6)},
         'side_s': {'tx_pri': (0, 5), 'rx_pri': (0, 5), 'rx_sec': (0, 4)},
        },

        'PPB1B': {
         'side_m': {'tx_pri': (0,  9), 'rx_pri': (0,  9), 'rx_sec': (0,  8)},
         'side_s': {'tx_pri': (0, 11), 'rx_pri': (0, 11), 'rx_sec': (0, 10)},
        },

        'PPB1C': {
         'side_m': {'tx_pri': (0, 0), 'rx_pri': (0, 0), 'rx_sec': (0, 1)},
         'side_s': {'tx_pri': (0, 2), 'rx_pri': (0, 2), 'rx_sec': (0, 3)},
        },

        'PPB1D': {
         'side_m': {'tx_pri': (1, 2), 'rx_pri': (1, 2)},
         'side_s': {'tx_pri': (1, 3), 'rx_pri': (1, 3)},
        },
    }


def parse_args():
    """
    -d = debug, -c = config_dir, --print = pretty print the runkey, --write = write the runkey back in files, --load = load the runkey from a JSON file
    """
    import argparse

    default_config_dir = '/home/itkstrips/itkstrips-configs-sr1/stave-configs/'

    parser = argparse.ArgumentParser(
        formatter_class = argparse.RawDescriptionHelpFormatter,
        description = "Load a Runkey or pretty-print it with --print",
        epilog = """Examples:
        python main_configDB.py --upload --config-dir /home/itkstrips/itkstrips-configs-sr1/stave-configs/ PPB1A:CERNSR1_stave0_testing_setup.json
        python main_configDB.py --upload --config-dir /home/itkstrips/itkstrips-configs-sr1/stave-configs/ 
        python main_configDB.py --print -c /home/itkstrips/itkstrips-configs-sr1/stave-configs/ PPB1A:CERNSR1_stave0_testing_setup.json
        
        python main_configDB.py --load runkey_latest.json --upload
        python main_configDB.py --load runkey_latest.json --translate temp_config_dir/
        python main_configDB.py --search latest '{"meta_qwe": 111}'
        python main_configDB.py --search latest Root
        python main_configDB.py --read > runkey_latest.json
        python main_configDB.py --profile --read
    """
    )

    parser.add_argument('-d', '--debug',  action='store_true', help="DEBUG level of logging")
    parser.add_argument('--host',  type=str, default="pcatlitkswrod04", help="Host that runs ConfigDB. Default: pcatlitkswrod04.")
    parser.add_argument('--port',  type=str, default="5111", help="Port of ConfigDB. Default: 5111.")
    parser.add_argument('--api-path',  type=str, default="api", help="ConfigDB OpenAPI URL path. Default: /api.")
    parser.add_argument('--print', action='store_true', help=f"Pretty print the runkey")
    parser.add_argument('--translate', type=str, help=f"Write the runkey Felix subtree back in YARR config files")
    parser.add_argument('--load',  type=str, help="If given, load the runkey from a JSON file")
    # https://stackoverflow.com/questions/15301147/python-argparse-default-value-or-specified-value
    parser.add_argument('--runkey-ref', type=str, default='latest', help="The runkey reference in the database to target. Default ID='latest'.")
    parser.add_argument('--upload-template', action='store_true', help="Upload&commit a blank template runkey.")
    parser.add_argument('--upload', action='store_true', help="Upload&commit a runkey to the database.")
    parser.add_argument('--upload-stave', nargs=2, type=str, help="Upload a single RunkeyStave node to the Staves subtree. Arguments: stave name, path to connectivity file.")
    parser.add_argument('--read', action='store_true', help="Download a full runkey from the database by its reference, and print to stdout.")
    parser.add_argument('--search', type=str, nargs=2, help="Search for data in a runkey. Takes 2 parameters: runkey tag and search dictionary.")
    parser.add_argument('--search_subtree', type=str, nargs=3, help="Search for data and it's children in a runkey. Takes 3 parameters: runkey tag*, object_type, and search dictionary.")
    parser.add_argument('-c', '--config-dir', type=str, default=default_config_dir, help=f"Path to the configs to load as a runkey, default is {default_config_dir}")
    parser.add_argument('connectivity_files', type=str, nargs='*',
                    help='the filenames of YARR connectivity jsons, if non given, use the defaults for PPB1[ABCD]')
    parser.add_argument("--profile", type=int, nargs='?', const=20, help="Enable profiling. Showing top default=20 processes.")

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()  

    # Logging levels
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    # for ConfigDB class in pyconfigdb configdb.py
    # http://localhost:5111/api
    configdb_url = f'http://{args.host}:{args.port}/{args.api_path}'
    logging.debug(f" Trying to set ServiceRegistry host for ConfigDB to: {configdb_url}")

    if args.profile: # Optional: put following main logic in 'try' block and last args.profile in 'finally' block
        import cProfile, pstats, io
        profiler = cProfile.Profile()
        profiler.enable()
        logging.info(" Profiling enabled")

    if args.load:
        with open(args.load) as f:
            runkey_dict = json.load(f)

    elif args.read:
        try:
            import pyconfigdb
        except ImportError:
            logging.error(" Error: If this is unsuccessful, make sure you are in the right .venv")

        # ConfigDB connection needs dbKey and ServiceRegistry URL
        # - the host that runs the service registry, which locates the ConfigDB
        # in our SR1 setup 1 host runs both service registry and config DB
        # currently it is the pcatlitkswrod04 SWROD
        configDB = pyconfigdb.ConfigDB(dbKey=CONFIGDB_KEY, srUrl=configdb_url)

        logging.info(f"reading a runkey: {args.runkey_ref}")
        tagTree = configDB.tag_tree(args.runkey_ref, stage=False, payload_data=True)
        if 'id' in tagTree:
            logging.info(f" Downloaded runkey 'id': {tagTree['id']} and time {tagTree.get('time')}")
        else:
            logging.error(" No 'id' in downloaded runkey")
        print(json.dumps(tagTree, indent=2))
        #exit(0)

    elif args.search_subtree: # Returns a search with it's children
        runkey, object_type, search_dict = args.search_subtree
        try:
            search_dict = json.loads(search_dict)
            assert isinstance(search_dict, dict)
        except Exception:
            pass

        try:
            import pyconfigdb
        except ImportError:
            logging.error(" Error: If this is unsuccessful, make sure you are in the right .venv")
        configDB = pyconfigdb.ConfigDB(dbKey=CONFIGDB_KEY, srUrl=configdb_url)

        # this one works as expected, but there is no nested search:
        # sub_tree = configDB.search_subtree(runkey, search_dict=search_dict, payload_data=True)
        # this one is not clear, it returns empty lists:
        # sub_tree = configDB.search_in_tag(runkey, search_dict=search_dict, payload_data=True)
        # this one returns nodes themselves, with ids, no children:
        # sub_tree = configDB.search(runkey, search_dict=search_dict)
        if isinstance(search_dict, dict):
            logging.info(f" Search a runkey {runkey} for search_dict={search_dict}")
            sub_tree = configDB.search_subtree(runkey, object_type=object_type, search_dict=search_dict, payload_data=True)
        else:
            logging.info(f" Search a runkey {runkey} for object_type={object_type}")
            sub_tree = configDB.search_subtree(runkey, object_type=object_type, payload_data=True)

        print(json.dumps(sub_tree, indent=2))
        #exit(0)


    #hccID = configDB.search('latest', object_type="HCC", config_type="config", search_dict=searchDict)
    elif args.search:
        #TODO Config file is formatted incorrectly! Not valid json = cannot search
        runkey, object_type, config_type, search_dict = args.search
        try:
            search_dict = json.loads(search_dict)
            assert isinstance(search_dict, dict)
        except Exception:
            pass

        try:
            import pyconfigdb
        except ImportError:
            logging.error(" Error: If this is unsuccessful, make sure you are in the right .venv")
        configDB = pyconfigdb.ConfigDB(dbKey=CONFIGDB_KEY, srUrl=configdb_url)


        if isinstance(search_dict, dict):
            logging.info(f" Search a runkey {runkey} for search_dict={search_dict}")
            result = configDB.search(runkey, object_type=object_type, config_type=config_type, search_dict=search_dict)
        elif config_type:
            print("config_type:", config_type)
            logging.info(f" Search a runkey {runkey} for config_type={config_type}")
            result = configDB.search(runkey, object_type=object_type, config_type=config_type)
        else:
            logging.info(f" Search a runkey {runkey} for object_type={object_type}")
            result = configDB.search(runkey, object_type=object_type)
        

        print(json.dumps(result, indent=2))
        #exit(0)

    elif args.upload_stave or args.upload_template:
        pass  # these create their own runkey

    else:
        connectivity_files = {}
        for confile_arg_str in args.connectivity_files:
            stave_name, stave_file = confile_arg_str.split(':')
            connectivity_files[stave_name] = stave_file
        runkey_dict = fxt.build_from_template(args.config_dir, connectivity_files, current_felix_fiber_mapping, runkey_dict_template)
        # TODO: are these 2 lines vvv needed/redundant?
        runkey_dict = json.dumps(runkey_dict, default=dict, indent=2)
        runkey_dict = json.loads(runkey_dict)

    if args.print: 
        #print(json.dumps(runkey_dict, default=dict, indent=2))
        with open('runkey_dump_to_file.json', 'w') as f:
            logging.info(" Writing runkey to file: runkey_dump_to_file.json")
            json.dump(runkey_dict, f, indent=2)
        #exit(0)

    if args.translate:
        # try to write the files back, with the new namings
        output_dir = args.translate
        root_node = runkey_dict['objects'][0]
        #print("runkey_dict:", json.dumps(runkey_dict, default=dict, indent=2))
        #print("get_subnode_of_type(root_node, 'Felix')" , json.dumps( get_subnode_of_type(root_node, 'Felix'), default=dict, indent=2))
        yarr_connectivity = runkey_fiber_to_yarr_connectivity(get_subnode_of_type(root_node, 'Felix'))
        write_cfg_files(yarr_connectivity, output_dir)
        #exit(0)


    # Write to file
    #with open('2runkey_dump_to_file.json', 'w') as f:
    #    logging.debug(" Writing runkey to file: 2runkey_dump_to_file.json")
    #    json.dump(runkey_dict, f, indent=2)

    # TODO: this is a temporary fix, we should not have to do this! Swaps "" for dict for metadata
    def update_runkey_dict(d):
        if isinstance(d, dict):
            for k, v in d.items():
                if k == "data" and isinstance(v, str):
                    d[k] = {"value1": v}
                else:
                    update_runkey_dict(v)
        elif isinstance(d, list):
            for item in d:
                update_runkey_dict(item)

    #update_runkey_dict(runkey_dict)

    ## Write to file
    # TODO: why it writes here?
    # Ans: This was a nice debugging feature is all
    #with open('runkey_dump_to_file.json', 'w') as f:
    #    logging.info(" Writing runkey to file: runkey_dump_to_file.json")
    #    json.dump(runkey_dict, f, indent=2)

    configDBname = "stripsRunkeyTest_" + datetime.datetime.now().strftime("%Y-%m-%d_%Hh%Mm%Ss")
    comment = "test" # Commit area comment
    logging.debug(f" Creating {configDBname}")

    try:
        import pyconfigdb
    except ImportError:
        logging.error(" Error: If this is unsuccessful, make sure you are in the right .venv")
    configDB = pyconfigdb.ConfigDB(dbKey=CONFIGDB_KEY, srUrl=configdb_url)

    logging.debug(" Check skype with Jonas on serial name thing, for HCC specifically")

    if args.upload_template:
        logging.info(f"Uploading the runkey template as: {args.runkey_ref}")
        staves_node = {
            "type": "Staves",
            "payloads": [],
            "children": []
        }

        runkey_dict_template['children'].append(staves_node)
        runkey_id = configDB.stage_create(data=runkey_dict_template, comment="upload an blank runkey")
        logging.info(f"Staged id: {runkey_id}")
        ids = configDB.stage_commit(runkey_id, new_name=configDBname+"_blankTemplate", comment=comment)
        logging.info(f"Committed ids: {ids}")

    if args.upload:
        runkey_id = args.runkey_ref
        logging.info(f"Uploading the runkey to the data base: {runkey_id}")
        stage_time = time.time()
        #logging.debug(f"Size of runkey: {runkey_size_mb:.2f} MB")

        if runkey_dict['type'] == 'runkey':
            logging.warning(" You are uploading a runkey json - it does not work currently. The new runkey will be empty.")

        # TODO: how to stage to a specific runkey ID?
        # i.e. where to use runkey_id here?
        runkey_id = configDB.stage_create(data=runkey_dict, comment="foo bar test")  # comment)
        benchmark = fxt.benchmarking(stage_time, "Staging runkey")
        logging.info(f"Staged id: {runkey_id}")

        logging.info("Commiting the runkey to the data base")
        ids = configDB.stage_commit(runkey_id, new_name=configDBname, comment=comment)
        benchmark = fxt.benchmarking(benchmark, "Committing runkey")
        logging.info(f"Committed ids: {ids}")

    elif args.upload_stave:
        from configdb_operations import upload_stave

        logging.info(f"Uploading a single RunkeyStave node to the runkey {args.runkey_ref}")
        # TODO: there is no option to check out the runkey into the staging area now.
        # Therefore, we download the whole runkey, add a stave node, and upload it back.
        # When the staging area becomes available, use it.

        # Create RunkeyStave
        stave_name, stave_fpath = args.upload_stave
        stave_node = fxt.build_stave_node_from_file(stave_name, stave_fpath)

        # pretty-print the stave runkey to the trace log
        logging.debug(f"stave runkey {stave_name}: {json.dumps(stave_node, indent=2)}")

        upload_stave(configDB, args.runkey_ref, stave_node)
        logging.info(f'Uploaded a stave and updated references: {stave_name}')

    else: 
        logging.info(" Not committing to configDB")


    if args.profile:
        profiler.disable()
        s = io.StringIO()
        ps = pstats.Stats(profiler, stream=s).sort_stats("cumulative")
        ps.print_stats(args.profile)
        logging.info(" Profiling information > ./profile.log")
        with open("profile.log", "w") as f:
            f.write(s.getvalue())

    # these must be examples of search on lpGBT payload?
    ##lpGBT_ID = configDB.search('latest', object_type="lpGBT", config_type="config", search_dict=searchDict)
    #GBT_TX_Fibers = configDB.search('latest', object_type="GBT_TX_Fiber")#, config_type="config", search_dict=searchDict)
    ##print("\nGBT_TX_Fibers:", GBT_TX_Fibers)
    ##print("\n \n")
    #Mbps320 = configDB.search('latest', object_type="320Mbps")#, config_type="config", search_dict=searchDict)
    #with open(Mbps320[0]['payloads'][0]['name'], 'w') as f:
    #    f.write(Mbps320[0]['payloads'][0]['data'])
    #lpGBTs = configDB.search_subtree('latest', object_type="lpGBT")#, search_dict=searchDict)
    ##print("\nlpGBTs:", dumps(lpGBTs, indent=2))
    ##print("lpGBTs:", lpGBTs[0])
    ##print('len(lpGBTs):', len(lpGBTs))

    # Search function format
    #search(self, identifier: str, object_type: str = "", config_type: str = "", search_dict: dict = {}):
    #search_subtree(self, identifier: str, object_type: str = "", search_dict: dict = {}, payload_data: bool = False):

    # Search for HCC ID. Search within example dict: {"HCC.regs.Delay1": "02400000"}
    #searchDict = {"value": "{'name': 'Stave3_PPB_hcc14', 'HCC': {'ID': 2, 'fuse_id': '40093f', 'regs': {'Delay1': '02400000', 'Delay2': '44444444', 'Delay3': '00000444', 'PLL1': '00ff3b05', 'DRV1': '0fffffff', 'DRV2': '00000014', 'ICenable': '000007fe', 'OPmode': '00030011', 'OPmodeC': '00030011', 'Cfg1': '00000100', 'Cfg2': '0000000f', 'ExtRst': '00710003', 'ExtRstC': '00710003', 'ErrCfg': '00000000', 'ADCcfg': '00416601', 'Pulse': '00000001'}, 'subregs': {}}, 'ABCs': {'IDs': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 'common': {'ADCS1': '00d031ac', 'ADCS2': '00000b36', 'ADCS3': '89934008', 'CREG0': '0830003c', 'CREG1': '43200200', 'SCReg': '4a504a00'}, 'subregs': [{'BVREF': 12, 'BIREF': 14, 'B8BREF': 12, 'COMBIAS': 10, 'ADC_BIAS': 10, 'D_S_DEC': 12, 'A_S_DEC': 13, 'STR_DEL': 20, 'STR_DEL_R': 3, 'BTRANGE': 6}, {'BVREF': 11, 'BIREF': 13, 'B8BREF': 11, 'COMBIAS': 10, 'ADC_BIAS': 8, 'D_S_DEC': 13, 'A_S_DEC': 13, 'STR_DEL': 20, 'STR_DEL_R': 3, 'BTRANGE': 6}, {'BVREF': 11, 'BIREF': 14, 'B8BREF': 11, 'COMBIAS': 10, 'ADC_BIAS': 9, 'D_S_DEC': 13, 'A_S_DEC': 12, 'STR_DEL': 20, 'STR_DEL_R': 3, 'BTRANGE': 6}, {'BVREF': 12, 'BIREF': 14, 'B8BREF': 12, 'COMBIAS': 11, 'ADC_BIAS': 10, 'D_S_DEC': 11, 'A_S_DEC': 13, 'STR_DEL': 20, 'STR_DEL_R': 3, 'BTRANGE': 6}, {'BVREF': 13, 'BIREF': 15, 'B8BREF': 12, 'COMBIAS': 11, 'ADC_BIAS': 8, 'D_S_DEC': 12, 'A_S_DEC': 13, 'STR_DEL': 20, 'STR_DEL_R': 3, 'BTRANGE': 6}, {'BVREF': 12, 'BIREF': 14, 'B8BREF': 12, 'COMBIAS': 9, 'ADC_BIAS': 10, 'D_S_DEC': 12, 'A_S_DEC': 13, 'STR_DEL': 20, 'STR_DEL_R': 3, 'BTRANGE': 6}, {'BVREF': 13, 'BIREF': 15, 'B8BREF': 13, 'COMBIAS': 10, 'ADC_BIAS': 9, 'D_S_DEC': 11, 'A_S_DEC': 11, 'STR_DEL': 20, 'STR_DEL_R': 3, 'BTRANGE': 6}, {'BVREF': 12, 'BIREF': 15, 'B8BREF': 13, 'COMBIAS': 11, 'ADC_BIAS': 9, 'D_S_DEC': 12, 'A_S_DEC': 12, 'STR_DEL': 20, 'STR_DEL_R': 3, 'BTRANGE': 6}, {'BVREF': 12, 'BIREF': 15, 'B8BREF': 12, 'COMBIAS': 9, 'ADC_BIAS': 9, 'D_S_DEC': 12, 'A_S_DEC': 13, 'STR_DEL': 20, 'STR_DEL_R': 3, 'BTRANGE': 6}, {'BVREF': 12, 'BIREF': 14, 'B8BREF': 12, 'COMBIAS': 10, 'ADC_BIAS': 10, 'D_S_DEC': 13, 'A_S_DEC': 12, 'STR_DEL': 20, 'STR_DEL_R': 3, 'BTRANGE': 6}], 'masked': [None, None, None, None, None, None, None, None, None, None], 'trims': [16, 16, 16, 16, 16, 16, 16, 16, 16, 16], 'Parameters': {}}}"}
    #hccID = configDB.search('stripsRunkeyTest_2024-09-02_22h09m47s', object_type="HCC", config_type="config", search_dict=searchDict)
    #print("\nHCC ID:", hccID[0]['id'])

