#!/usr/bin/env python
import os, json, uuid, argparse, time

# from requests import post as http_post
from pathlib import Path  # to get file names in a directory with Path(directory).glob('CERNSR1*testin_hcc*.json') 
from os.path import isfile

# Local imports
import configdb_connectivity as conn
from configdb_stripsRunkeys import *

# TODO: check this one is never used?
# try:
#     import pyconfigdb
# except ImportError:
#     print("Error: If this is unsuccessful, make sure you are in the right .venv")

#dir_path = os.path.dirname(os.path.realpath(__file__))
# TODO: don't hardcode
lpgbt_config_dir = '/home/itkstrips/itkstrips-configs-sr1/lpgbt/'

# when json library converts a Python object to a json string it checks its type
# unfortunately, it checks whether an object isinstance(obj, dict)
# not some MutableMapping or UserDict, as by the modern rule in Python
# see https://stackoverflow.com/questions/70762125/why-is-class-that-extends-dict-json-serializable-but-one-that-extends-collectio
# json is a very old module, so it still works by old rules
# add default=dict to treat an object of unknown type as a dict
# we use UserDict in the shortcut classes for Strips runkey structures
def dumps(obj):
    return json.dumps(obj)  # default=dict)  # TODO: test without default dict now


def load_std_configs(stave_connectivity_files):
    input_stave_configs = {}
    # a dictionary of
    # stave_name: {
    #   'chipType': <>
    #   'side_m': {}
    #   'side_s': {
    #     itsdaq_num: <config>,
    #     'rx': <>
    #     'tx': <>
    #     'enable':  <>
    #     'locked': <>
    #   }
    # }

    for stave_name, connectivity_conf in stave_connectivity_files.items():
        input_stave_configs.setdefault(stave_name, {'side_m': {}, 'side_s': {}})
        # TODO: should we test that we do not overwrite the config for the same stave?
        stave_conf = input_stave_configs[stave_name]
        #print("Path():",dir_with_configs + '/' + connectivity_conf)

        if isinstance(connectivity_conf, str):
            dir_with_configs, conf_fname = os.path.split(connectivity_conf)
            connectivity_file = Path(dir_with_configs + '/' + conf_fname)
            with connectivity_file.open() as file:
                connectivity_json = json.load(file)

        else:
            assert(isinstance(connectivity_conf, dict))
            connectivity_json = connectivity_conf

        # connectivity_json has a simple structure:
        # chipType: <>
        # chips: [{config_json, tx, rx, enable, locked}]
        # I am not sure what ebanle and locked mean -- TODO: touch base with Bruce
        # for now let's save them with the chip config
        stave_conf['chipType'] = connectivity_json['chipType']  # TODO: chipType probably has to go all the way under HCC
        # although the whole stave is loaded with the same chips,
        # we need to pick this parameter from under the fiber nodes, when we generate the run connectivity scan

        is_short_strip = len(connectivity_json['chips']) > 28
        stave_conf['staveType'] = 'SS' if is_short_strip else 'LS'
        # <-- TODO: this logic might not work for Endcap petals!

        for chip_c in connectivity_json['chips']:
            chip_config = chip_c['config']

            # getting the "itsdaq number"
            if isinstance(chip_config, str):
                # then it is a filename
                # and our convention is to write hcc<itsdaq number>
                # in the filename
                cpath = os.path.basename(chip_c['config'])
                # get the itsdaq  number out of the path
                # f'CERNSR1_{stave_num}_tecting_hcc<num>.json
                # TODO: it is a bad idea to match the number like this
                #       but it is a standard in SR1 currently:
                itsdaq_number = int(cpath[cpath.index('hcc')+3: cpath.index('.json')])

            else:
                assert isinstance(chip_config, dict)
                # if it is a dictionary
                # then it's the HCC config
                # and itsdaq number is saved at the end of its name
                hcc_name = chip_config['name']
                itsdaq_number = int(hcc_name[hcc_name.index('hcc')+3:])

            # translate it to the common spec terms:
            hcc_location = conn.translate_ITSDAQ_number(itsdaq_number, is_short_strip)
            # it returns the location in the format:
            # ReferenceHCCLocation(stave_side, module_number, hybrid_name)
            # where stave_side = s or m
            # module_number is an int
            # hybrid_name is X or Y
            # save it
            side_conf = stave_conf['side_'+hcc_location.stave_side]          
            side_conf.setdefault('FEs', {})

            fe_config = {'module_number': hcc_location.module_number, 'hybrid_name': hcc_location.hybrid_name}

            # of course, read the config and save it
            # I assume it is relative to the path of the connectivity file
            # it might be an absolute path though TODO make it more robust later
            if isinstance(chip_config, str):
                with open(dir_with_configs + '/' + chip_config) as file:
                    hcc_config = json.load(file)

            else:
                assert isinstance(chip_config, dict)
                hcc_config = chip_config

            #side_conf['FEs'][fe_id]
            fe_config['config'] = hcc_config

            # TODO: I think enable and locked are the parameters for the scan engine
            #       (e.g. if a FE is "locked" the scan won't update the config file inplace)
            #       if so, these parameters should not be in the chip configs
            #       they are the configuartion of the scan, i.e. the procedure thatis done on the chips
            fe_config['enable'] = chip_c['enable']
            fe_config['locked'] = chip_c['locked']

            # now the connectivity
            raw_tx = chip_c['tx']
            raw_rx = chip_c['rx']  

            cur_rx_connectivity = conn.translate_Felixcore_RX_elink(raw_rx)
            # namedtuple("ReferenceHCCFelixRX",  "device gbt_link raw_elink elink egroup epath")
            # device gbt_link raw_elink are global addresses, i.e. which fiber is connected to this stave side
            # only elink egroup epath are the connectivity on the stave side
            # but!
            # 1 stave side can have 1 or 2 RX fibers (and always only 1 tx fiber)
            # let's assume its current GBT links are always an odd + an even
            rx_name = 'rx_pri'  # "pri" for primary lpGBT, it is the one with both tx and rx
            if is_short_strip and cur_rx_connectivity.gbt_link % 2 == 1:
                # TODO: this is wrong!! in the current fiber mapping not all sec lpGBT are on odd GBT links
                rx_name = 'rx_sec'  # the secondary lpGBT RX fiber, this lpGBT does not have a TX fiber, and it is controlled by the primary lpGBT

            side_conf.setdefault(rx_name, {'uuid': str(uuid.uuid4()), 'elinks': {}})
            side_conf[rx_name]['elinks'][(cur_rx_connectivity.elink, itsdaq_number)] = {
                'egroup': cur_rx_connectivity.egroup,
                'epath': cur_rx_connectivity.epath}  # , 'itsdaq_number': itsdaq_number

            cur_tx_connectivity = conn.translate_Felixcore_TX_elink(raw_tx)
            # namedtuple("ReferenceHCCFelixTX",  "device gbt_link raw_elink elink tx_group encoder_id swapped")
            # a stave side always has only 1 tx fiber
            side_conf.setdefault('tx_pri', {'uuid': str(uuid.uuid4()), 'elinks': {}}) # to connect it under FELIX tree
            side_conf['tx_pri']['elinks'][(cur_tx_connectivity.elink, itsdaq_number)] = {
                            'tx_group': cur_tx_connectivity.tx_group,
                            'encoder_id': cur_tx_connectivity.encoder_id,
                            'swapped': cur_tx_connectivity.swapped}
                            # 'itsdaq_number': itsdaq_number

            # let's save the elinks in the individual chip configs so that it is easier to use later
            fe_config['rx_elink'] = (rx_name, cur_rx_connectivity.elink) # yeah
            fe_config['tx_elink'] = cur_tx_connectivity.elink

            # let's also generate the UUID for the runkey references
            side_conf['FEs'][itsdaq_number] = str(uuid.uuid4()), fe_config

    return input_stave_configs


'''
now everything is saved in input_stave_configs as

stave_name:
  chipType: <>
  side_[m|s]:
    tx:
      elink: {tx_group, encoder_id, swapped, itsdaq_number}
    rx_[0|1]:
      elink: {egroup, epath, itsdaq_number}
    FEs:
      <itsdaq_number>:
        (uuid, {module_number, hybrid_name, config, enable, locked, rx_elink, tx_elink})

not sure if it is perfect, but should be close enough
'''

def build_stave_node(stave_name, stave_cfg):
    stave_data = {
        "staveName": stave_name,
        "staveType": stave_cfg['staveType'],
        "chipType": stave_cfg['chipType']
    }
    stave_side_m = RunkeyNode("Side", {"sideType": "side_m"})
    stave_side_s = RunkeyNode("Side", {"sideType": "side_s"})
    a_stave_runkey = RunkeyNode("Stave", stave_data, children=[stave_side_m, stave_side_s])

    # Fill the sides configs
    for rkey_side, side_config in [(stave_side_m, stave_cfg['side_m']), (stave_side_s, stave_cfg['side_s'])]:
        # Add lpGBT configs to the stave (In a loop, one side at a time)
        sideStr = 'M' if stave_side_m == rkey_side else 'S'

        file_names = []
        speeds = ["320", "640"]
        for speed in speeds:
            if stave_name == "PPB1A" or stave_name == "PPB1B" or stave_name == "PPB1C":
                file_names.extend([
                    f"{speed}Mbps/lpgbt_SS_{sideStr}_LH_Pri_{speed}.cnf",
                    f"{speed}Mbps/lpgbt_SS_{sideStr}_LH_Sec_{speed}.cnf"
                ])
                if stave_name == "PPB1C" and sideStr == "S": # Stave C is different by this one file
                    file_names.remove(f"{speed}Mbps/lpgbt_SS_S_LH_Pri_{speed}.cnf")
                    file_names.extend([f"{speed}Mbps/lpgbt_SS_S_RH_Pri_{speed}.cnf"])
            elif stave_name == "PPB1D":
                file_names.extend([f"{speed}Mbps/lpgbt_LS_{sideStr}_LH_Pri_{speed}.cnf"])

        lpgbt_variables = []
        # FIXME: there is no such file on CI! it must be an input from main
        #for file_name in file_names:
        #    with open(lpgbt_config_dir + file_name) as file:
        #        lpgbt_variables.append(file.read())

        lpGBT_320Mbps = RunkeyNode('320Mbps')
        lpGBT_640Mbps = RunkeyNode('640Mbps')

        if len(lpgbt_variables) == 4:  # SS
            # TODO: the third argument is UUID, not name - should it be like that?
            lpGBT_320Mbps.add_payload(file_names[0][14:25], lpgbt_variables[0], name=file_names[0][8:])
            lpGBT_320Mbps.add_payload(file_names[1][14:25], lpgbt_variables[1], name=file_names[1][8:])
            lpGBT_640Mbps.add_payload(file_names[2][14:25], lpgbt_variables[2], name=file_names[2][8:])
            lpGBT_640Mbps.add_payload(file_names[3][14:25], lpgbt_variables[3], name=file_names[3][8:])
        elif len(lpgbt_variables) == 2:  # LS
            lpGBT_320Mbps.add_payload(file_names[0][14:25], lpgbt_variables[0], name=file_names[0][8:])
            lpGBT_640Mbps.add_payload(file_names[1][14:25], lpgbt_variables[1], name=file_names[1][8:])

        rkey_lpGBT = RunkeyNode('lpGBT', children=[lpGBT_320Mbps, lpGBT_640Mbps])
        rkey_side.add_child(rkey_lpGBT)

        # FEs configs
        for itsdaq_number, (fe_uuid, fe_cfg) in side_config.get('FEs', {}).items():
            # rx_elink_data = {'rx_side': fe_cfg['rx_elink'][0], 'rx_elink': fe_cfg['rx_elink'][1]}
            # TODO: why is it "rx_side"? it is processing a side here, right?
            #       what was it before? link?
            # hcc_data = {
            #     'itsdaq_number': str(itsdaq_number),
            #     'module_number': str(fe_cfg['module_number']),
            #     'hybrid_name': str(fe_cfg['hybrid_name']),
            #     'rx_elink': rx_elink_data,
            #     'tx_elink': str(fe_cfg['tx_elink']),
            #     'enable': str(fe_cfg['enable']),
            #     'locked': str(fe_cfg['locked'])
            # }
            fe_runkey = RunkeyHCC(itsdaq_number, **fe_cfg)

            # # What is this:
            # if fe_cfg['config']['name'].startswith('Stave'):
            #     stave_number = fe_cfg['config']['name'].split('_')[0][5:]
            #     hcc_file = f"CERNSR1_stave{stave_number}_testing_hcc{fe_cfg['config']['name'].split('hcc')[-1]}.json"

            # fe_runkey = RunkeyNode('HCC', hcc_data, fe_cfg['config'], config_name=hcc_file)
            # how come HCC is entered as a general RunkeyNode and not RunkeyHCC?

            rkey_side.add_child(fe_runkey)

        # TX and RX fiber configuration
        if 'tx_pri' in side_config:
            gbt_tx_data = {
                'chipType': stave_cfg['chipType'],
                'staveName': stave_name,
                'staveSide': sideStr
            }
            # why chiptype is here at all?
            # these are shortcuts, while references don't work:
            # we generate YARR configs from fibers under Felix subtree
            rkey_tx = RunkeyNode('GBT_TX_Fiber', gbt_tx_data)
            rkey_side.add_child(rkey_tx)

            for (elink, itsdaq_number), more_info in side_config['tx_pri']['elinks'].items():
                tx_elink_data = {
                    'elink_number': elink,
                    'tx_group': more_info['tx_group'],
                    'encoder_id': more_info['encoder_id'],
                    'swapped': more_info['swapped']
                }
                rkey_elink = RunkeyNode('elink', tx_elink_data)
                '''
                side_conf['tx_pri']['elinks'][(cur_tx_connectivity.elink, itsdaq_number)] = {
                        'tx_group': cur_tx_connectivity.tx_group,
                        'encoder_id': cur_tx_connectivity.encoder_id,
                        'swapped': cur_tx_connectivity.swapped}
                print("encode_id:", more_info['encoder_id'])
                '''

                #load_payloads_from_dict(rkey_elink, more_info)
                rkey_tx.add_child(rkey_elink)

                # TODO: check what is 'reuse_id'? Here it attempts to add it as a node.
                #       is it a reference to the HCC node?
                #hcc = get_subnode_of_type_with_payload(rkey_side, 'HCC', 'itsdaq_number', str(itsdaq_number))
                hcc_matches = get_subnodes_of_type_with_nested_field(rkey_side, 'HCC', 'itsdaq_number', itsdaq_number)
                if len(hcc_matches)==1:
                    # rkey_elink.add_child({"reuse_id": hcc['id']})
                    rkey_elink['children'].append({"reuse_id": hcc_matches[0]['id']})

                else:
                    raise RuntimeError(f"not just 1 hcc ({itsdaq_number}) for this elink {elink}: {len(hcc_matches)}")

        for rx_fiber_name in ('rx_pri', 'rx_sec'):
            if rx_fiber_name in side_config:
                rkey_rx = RunkeyNode('GBT_RX_Fiber',
                                        {'rx_fiber_type': rx_fiber_name,
                                        'chipType': stave_cfg['chipType'],
                                        'staveName': stave_name,
                                        'staveSide': sideStr})
                #rkey_rx.add_payload('staveName', stave_name)
                rkey_side.add_child(rkey_rx)

                for (elink, itsdaq_number), more_info in side_config[rx_fiber_name]['elinks'].items():
                    rx_elink_data = {
                        'elink_number': elink,
                        'egroup': more_info['egroup'],
                        'epath': more_info['epath']
                    }
                    rkey_elink = RunkeyNode('elink', rx_elink_data)
                    #load_payloads_from_dict(rkey_elink, more_info)
                    rkey_rx.add_child(rkey_elink)

                    #hcc = get_subnode_of_type_with_payload(rkey_side, 'HCC', 'itsdaq_number', str(itsdaq_number))
                    hcc_matches = get_subnodes_of_type_with_nested_field(rkey_side, 'HCC', 'itsdaq_number', itsdaq_number)
                    if len(hcc_matches)==1:
                        # rkey_elink.add_child({"reuse_id": hcc['id']})
                        # TODO: again, check what is 'reuse_id' - is it a reference?
                        rkey_elink['children'].append({"reuse_id": hcc_matches[0]['id']})

                    else:
                        raise RuntimeError(f"not just 1 hcc ({itsdaq_number}) for this elink {elink}: {len(hcc_matches)}")


    return a_stave_runkey

def build_stave_node_from_file(stave_name, connectivity_fname):
    std_config = load_std_configs({stave_name: connectivity_fname})
    return build_stave_node(stave_name, std_config[stave_name])

def build_stave_node_from_dict(stave_name, config_dict):
    std_config = load_std_configs({stave_name: config_dict})
    return build_stave_node(stave_name, std_config[stave_name])

# Done with the staves configs. Now we need the references to the fibers in the FELIX tree
# https://itk-strips-system-tests-sr1.docs.cern.ch/mapping/#physical-location-of-staves
current_felix_geometry_mapping = {
        'PPB1B': StripsGeometryCoordinate('BSS', '1', '1'),
        'PPB1A': StripsGeometryCoordinate('BSS', '1', '2'),
        'PPB1C': StripsGeometryCoordinate('BSS', '1', '3'),
        'PPB1D': StripsGeometryCoordinate('BSS', '2', '1')
}


def connect_staves(runkey_dict, fiber_mapping, current_felix_geometry_mapping):
    '''connect_staves(runkey_dict, fiber_mapping, current_felix_geometry_mapping)

    Function to connect staves in the runkey to FELIX fibers
    according to the given mapping.
    '''

    staves_tree = get_subnode_of_type(runkey_dict, "Staves")
    felix_tree = get_subnode_of_type(runkey_dict, "Felix")
    geom_tree = get_subnode_of_type(runkey_dict, "Geometry")

    for rkey_stave in staves_tree['children']:
        staveName = get_node_payload_data(rkey_stave, 'staveName')
        test_stave_connected = staveName in fiber_mapping and staveName in current_felix_geometry_mapping
        if not test_stave_connected:
            logging.warning(f'Skipping stave {staveName}, connected only to: FELIX {staveName in fiber_mapping} and Geom {staveName in current_felix_geometry_mapping}')
            continue

        stave_coord = current_felix_geometry_mapping[staveName]

        #             hcc = get_subnode_of_type_with_payload(rkey_side, 'HCC', 'itsdaq_number', str(itsdaq_number))
        rkey_geom_section = get_subnode_of_type_with_payload(geom_tree, 'Section', 'coordinate', stave_coord.section)
        rkey_geom_layer = get_subnode_of_type_with_payload(rkey_geom_section, 'Layer', 'coordinate', stave_coord.layer)
        rkey_geom_slot = get_subnode_of_type_with_payload(rkey_geom_layer, 'Slot', 'coordinate', stave_coord.slot)

        # import pdb; pdb.set_trace()
        rkey_geom_slot['children'].append({"reuse_id": rkey_stave['id']})

        for rkey_side in rkey_stave['children']:
            if not rkey_side['children']:
                continue

            sideType = get_node_payload_data(rkey_side, 'sideType')
            tx_fiber = get_subnode_of_type(rkey_side, 'GBT_TX_Fiber')

            tx_dev_num, tx_gbt_num = fiber_mapping[staveName][sideType]['tx_pri']
            rkey_flx_device = get_subnode_of_type_with_payload(felix_tree, 'FELIX_DEVICE', 'FID_CONTROLLER_ID', str(tx_dev_num))
            rkey_tx_fiber = get_subnode_of_type_with_payload(rkey_flx_device, 'GBT_TX', 'link', str(tx_gbt_num))
            rkey_tx_fiber['children'].append({"reuse_id": tx_fiber['id']})
            # TODO: again, I do not see HCCs under the elinks here
            #       is something needed in HCC attributes or payloads?
            # copy the whole thing for now:
            # rkey_tx_fiber['children'].append(tx_fiber.make_reference_clone())

            # RX fiber configuration
            for rx_type in ('rx_pri', 'rx_sec'):
                rx_fiber = get_subnode_of_type_with_payload(rkey_side, 'GBT_RX_Fiber', 'rx_fiber_type', rx_type)
                if rx_fiber is None:
                    if rx_type in fiber_mapping[staveName][sideType]:
                        logging.warning(f'The runkey does not have an {rx_type} but the mapping has it: stave {staveName} side {sideType}')
                    continue

                rx_dev_num, rx_gbt_num = fiber_mapping[staveName][sideType][rx_type]
                rkey_flx_device = get_subnode_of_type_with_payload(felix_tree, 'FELIX_DEVICE', 'FID_CONTROLLER_ID', str(rx_dev_num))
                rkey_rx_fiber = get_subnode_of_type_with_payload(rkey_flx_device, 'GBT_RX', 'link', str(rx_gbt_num))
                rkey_rx_fiber['children'].append({"reuse_id": rx_fiber['id']})
                # TODO: check reuse_id
                # rkey_rx_fiber['children'].append(rx_fiber.make_reference_clone())

# Info how to match connectivity filenames to stave names.
std_stave_connectivity_files = {
    'PPB1A': 'CERNSR1_stave0_testing_setup.json',
    'PPB1B': 'CERNSR1_stave1_testing_setup.json',
    'PPB1C': 'CERNSR1_stave2_testing_setup.json',
    'PPB1D': 'CERNSR1_stave3_testing_setup.json',
}

new_stave_connectivity_files = {
    'PPB1A': 'CERNSR1_testing_setup-PPB1A.json',
    'PPB1B': 'CERNSR1_testing_setup-PPB1B.json',
    'PPB1C': 'CERNSR1_testing_setup-PPB1C.json',
    'PPB1D': 'CERNSR1_testing_setup-PPB1D.json',
}

# Main function to build from template
def build_from_template(config_dir, connectivity_files, current_felix_fiber_mapping, runkey_dict_template):
    from copy import deepcopy
    runkey_dict = deepcopy(runkey_dict_template)

    if not connectivity_files:
        # check whether the old naming or new naming is in the directory:
        if   all(isfile(config_dir + '/' + fpath) for fpath in std_stave_connectivity_files.values()):
            connectivity_files = std_stave_connectivity_files
        elif all(isfile(config_dir + '/' + fpath) for fpath in new_stave_connectivity_files.values()):
            connectivity_files = new_stave_connectivity_files
        else:
            raise Exception(f"Without connectivity input, cannot find either std or new connectivity files in {config_dir}")

    # build the Staves sub-tree
    runkey_staves = RunkeyNode("Staves")

    # loop over connectivity staves & build stave nodes one by one
    for stave_name, std_stave_connectivity_fname in connectivity_files.items():
        # a temporary intermediate config:
        a_stave_runkey = build_stave_node_from_file(stave_name, config_dir + '/' + std_stave_connectivity_fname)
        runkey_staves.add_child(a_stave_runkey)

    runkey_dict['children'].append(runkey_staves)

    connect_staves(runkey_dict, current_felix_fiber_mapping, current_felix_geometry_mapping)

    return runkey_dict


def benchmarking(start_time, comment, open_as='a'):
    with open('benchmarking.txt', open_as) as f:
        f.write(f"Time for [{comment}]: " + f"{time.time() - start_time:.3f}" + " seconds\n")
    return time.time()
