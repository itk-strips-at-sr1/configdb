Typical usage, from the [SR1 docs](https://itk-strips-system-tests-sr1.docs.cern.ch/daq/):

```
cd ~/tests/YARR_upstream/build

bin/scanConsole -p -c CERNSR1_stave0_testing_setup.json \
  -r ~/itkstrips-configs-sr1/stave-configs/felix_client_strips_flx03_newNamings.json \
  -l ~/itkstrips-configs-sr1/stave-configs/trace_star.json \
  -s ~/itkstrips-configs-sr1/stave-configs/std_config.json
```

# Misc

Disable HPRs in chip configs:

```
sed -i '/"subregs": {}/ s/{}/{"MASKHPR": "1", "STOPHPR": "1"}/' CERNSR1_stave0_testing_hcc*.json 
sed -i 's/"BTRANGE"/"MASKHPR": "1", "STOPHPR": "1",\n        "BTRANGE"/' CERNSR1_stave0_testing_hcc*.json 
```
